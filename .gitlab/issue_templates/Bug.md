## **Bugs:** indicar o(s) bug(s)
**Descrição:** descrição detalhada do erro (ex: criei um loop 5x mas o programa corre-o 10x)

---

**Teste:** indicar o teste que levou à deteção desse erro (ex: após a inserção de uma função qualquer, o programa deixou de compilar)

**Resultado esperado:** indicar o que se esperava que acontecesse caso não houvesse esse erro

--- 

**Lista completa de bugs:**

- [ ] Bug 1
- [ ] Bug 2
- [ ] ...
