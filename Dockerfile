# Dockerfile

# FROM directive instructing base image to build upon
FROM python:3.7-buster

EXPOSE 8080
STOPSIGNAL SIGTERM

#RUN apt-get update
#RUN apt-get -y install git
#RUN git clone --single-branch --branch deploy-test https://gitlab.com/MigDinny/dash-it.git --depth 1 && cd dash-it


RUN mkdir /temporary

COPY . /temporary/

WORKDIR /temporary

RUN pip install -r requirements.txt

#WORKDIR /dev

RUN cd dev && python3 manage.py makemigrations
RUN cd dev && python3 manage.py migrate

CMD cd dev && python3 manage.py runserver 0.0.0.0:8000
