# **READ ME**
Este read me contém informações sobre como correr a aplicação e referências a documentos essenciais ao projeto.
 
--------------------------------

## **Índice**
1. [**Como correr**](https://gitlab.com/MigDinny/dash-it/-/tree/dev#1-como-correr) <br>
    1. [Instalar pip na consola](https://gitlab.com/MigDinny/dash-it/-/tree/dev#1-instalar-pip-na-consola) <br>
    2. [Instalar dependências no Python](https://gitlab.com/MigDinny/dash-it/-/tree/dev#2-instalar-o-django-no-pyhton) <br>
    3. [Instalar Git GUIs](https://gitlab.com/MigDinny/dash-it/-/tree/dev#3-instalar-git-guis) <br>
        1. [\[windows, MacOS ou Linux\] Gitkraken](https://gitlab.com/MigDinny/dash-it/-/tree/dev#31-windows-macos-ou-linux-gitkraken) <br>
        2. [\[Windows ou MacOS\] Github Desktop](https://gitlab.com/MigDinny/dash-it/-/tree/dev#32-windows-ou-macos-github-desktop) <br>
        3. [Terminal](https://gitlab.com/MigDinny/dash-it/-/tree/dev#33-terminal) <br>
        4. [Possíveis erros](https://gitlab.com/MigDinny/dash-it/-/tree/dev#34-poss%C3%ADveis-erros) <br>
    4. [Executar o programa](https://gitlab.com/MigDinny/dash-it/-/tree/dev#4-executar-o-programa) <br>
    5. [Testar com docker](https://gitlab.com/MigDinny/dash-it#5-testar-com-docker) <br>
2. [**Referências**](https://gitlab.com/MigDinny/dash-it/-/blob/dev/README.md#2-refer%C3%AAncias) <br>




## **1. COMO CORRER**
### Destinado a: **todos**.
#### **Requisitos:** nenhum.<br><br>

### **1. Instalar pip na consola**
- Para terminal/wsl:
    ```zsh 
    sudo apt install python3-pip
    ```

    >**Nota:** Consoante a versão de python instalada, poderá mudar para python, python2 ao invés de python3.

- [Para consola do windows](https://www.liquidweb.com/kb/install-pip-windows/)

### **2. Instalar dependências no Pyhton**
    pip install Django
    pip install pydriller

### **3. Instalar Git GUIs**
Para conseguir ter o projeto na sua máquina local tem que instalar o Git (ver ponto 2.1. Básicos do Git - Instalar o Git).
Os **próximos passos** consistem em **diferentes maneiras de correr o projeto na máquina local** recorrendo a diferentes programas. **Escolher uma ou várias opções para trabalhar.**
Após ter feito clone do projeto não fazer uma segunda vez, trabalhar na diretoria escolhida**.
<br><br>

Se pretender utilizar outra ferramenta não mencionadas abaixo há algumas opções para escolher, nomeadamente: https://git-scm.com/downloads/guis .<br>
Convém pesar os prós e os contras de cada uma e selecionar o que achar mais indicado: https://en.wikipedia.org/wiki/Comparison_of_Git_GUIs . <br><br>


Para configurar a ferramenta de trabalho com o Git, seguir passos:
#### **3.1. [Windows, MacOS ou Linux] GitKraken**
1. [Instalar gitkraken](https://www.gitkraken.com/download)
2.  Ir a File > Preferences > Integrations > Gitlab > Connect to Gitlab > Continue Authorization
3. **Não usar se já foi feito um clone do projeto antes**.<br>
No gitlab clicar em: https://gitlab.com/MigDinny/dash-it > Clone > Copiar texto em **Clone with HTTPS**(https://gitlab.com/MigDinny/dash-it.git).<br>
No gitkraken, File > Clone Repo > Clone with URL > Colocar diretoria onde quer guardar projeto e URL copiado anteriormente > Clone the repo!
4. Para começar a trabalhar no projeto ir a File > Open Repo > Selecionar repositório a utilizar
5. [Tutorial para saber utilizar GitKraken](https://youtu.be/ub9GfRziCtU?list=PLe6EXFvnTV78WqGmGSq8JPnafR3lAa55n)
<br>

#### **3.2. [Windows ou MacOS] Github Desktop**
1. [Instalar Github Desktop - Selecionar MacOS ou Windows](https://docs.github.com/pt/desktop/installing-and-configuring-github-desktop/installing-and-authenticating-to-github-desktop/installing-github-desktop)
2. [Autenticação (Selecionar MacOS ou Windows)](https://docs.github.com/pt/desktop/installing-and-configuring-github-desktop/installing-and-authenticating-to-github-desktop/authenticating-to-github)
3. **Não usar se já foi feito um clone do projeto antes**.<br>
No gitlab clicar em: https://gitlab.com/MigDinny/dash-it > Clone > Copiar texto em **Clone with HTTPS**(https://gitlab.com/MigDinny/dash-it.git).<br>
No GitHub Desktop, File > Clone Repository > URL > Colocar diretoria onde quer guardar projeto(Local path) e URL copiado anteriormente(URL) > Clone
4. Para começar a trabalhar no projeto ir a Current repository > Other > Selecionar o correto
5. [Tutorial para saber utilizar GitHub Desktop](https://www.youtube.com/watch?v=RPagOAUx2SQ) 
<br>

#### **3.3. Terminal**
Configuração:
1. [Open a terminal](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#open-a-terminal)
2. [Install Git](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#install-git)
3. [Configure Git](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#configure-git)
4. **Não usar se já foi feito um clone do projeto antes**.<br>
No gitlab clicar em: https://gitlab.com/MigDinny/dash-it > Clone > Copiar texto em **Clone with HTTPS**(https://gitlab.com/MigDinny/dash-it.git).<br>
Mudar para a diretoria onde quer guardar o projeto antes de executar: [Clone with HTTPS](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-with-https) 

Depois para utilizar o terminal:
1. Abrir o terminal na diretoria do projeto
2. [Ver repositórios remotos](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#view-your-remote-repositories)
3. [Fazer download das últimas mudanças no projeto](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#download-the-latest-changes-in-the-project)
4. [Mudar para o branch correto - dev - se for uma feature de desenvolvimento](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#switch-to-a-branch)
5. [Criar novo branch](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#create-a-branch)
6. [Outros comandos](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#view-differences)<br>

<br>

#### **3.4. Possíveis erros**
**Erro no Gitlab: Não será possível fazer push ou pull via SSH até ser adicionada uma chave SSH ao perfil** - O Gitlab usa o protocolo SSH para comunicar com o Git. Quando se usa a chave SSH para se autenticar com o servidor remoto do Gitlab não é necessário fornecer sempre o username e password. : 
- [Gitlab - SSH](https://docs.gitlab.com/ee/ssh/)
- [Youtube - Tutorial](https://www.youtube.com/watch?v=mNtQ55quG9M&list=PLhW3qG5bs-L8YSnCiyQ-jD8XfHC2W1NL_&index=4)
<br><br><br>


### **4. Executar o programa**
- Verificar se o ficheiro manage.py se encontra na diretoria. Para tal efeito, correr os comandos:
    ```zsh
    cd dash-it/dev/dash_it
    ls
    ```
- Executar os comandos:
    ```zsh
    python3 manage.py migrate
    python3 manage.py runserver
    ````

    Deverá ter como output algo semelhante:
    ```
    Operations to perform:
    Apply all migrations: admin, auth, contenttypes, sessions
    Running migrations:
    No migrations to apply.
    Watching for file changes with StatReloader
    Performing system checks...

    System check identified no issues (0 silenced).
    October 04, 2021 - 19:32:33
    Django version 3.2.7, using settings 'dash_it.settings'
    Starting development server at http://127.0.0.1:8000/
    Quit the server with CONTROL-C.
    ````
    Finalmente, deverá aceder ao link http://127.0.0.1:8000/, que aparece no output para poder aceder à página web criada.

<br>

#### **5. Testar com docker**

Para testar, com o docker, devem instalar o mesmo se ainda não o tiverem feito. <br>
Link: https://docs.docker.com/desktop/windows/install/  <br>

- docker build . -t dashit:dev <br>


Este comando deve ser executado dentro da pasta dash-it, onde estão os requirements.txt e o Dockerfile. Uma imagem chamada "dashit" é criada, com a tag "dev". A tag serve para diferenciar várias imagens com o mesmo nome. Podem usar a tag para saber qual é a branch que estão a usar para criar imagem, mas não é obrigatório. Para ser mais fácil, para quem não está habituado a usar o docker, aconselhamos a apagarem a imagem que criaram sempre que querem criar outra para minimizar conflitos. Podem apagá-la na própria UI do Docker, ou procurem um comando na net para isso. <br>

- docker run -p 8080:8000 dashit:dev  <br>


Este comando serve para executar a imagem dentro de um container isolado do docker. O último argumento diz respeito à imagem a usar, e deverá ser a mesma do comando anterior. O argumento anterior serve para mapear as portas. Isto é super importante: significa que a porta 8000 do Django dentro do container é mapeada para a porta 8080 do HOST da máquina virtual, ou seja, vocês querem aceder depois a 127.0.0.1:8080 e não 8000 (8000 só é valido dentro do container, no vosso PC usam 8080). <br>



---

## **2. REFERÊNCIAS**

### ATAS

- [Atas](https://gitlab.com/MigDinny/dash-it/-/tree/dev/%5Bproc%5D/atas) <br>
- [Planilha](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/planilha_atas.pdf) <br>
- [Procedimento](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/manual_qualidade.md#3-atas) <br>
- [Template](https://gitlab.com/MigDinny/dash-it/-/snippets/2208621) <br>

### DIAGRAMAS E LISTAS
- [Conventional commits](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/Conventional-Commits_cheat_sheet.pdf) <br>
- [Diagrama de arquitetura de python/django](https://gitlab.com/MigDinny/dash-it/-/blob/dev/arch/diagramas/arquitetura%20python%20django.png) <br>
- [Diagrama de arquitetura de software](https://gitlab.com/MigDinny/dash-it/-/blob/dev/arch/diagramas/arquitetura%20software.png)
- [Diagrama de comunicação](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/Diagrama_de_Comunicacao.pdf) <br>
- [Lista de cargos](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/Lista_de_Cargos_-_Dash_it_-v1.pdf) <br>
- [Modelos ER](https://gitlab.com/MigDinny/dash-it/-/tree/dev/arch/diagramas)<br>

### MANUAIS
- [Manual de qualidade](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/manual_qualidade.md) <br>
- [Manual de requisitos](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md) <br>

### REQUISITOS
- [Mapa de navegação](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/imagens/Mapa_de_navegacao.png) <br>
- [Requisitos deployed 04/12](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/Requitos%20Deployed%2004.12.2021.md) <br>

### TUTORIAIS
- [Criar um requisito](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/tutorials/tutorial_requirement.md) <br>
- [Django](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/tutorials/tutorial_django.md) <br>
- [Ficheiro Pessoal](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/tutorials/tutorial_ficheiro_pessoal.md) <br>
- [Git](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/tutorials/tutorial_git.md) <br>
- [HTML](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/tutorials/tutorial_html.md) <br>
- [Weights](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/tutorials/tutorial_weights.md) <br>

### UI
- [Mockups](https://gitlab.com/MigDinny/dash-it/-/tree/dev/req/Mockups) <br>
- [Vistas](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/Vistas.md) <br>

### VERIFICAÇÕES
- [Verificação de tempos colocados nos issues](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Verificacao_de_tempos_nos_issues.md)<br>
- [Verificação da estrutura dos requisitos](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Verificacao_da_estrutura_dos_requisitos.md)<br>

