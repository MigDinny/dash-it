# **Ata nº10** | Engenharia de Software PL10

## 06/12/2021

### Apresentação da atividade da turma ao longo da semana<br><br>

<div align="justify">

## **ESTADO DO PROJETO**

**Estruturais:**<br>

- [ ] Manual de Requisitos (em desenvolvimento);
- [ ] Manual de Qualidade (em desenvolvimento);
- [ ] Manual de Instalação;
- [x] Plano de Testes;
- [ ] Rever os branches e limpar os que estiverem inativos.

**Projeto:**<br>

- [x] Voltar a fazer o documento dos requisitos para ser mais discriminado;
- [x] Criação de um mapa de navegação para saber o que está a ser mostrado.

**Progresso:** 3/7

<br>

---

## **PONTOS DEBATIDOS**

1. **Estado do projeto**<br>

   - Durante a semana foram desenvolvidos alguns requisitos, todos os testes para o deploy, foi feito o mapa de navegação e foram escritos os vários documentos;
   - Foram implementados os requisitos que constam no ficheiro de entrega do dia 04.12.2021;
   - Foi feito o css para algumas das vistas (não todas). Contudo, este ainda não consta no deploy entregue a 04.12.2021;
   - Para analisar outro repositório através da nossa dashboard é necessário aguardar uns minutos ou dar refresh à página;
   - A equipa de testes encontrou bastantes erros no projeto dos quais bastantes foram resolvidos antes do primeiro deploy. Dos erros que não foram corrigidos, ficaram escritos para análise e correção futura;
   - Foi escrito o documento com os requisitos implementados para entregar à outra turma;
   - A equipa já recebeu o documento da outra turma com a informação detalhada sobre o que é necessário para testar o deploy dos mesmos, já distribuiu os vários requisitos pela equipa de testes e já começaram a testar;
   - A equipa de testes está a testar tanto os requisitos internos do projeto como os exteriores da outra equipa;
   - Relato de como foi feita a divisão das tarefas durante a semana, onde haverá pessoas que irão testar o projeto de outros grupos, e pessoas que irão continuar a desenvolver o projeto;
   - Já está disponível o ficheiro .md com o plano de testes no GitLab;
   - Foi descoberto a fonte do problema que fazia com que uma mesma pessoa aparecesse como duas na API do professor e até mesmo no nosso projeto: GitKraken;
   - No manual de qualidade, no mapa de esforço é para colocar as principais tarefas de cada sub-unidade;
   - Um elemento desistiu da cadeira (Filipe Viana).
     <br><br>

2. **Questões**<br>

   - Professor: Como estamos a conseguir entregar as versões a nível de branches? Resposta: Estamos a trabalhar em branches diferentes. O UI tem um dado branch e o DEV tem um dado branch;
   - Foi questionado ao professor como é que nós, enquanto equipa, vamos conseguir fazer o deploy na máquina do DEI visto que não o podemos alterar durante 15 dias para a outra equipa testar ao qual foi respondido que dá para fazer dois deploys na mesma máquina com portas diferentes;
   - ...<br><br>

3. **Sugestões**

   - Foi sugerido que fossem separados os tópicos dos documentos em vários ficheiros .md quando há muita gente a trabalhar num mesmo documento;
   - Foi sugerido retificar o esforço de trabalho das pessoas que trabalham mais e das que trabalham menos.<br><br>

4. **Outros comentários**<br>
   - Os testes são feitos através do docker e não da máquina do DEI;
   - É desejável manter inalterado o deploy dado à outra turma;
   - O professor mencionou que quando tenta pesquisar o projeto de outra equipa no nosso aparece "NA" porque o projeto da outra turma está privado;
   - O professor analisou o trabalho investido por parte de cada elemento do grupo;
   - O professor referiu que é normal o trabalho não estar distribuído de igual maneira por todos os elementos do grupo;
   - Foi discutido o plano de trabalho imposto aos alunos ao longo do semestre e a falta de comunicação entre as várias sub-equipas;
   - Há um problema que ressurge todos os anos por causa da equipa não apresentar trabalho mas colocar horas de trabalho;
   - É necessário discernir o valor das coisas para o cliente e implementar isso.

## <br><br>

---

## **OBJETIVOS A CONCRETIZAR**

**Projeto**<br>

- [ ] Manual de Requisitos (em desenvolvimento);
- [ ] Manual de Qualidade (em desenvolvimento);
- [ ] Manual de Instalação;
- [ ] Rever os branches e limpar os que estiverem inativos.
      <br><br>

---

## **PARTICIPANTES**

- [x] Professor Doutor Mário Zenha-Rela
- [x] Alexy Almeida<br>
- [x] Ana Martinez<br>
- [x] Duarte Meneses<br>
- [x] Edgar Duarte<br>
- [x] Eva Teixeira<br>
- [ ] Filipe Viana<br>
- [x] Francisco Carreira<br>
- [x] Gonçalo Pimenta<br>
- [x] Gonçalo Lopes<br>
- [x] Inês Marçal <br>
- [x] Joana Antunes<br>
- [x] João Lourenço<br>
- [x] João Catré<br>
- [ ] João Freire<br>
- [x] Luís Braga<br>
- [x] Luís Vieira <br>
- [x] Miguel Barroso<br>
- [x] Patrícia Costa<br>
- [ ] Ricardo Vieira <br>
- [x] Rodrigo Ferreira <br>
- [x] Sofia Alves <br>
- [ ] Sofia Santos <br>
- [x] Sofia Neves<br>
- [x] Tatiana Almeida<br>

Participantes: 20/24

<br>

---

## **DOCUMENTO**

**Autoria:** Sofia Neves<br>
**Recolha de informação:** Sofia Alves, Sofia Neves<br>
**Revisão:** Gonçalo Lopes, João Lourenço
