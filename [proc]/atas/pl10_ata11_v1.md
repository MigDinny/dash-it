# **Ata nº11** | Engenharia de Software PL10 
## 13/12/2021
### Discussão da evolução, estado final do projeto e a contribuição de cada elemento da equipa para a sua realização<br><br>

<div align="justify">

## **ESTADO DO PROJETO**

**Projeto:**<br>
- [ ] Manual de Requisitos;
    - Em desenvolvimento.
- [ ] Manual de Qualidade;
    - Em desenvolvimento.
- [x] Manual de Instalação;
- [x] Rever os branches e limpar os que estiverem inativos. <br>


**Progresso:** 2/4.<br>

---

## **PONTOS DEBATIDOS**

1. **Discussão**<br>
    Durante a última aula, foram abordados os seguintes pontos:
   - Foi questionada, por parte do docente, a atividade desenvolvida por cada um dos elementos da equipa;
        - Toda essa informação encontra-se no ficheiro pessoal de cada elemento, que o atualizou com as suas contribuições, merge-requests e issues.
   - Foram detetados alguns erros que influenciaram a não apresentação do projeto no formato pretendido pelo docente, tendo sido o mais relevante a falta de comunicação entre subunidades (de destacar a falta de comunicação entre os departamentos de UI e de DEV, o que levou à não integração do código de front-end desenvolvido pela primeira);
        - Por esta razão, foi recomendado um desenvolvimento onde ambas as subunidades integravam, sucessivamente, o trabalho desenvolvido por cada uma e não apenas numa fase final - **integração contínua**.
   - Um dos fatores que também foi referido como tendo desencadeado um atraso no desenvolvimento do projeto foi a criação de uma base de dados de raíz quando havia a possibilidade de recorrer à memória e guardar todos os dados, que seriam posteriormente acedidos;
   - Uma questão também abordada foi a necessidade de efetuar testes noutras máquinas, de forma a tornar esse processo mais eficaz e detetar erros que poderiam não ser detetados na nossa máquina: recorrer, por exemplo, ao Docker;
   - Foi criado um estágio intermédio ao nível do qual eram feitos a testagem e a validação do código antes de ser acrescentado à branch principal.

<br>

---

## **OBJETIVOS A CONCRETIZAR**

**Projeto:**<br>
- Tendo sido esta a última aula prática da disciplina (onde foi apresentada a dashboard final ao docente), espera-se apenas a conclusão da documentação necessária a ser entregue até ao dia 18 de dezembro.<br>

---

## **PARTICIPANTES**

- [x] Professor Doutor Mário Zenha-Rela<br>
- [x] Alexy Almeida<br>
- [x] Ana Martinez<br>
- [x] Duarte Meneses<br>
- [x] Edgar Duarte<br>
- [x] Eva Teixeira<br>
- [ ] Filipe Viana<br>
- [x] Francisco Carreira<br>
- [x] Gonçalo Pimenta<br>
- [x] Gonçalo Lopes<br>
- [x] Inês Marçal<br>
- [x] Joana Antunes<br>
- [x] João Lourenço<br>
- [x] João Catré<br>
- [x] João Freire<br>
- [x] Luís Braga<br>
- [x] Luís Vieira <br>
- [x] Miguel Barroso<br>
- [x] Patrícia Costa<br>
- [x] Ricardo Vieira<br>
- [x] Rodrigo Ferreira<br>
- [x] Sofia Alves<br>
- [x] Sofia Santos<br>
- [x] Sofia Neves<br>
- [x] Tatiana Almeida<br>

Participantes: 23/24

<br>

---

## **DOCUMENTO**

**Autoria:** Joana Antunes<br>
**Recolha de informação:** Joana Antunes, Sofia Neves<br>
**Revisão:** Patrícia Costa, Inês Marçal, Duarte Meneses
