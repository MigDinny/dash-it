# **Ata nº1** | Engenharia de Software PL10 

<div align="justify">

No dia 20 de setembro de 2021 realizou-se a primeira aula prática e a primeira reunião do grupo "Dash'it", com início às 14h e 15h, respetivamente. A aula e a reunião decorreram de forma presencial na sala C6.6 do Departamento de Engenharia Informática da Universidade de Coimbra.<br><br>

## **TÓPICOS LECIONADOS**

1. **Introdução à Engenharia de Software**<br>
   Cada turma irá constituir uma equipa para desenvolver um software a ser apresentado a um cliente (o docente da turma).

2. **Ferramentas**<br>
   A linguagem para o desenvolvimento do código deverá ser em Python, utilizando também a framework Django. É necessário utilizar um repositório do GitLab e um canal de comunicação para a equipa.

3. **Principais cargos**<br>
   Foram mencionados os principais cargos do projeto, sendo eles:
   - Gestor de equipa;
   - Gestor de qualidade:
     - Equipa de testes (incide na qualidade do produto);
     - Equipa de qualidade do desenvolvimento (incide na qualidade do processo).
   - Gestor técnico;
   - Gestor de cliente ou Analista de requisitos;
   - Outros (os restantes cargos do projeto não foram ainda abordados).

4. **Dificuldades**<br>
   Foram mencionadas algumas dificuldades da engenharia de software, sendo as principais e mais comuns: o trabalho em equipa e o cumprimento de requisitos do cliente.

5. **Objetivos e tarefas propostas**<br>
   Os principais objetivos e tarefas impostos para a primeira semana foram os seguintes, por ordem cronológica:
   - criar um repositório do GitLab;
   - cada elemento da equipa tem de criar a sua área pessoal no GitLab;
   - dar acesso de leitura do repositório aos quatro docentes da cadeira;
   - criar um canal de comunicação da equipa;
   - decidir o nome do software;
   - distribuir os cargos (conhecidos até ao momento).<br><br>

## **PONTOS DEBATIDOS**

1. **Atribuição de  cargos**<br>
   Os cargos atribuídos até ao momento foram o de Gestor de qualidade, Gestor técnico e dois técnicos secundários, Gestor de cliente e o encarregado das atas. Estes e os restantes cargos serão melhor discutidos depois da aula teórica de quarta-feira do dia 22 de setembro de 2021.

2. **Workflow**<br>
  - O Discord será o principal meio de comunicação, criando-se um servidor para esse efeito. Isso permitirá distinguir os vários cargos da equipa, criar canais de comunicação relativos a determinados trabalhos ou funções, assim como debater ideias, fazer reuniões ou partilhar ficheiros. 
  - Os ficheiros serão disponibilizados no GitLab, tendo os docentes acesso de leitura a esses mesmos. 
  - Cada membro da equipa terá de ter a sua área pessoal no GitLab e estar no servidor de Slack da cadeira de Engenharia de Software.
  - A programação será feita em Python e com recurso à framework Django.
  
3. **Nome do software**<br>
   Cada membro da equipa sugeriu um ou vários nomes para o software, tendo sido "Dash'it" o nome com maior número de votos.<br><br>

A reunião encerrou-se às 16h, havendo, no entanto, necessidade de se realizar uma nova antes da aula prática do dia 27 de setembro de 2021, de forma a tratar dos assuntos que ficaram por ser resolvidos.<br><br>

## **PARTICIPANTES**

[x] Professor Doutor Mário Zenha-Rela

[x] Alexy Almeida<br>
[x] Ana Martinez<br>
[x] Duarte Meneses<br>
[x] Edgar Duarte<br>
[x] Eva Teixeira<br>
[x] Filipe Viana<br>
[x] Francisco Carreira<br>
[x] Gonçalo Pimenta<br>
[x] Gonçalo Lopes<br>
[x] Inês Marçal <br>
[x] Joana Antunes<br>
[x] João Lourenço<br>
[x] João Catré<br>
[x] João Freire<br>
[x] Luís Vieira <br>
[x] Miguel Barroso<br>
[x] Patrícia Costa<br>
[x] Ricardo Vieira <br>
[x] Rodrigo Ferreira <br>
[x] Sofia Alves <br>
[x] Sofia Santos <br>
[x] Sofia Neves<br>
[x] Tatiana Almeida<br><br>


 
## **DOCUMENTO**

Autoria: Alexy de Almeida<br>
Recolha de informação: João Catré, Sofia Alves, Sofia Neves<br>
Revisão: Duarte Meneses, Tatiana Almeida


