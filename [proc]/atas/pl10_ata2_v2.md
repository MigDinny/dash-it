# **Ata nº2** | Engenharia de Software PL10 
## 27/10/2021
### Discussão do estado atual do projeto, progressos feitos e objetivos para a semana seguinte. <br><br>

<div align="justify">


## **ESTADO DO PROJETO**
- [x] Criar repositório GitLab<br>
- [ ] Cada membro da equipa encontra-se no repositório do projeto<br>
- [ ] Dar acesso de leitura do repositório aos quatro docentes da cadeira<br>
- [x] Criar um canal de comunicação<br>
- [x] Distribuir cargos pela equipa<br>
- [x] Atribuir um nome ao projeto<br>
- [ ] Criar issues das tarefas realizadas durante a semana<br>

**Progresso:** 4/7.<br><br>

## **PONTOS DEBATIDOS**

1. **Penalizações**<br>
   - Alguns objetivos propostos na semana anterior não foram cumpridos (ver mais à frente no progresso do projeto) e o grupo foi penalizado em alguns pontos.<br><br>

2. **Discussão**<br>
   - Foi dado acesso ao repositório ao docente da PL, ficando a faltar dar acesso aos restantes docentes;
   - A equipa apresentou os roles existentes e as funções que se desempenhariam em cada um deles, assim como a escolha dos nomes e dos team leaders;
   - Discutiu-se a alteração de roles, tais como:
     - mudar a "Secretaria" para esta passar a integrar a "Gestão de Projeto";
     - alterar "Design" para "UI Design" de forma a evitar confusões e lapsos. 
   - Um assunto que se debateu bastante foi a atribuição de tickets. Cada pessoa criará o seu, definindo-se um **WiP** (número máximo de tickets por elemento), de forma a garantir que as tarefas iniciadas são concluídas (actions done);
   - Mencionou-se que, em média, o tempo limite para resolver uma issue é de 20 minutos, podendo este valor variar bastante consoante a issue em questão (pode ser 5 minutos ou 1h). De forma a controlar melhor o tempo gasto em cada, sugeriu-se usar o **toggl**;
   - Falou-se do **DOD** (definition of done), que consiste em verificar se uma tarefa cumpre os requisitos para ser validada, permitindo garantir que todas as tarefas são bem executadas até ao fim;
   - Também se mencionou que os tickets devem passar o "teste **SMART**":
     - **Specific:** as tarefas são bem definidas e claras;
     - **Measurable:** é possível medir o progresso da tarefa;
     - **Achievable:** a tarefa é fazível;
     - **Realistic:** a tarefa contribui positivamente para o projeto e é realmente essencial;
     - **Timely:** a tarefa tem um prazo, com data de início e de fim, de forma a criar uma sensação de urgência.<br><br>
   
3. **Decisões**<br>
   - Tudo o que diz respeito à avaliação terá que ser posto no GitLab, a 'single source of truth' do projeto;
   - Inicialmente, as tarefas necessitarão de verificação, tendo surgido a ideia de criar alguns testes para correr no fim e ver se, efetivamente, a tarefa se encontra concluída;
   - Se existir uma tarefa que não exija uma intervenção direta no repositório, criamos uma issue e fechamo-la imediatamente;
   - Cada pessoa poderá tratar no **máximo de 3 issues ao mesmo tempo**, de forma a evitar que alguns membros da equipa façam demasiado e outros muito pouco;
   - Será posto ficheiro de guidelines no GitLab com tutoriais sobre o Git, GitLab e Django (destinado apenas à dev team), assim como o procedimento de criação de issues e contribuição. Dessa forma, os membros poderão realizar desde logo as suas primeiras tarefas: **criar uma issue e contribuir com um ficheiro markdown, descrevendo o perfil do membro**. 
   - O envio das atas passará a ser por email, para o docente, na forma de link da pasta onde se encontram as atas.<br><br>


## **OBJETIVOS A CONCRETIZAR**
**Presenças**<br>
- Verificar que todos os membros se encontram no canal do Slack;<br>
- Verificar que todos os membros se encontram no repositório do projeto;<br
- Dar acesso de leitura do repositório aos quatro docentes da cadeira.<br>

**GitLab**<br>
- Criar pastas principais;<br>
- Criar issues das tarefas realizadas durante a semana (menos de 30 é considerado mau);<br>
- Cada membro tem de criar pelo menos um ticket: ficheiro .md com info pessoal (ver template fornecido no GitLab).<br>

**Estrutura**<br>
- Redefinir cargos;<br> 
- Definir como é feito o processo de criação das atas;<br>
- Definir de que forma a equipa se organiza;<br>
- Pedir documentação: template de ata, esqueleto do GitLab e documento de requisitos;<br>
- Finalizar estrutura dos tutoriais de Git, GitHub, GitLab e Django, colocando-os no GitLab, num ficheiro readme;<br>
- Criar template sobre ficheiro .md que cada membro terá de ter no seu perfil GitLab;<br>
- Criar uma página pública com o nome do projeto.<br><br>

## **PARTICIPANTES**

- [x] Professor Doutor Mário Zenha-Rela

- [x] Alexy Almeida<br>
- [x] Ana Martinez<br>
- [x] Duarte Meneses<br>
- [x] Edgar Duarte<br>
- [x] Eva Teixeira<br>
- [x] Filipe Viana<br>
- [ ] Francisco Carreira<br>
- [x] Gonçalo Pimenta<br>
- [x] Gonçalo Lopes<br>
- [x] Inês Marçal <br>
- [x] Joana Antunes<br>
- [x] João Lourenço<br>
- [x] João Catré<br>
- [ ] João Freire<br>
- [x] Luís Braga<br>
- [x] Luís Vieira <br>
- [x] Miguel Barroso<br>
- [x] Patrícia Costa<br>
- [x] Ricardo Vieira <br>
- [x] Rodrigo Ferreira <br>
- [x] Sofia Alves <br>
- [x] Sofia Santos <br>
- [x] Sofia Neves<br>
- [x] Tatiana Almeida<br>

Membros que não integraram o repositório no GitLab até à 2ª aula: 
- Filipe Viana
- João Freire
- Sofia Santos

Um novo membro integrou a equipa: 
- Luís Braga<br><br>

## **DOCUMENTO**

**Autoria:** Alexy de Almeida<br>
**Recolha de informação:** João Catré, Sofia Alves<br>
**Revisão:** Duarte Meneses, Patrícia Costa