# **Ata nº3** | Engenharia de Software PL10 
## 04/10/2021
## Local: C6.6
### Discussão do estado atual do projeto, progressos feitos e objetivos para a semana seguinte. <br><br>

<div align="justify">


## **ESTADO DO PROJETO**
- [x] Cada membro da equipa encontra-se no repositório do projeto<br>
- [x] Dar acesso de leitura do repositório aos quatro docentes da cadeira<br>
- [x] Criar issues das tarefas realizadas durante a semana<br>
- [x] Todos os membros encontram-se no canal do Slack<br>
- [x] Criar pastas principais<br>
- [x] Criar issues das tarefas realizadas durante a semana (menos de 30 é considerado mau)<br>
- [x] Cada membro tem de criar pelo menos um ticket: ficheiro .md com info pessoal (ver template fornecido no GitLab)<br>
- [x] Redefinir cargos<br>
- [ ] Definir como é feito o processo de criação das atas<br>
- [ ] Definir de que forma a equipa se organiza<br>
- [x] Pedir documentação: template de ata, esqueleto do GitLab e documento de requisitos<br>
- [x] Finalizar estrutura dos tutoriais de Git, GitHub, GitLab e Django, colocando-os no GitLab, num ficheiro readme<br>
- [x] Criar template sobre ficheiro .md que cada membro terá de ter no seu perfil GitLab<br>
- [ ] Criar uma página pública com o nome do projeto<br>

**Progresso:** 11/14.<br><br>

## **PONTOS DEBATIDOS**

1. **Discussão**<br>
   - A aula iniciou com um breve esclarecimento de dúvidas;
   - Foi mostrado ao professor todo o trabalho realizado até ao momento:
 	- **Repositório Gitlab:** 
		* ficheiros de perfil criados, bem como os respetivos Tickets;
		* pipeline: foram enumerados alguns erros que podem vir a acontecer quando alguém tentar correr o código na própria máquina;
		* requisitos;
		* Burndown Charts (apesar de ainda não terem dados muito relevantes);
		* local onde incluímos vários tutoriais para promover a aprendizagem coletiva de forma mais eficaz.
	- **Canal de Comunicação:**
		* foi apresentado ao docente o canal de comunicação no DISCORD mostrando que está bem organizado de modo a facilitar a comunicação e o trabalho de equipa.
    - Importância dos **Conventional Commits** como forma a facilitar a leitura/escrita dos commits, tendo um histórico;
    - Definição de **Manual de Qualidade**: local onde iremos ter todos os tutoriais e material de apoio à realização do projeto;
    - Foi salientado pelo docente que a parte de **Desenvolvimento** do projeto apenas ronda os **30%** do tempo de modo a realçar o peso do **Controlo de Qualidade**;
    - Foi debatida a importância de colocar tudo o que é feito no repositório o mais rápido possível, mantendo-o atualizado, de forma a evitar perdas de informação. O período de atualização não deve ser superior a 24h.


2. **Decisões**<br>
   - Os tickets deverão ter features;
   - É de valor ter um Manual de Qualidade no repositório do GitLab.

## **OBJETIVOS A CONCRETIZAR**
**GitLab**<br>
- Alterar o nome do local onde estão contemplados todos os tutoriais para "Manual de Qualidade";
- Disponibilizar tutorial para a construção das atas.

**Projeto**<br>
- Pensar no design de um conjunto de possíveis ecrãs de interação com o utilizador para a Dashboard.

**Estrutura**<br>
- Reforçar a forma como a equipa se organiza;<br>
- Finalizar os tutoriais de Git, GitHub, GitLab e Django, colocando-os no GitLab no Manual de Qualidade;<br>
- Criar uma página pública com o nome do projeto.<br><br>





## **PARTICIPANTES**

- [x] Professor Doutor Mário Zenha-Rela

- [x] Alexy Almeida<br>
- [x] Ana Martinez<br>
- [x] Duarte Meneses<br>
- [x] Edgar Duarte<br>
- [x] Eva Teixeira<br>
- [x] Filipe Viana<br>
- [x] Francisco Carreira<br>
- [x] Gonçalo Pimenta<br>
- [x] Gonçalo Lopes<br>
- [x] Inês Marçal <br>
- [x] Joana Antunes<br>
- [x] João Lourenço<br>
- [x] João Catré<br>
- [x] João Freire<br>
- [x] Luís Braga<br>
- [x] Luís Vieira <br>
- [x] Miguel Barroso<br>
- [x] Patrícia Costa<br>
- [x] Ricardo Vieira <br>
- [x] Rodrigo Ferreira <br>
- [x] Sofia Alves <br>
- [x] Sofia Santos <br>
- [x] Sofia Neves<br>
- [x] Tatiana Almeida<br>

Foram contabilizados 24/24 participantes.

## **DOCUMENTO**

**Autoria:** Gonçalo Lopes<br>
**Recolha de informação:** Gonçalo Lopes, Francisco Carreira<br>
**Revisão:** Duarte Meneses, Filipe Viana
