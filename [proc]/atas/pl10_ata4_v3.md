# **Ata nº4** | Engenharia de Software PL10
## 11/10/2021

### Discussão do estado atual do projeto, progressos feitos e objetivos para a semana seguinte.  <br><br>

<div align="justify">


## **ESTADO DO PROJETO**
**GitLab**<br>
- [x] Alterar o nome do local onde estão contemplados todos os tutoriais para "Manual de Qualidade";<br>
- [x] Disponibilizar tutorial para a construção das atas.<br>

**Projeto**<br>
- [x] Pensar no design de um conjunto de possíveis ecrãs de interação com o utilizador para a Dashboard.<br>
- [x] Dar a conhecer a todos os membros os requisitos a si atribuídos;<br>
- [ ] Criar requisitos e respetivos tickets:<br>
    - 18/24 feitos;<br>
    - 0/24 revistos.<br>

**Estrutura**<br>
- [x] Reforçar a forma como a equipa se organiza;<br>
- [x] Finalizar os tutoriais de Git, GitHub, GitLab e Django, colocando-os no GitLab no Manual de Qualidade;
- [ ] Criar uma página pública com o nome do projeto.<br><br>



**Progresso:** 6/8.<br>

---

## **PONTOS DEBATIDOS**

1. **Discussão**<br>
   - Inicialmente foram debatidas algumas questões relativas aos requisitos disponibilizados na semana passada pelo docente;<br>
   - Como quem lecionou a aula foi o docente José Pereira em vez do habitual docente Mário Rela, foi mostrado o trabalho realizado até ao momento, o que inclui:<br>
        - Funcionamento da atribuição das labels das issues;<br>
        - Membros da equipa e respetivos cargos;<br>
        - Tutoriais presentes na pasta `dash-it/[proc]/tutorials`;<br>
        - Manual de Qualidade;<br>
        - Layouts realizados pela equipa de UI e Requisitos.

2. **Sugestões e Observações**<br>
   No decorrer da aula, foram feitas as seguintes observações:
   - Relativamente à atribuição de requisitos, foi aconselhado que tal tarefa fosse realizada pelo gestor da equipa correspondente;<br>
   - Ter atenção ao cliente, uma vez que este é também o docente das aulas teóricas e práticas;<br>
   - Definir uma pessoa ou um grupo de pessoas para aprender mais aprofundadamente Django. Neste caso, seria a equipa de desenvolvimento;<br>
   - A criação de tutoriais de html é tarefa da equipa de UI;<br>
   - Ter uma lista de requisitos ordenada por prioridade;<br>
   - Relativamente às issues:<br>
      - Usar kanbam;<br>
      - Ter labels mais específicas, i.e., no caso da label "waiting approval" usar outra denominação que explicite melhor o seu significado.<br>


---

## **OBJETIVOS A CONCRETIZAR**
**Arquiteturas**<br>
- Criar uma arquitetura para html;<br>
- Criar um modelo ER;<br>
- Criar uma arquitetura para o código. <br>

**Requisitos, UI**<br>
- Criar o layout para as três primeiras vistas do repositório (página principal, árvore e lista de membros);<br>
- Perceber se é, ou não, necessário incluir a possibilidade de ver os membros inativos (para além dos ativos, requisito já pedido).<br>

**Projeto**<br>
- Criar requisitos e respetivos tickets.

**Estrutura**<br>
- Criar uma página pública com o nome do projeto.<br>

---

## **PARTICIPANTES**

- [x] Docente Doutor José Alexandre Pereira (substituição)<br>
- [x] Alexy Almeida<br>
- [x] Ana Martinez<br>
- [x] Duarte Meneses<br>
- [x] Edgar Duarte<br>
- [x] Eva Teixeira<br>
- [x] Filipe Viana<br>
- [x] Francisco Carreira<br>
- [x] Gonçalo Pimenta<br>
- [x] Gonçalo Lopes<br>
- [x] Inês Marçal <br>
- [x] Joana Antunes<br>
- [x] João Lourenço<br>
- [x] João Catré<br>
- [x] João Freire<br>
- [x] Luís Braga<br>
- [x] Luís Vieira <br>
- [x] Miguel Barroso<br>
- [x] Patrícia Costa<br>
- [x] Ricardo Vieira <br>
- [x] Rodrigo Ferreira <br>
- [x] Sofia Alves <br>
- [ ] Sofia Santos <br>
- [x] Sofia Neves<br>
- [x] Tatiana Almeida<br>

Participantes: 24/25<br>

---

## **DOCUMENTO**

**Autoria:** Sofia Alves<br>
**Recolha de informação:** Sofia Alves, João Freire<br>
**Revisão:** Filipe Viana, Patrícia Costa

