# **Ata nº5** | Engenharia de Software PL10

## **Aula Prática** 

## 18/10/2021

### Conversa sobre verificações e como, efetivamente, verificar se foi verificado.

---

## **ESTADO DO PROJETO**

**Arquiteturas**<br>
- [ ] Criar uma arquitetura para html;<br>
- [x] Criar um modelo ER;<br>
- [ ] Criar uma arquitetura para o código. <br>

**Requisitos, UI**<br>
- [x] Criar o layout para as três primeiras vistas do repositório (página principal, árvore e lista de membros);<br>
- [ ] Perceber se é, ou não, necessário incluir a possibilidade de ver os membros inativos (para além dos ativos, requisito já pedido).<br>

**Projeto**<br>
- [x] Criar requisitos e respetivos tickets.

**Estrutura**<br>
- [ ] Criar uma página pública com o nome do projeto.<br>

**Progresso:** 3/7.<br><br>

## **PONTOS DEBATIDOS**
   * Foram decididas as atividades a ser realizadas até à próxima aula prática, sendo estas o desenvolvimento dos layouts por parte da **equipa de UI** das três vistas consideradas prioritárias pelo docente: a página principal, uma árvore dinâmica onde são apresentados todos os ficheiros do repositório e ainda a lista dos membros da equipa;
   * Explicação das hierarquias/roles definidas na equipa;
   * Explicação de como é que o esquema da base de dados será feito;
   * Explicação dos testes que serão realizados para perceber qual a máquina final a ser utilizada para deploy. Após correr os testes e verificar qual é melhor, essa será escolhida;
   * Conflito de interesses entre gestores e equipa da qualidade (e as complicações que isso acarreta para os developers), visto que os gestores querem entregar o produto e a qualidade quer que este esteja bem ao ser entregue.

---

## **PROBLEMAS E SOLUÇÕES**

1. **Como se altera a estrutura da base de dados da produção, ou seja, meter no branch principal?**  
   * Manualmente, atualizar o ficheiro sql e dar upload;
   * Correr um script para parar a base de dados de staging e reiniciar, fazendo os testes com dados reais;
   * É preciso usar SEMPRE as mesmas versões de, por exemplo, base de dados para que haja compatibilidade ao trabalhar em diferentes máquinas.

2. **Como saber se é necessário atualizar a Cache?**  
   * O GitLab tem uma "ferramenta", WebHooks, que dá ping para o nosso servidor. Estando correto, atualiza-se manualmente.
   
   
3. **Transição de Arquitetura para ESTRUTURA de HTML**
   * Permite, inicialmente, estruturar a página, recorrendo de seguida ao CSS para fazer o seu "embelezamento";
   * A estrutura inicial não permite à equipa de desenvolvimento colocar os dados em sítios diferentes, evitando erros. Essa estruturação é de extrema importância para a equipa de development, dado que, sem a mesma, é muito difícil fazer uma transição dos dados do backend para o frontend.

---

## **OBJETIVOS A CONCRETIZAR**

1. Definir urgentemente (de preferência com um diagrama, para fácil compreensão visual) o que cada role "controla"/está responsável por;
2. Colocar os diferentes documentos no síto correto, existindo uma orientação conforme as extensões. Por exemplo, se for .py, dá para perceber que é código, pelo que basta uma pequena linha a explicar. Se for .md ou .txt já é necessário estar na pasta correta e o nome desta terá de ser sugestivo para que facilmente se compreenda o que lá está;
3. Inferir, conforme as issues completas que aparecem no perfil do git, qual a role do membro da equipa. Deste modo, será possível verificar se, conforme o trabalho realizado, a role se confirma ou não;
4. Alguém na equipa de desenvolvimento (DevTeam) terá de tratar do deploy para o Docker;
5. Foi pedido para que a equipa de requisitos tenha todos os tickets fechados de forma a estes não serem mais alterados ou, no máximo, a terem pequeníssimas alterações;
6. Colocar vídeos dos user stories à parte para que estejam mais acessíveis;
7. Criar uma arquitetura para html;
8. Criar uma arquitetura para o código;
9. Perceber se é, ou não, necessário incluir a possibilidade de ver os membros inativos (para além dos ativos, requisito já pedido);
10. Criar uma página pública com o nome do projeto.

  
---

## **PARTICIPANTES**

- [x] Alexy Almeida
- [ ] Ana Martinez
- [x] Duarte Meneses
- [x] Edgar Duarte
- [x] Eva Teixeira
- [x] Filipe Viana
- [x] Francisco Carreira
- [x] Gonçalo Pimenta
- [x] Gonçalo Lopes
- [x] Inês Marçal 
- [x] Joana Antunes
- [x] João Lourenço
- [x] João Catré
- [ ] João Freire
- [x] Luís Braga
- [x] Luís Vieira 
- [x] Miguel Barroso
- [x] Patrícia Costa
- [x] Ricardo Vieira 
- [x] Rodrigo Ferreira 
- [x] Sofia Alves 
- [ ] Sofia Santos 
- [x] Sofia Neves
- [x] Tatiana Almeida

Participantes: 21/24

---

## **DOCUMENTO**

**Autoria:** Francisco Carreira<br>
**Recolha de informação:** Francisco Carreira, João Catré<br>
**Revisão:** Duarte Meneses, Gonçalo Lopes<br>
