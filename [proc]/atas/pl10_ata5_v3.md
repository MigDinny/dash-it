# **Ata nº5** | Engenharia de Software PL10
## 18/10/2021
### Discussão do estado atual do projeto, progressos feitos e objetivos para a semana seguinte. <br><br>

<div align="justify">


## **ESTADO DO PROJETO**

**Arquiteturas**<br>
- [ ] Criar uma arquitetura para html;<br>
- [x] Criar um modelo ER;<br>
- [ ] Criar uma arquitetura para o código. <br>

**Requisitos, UI**<br>
- [x] Criar o layout para as três primeiras vistas do repositório (página principal, árvore e lista de membros);<br>
- [ ] Perceber se é, ou não, necessário incluir a possibilidade de ver os membros inativos.<br>

**Projeto**<br>
- [x] Criar requisitos e respetivos tickets; <br>
- [ ] Fazer o deploy da página pública com o nome do projeto num ambiente virtual.<br>

**Progresso:** 3/7.<br><br>

---

## **PONTOS DEBATIDOS**

1. **Discussão**<br>
Durante a última aula, foram abordados os pontos:
   - Exposição das vistas até agora realizadas pela equipa de UI;<br>
   - Esclarecimento da hierarquia e dos cargos definidos dentro da equipa;<br>
   - Exposição dos ficheiros relativos à distribuição dos requisitos;<br>
   - Explicação do modelo ER da base de dados;<br>
   - Explicação dos testes que serão realizados para perceber qual a máquina final a ser utilizada para deploy;<br> 
   - Esclarecimento sobre como atualizar a estrutura das tabelas SQL através do Git. <br>

2. **Sugestões e Observações**<br>
   No decorrer da aula, foram feitas as seguintes observações:
   - Da parte da equipa de qualidade, foi recomendado que estes, relativamente à criação das UST, mantivessem registado o número de tickets que foram criados e, dentro destes, quantos é que foram criados corretamente, incorretamente e quantos é que foram corrigidos, para que se possa fazer uma análise destas estatísticas e identificar as causas de possíveis erros;<br>
   - Foi sugerido fazer o deploy do projeto no Docker;<br> 
   - Foi proposto a utilização do WebHooks para manter a base de dados atualizada em tempo real com o GitLab;<br>

---   

## **OBJETIVOS A CONCRETIZAR**

**Estruturais:**<br>
- Criar um organograma com os elementos da equipa e os respetivos cargos;<br>
- Definir as prioridades dos requisitos a implementar.<br>

**Arquiteturas:**<br>
- Criar uma arquitetura para html;<br>
- Criar uma arquitetura para o código.<br>

**Projeto:**
- Fazer o deploy da página pública na máquina virtual do DEI.<br>

---

## **PARTICIPANTES**

- [x] Alexy Almeida<br>
- [ ] Ana Martinez<br>
- [x] Duarte Meneses<br>
- [x] Edgar Duarte<br>
- [x] Eva Teixeira<br>
- [x] Filipe Viana<br>
- [x] Francisco Carreira<br>
- [x] Gonçalo Pimenta<br>
- [x] Gonçalo Lopes<br>
- [x] Inês Marçal<br>
- [x] Joana Antunes<br>
- [x] João Lourenço<br>
- [x] João Catré<br>
- [ ] João Freire<br>
- [x] Luís Braga<br>
- [x] Luís Vieira <br>
- [x] Miguel Barroso<br>
- [x] Patrícia Costa<br>
- [x] Ricardo Vieira<br> 
- [x] Rodrigo Ferreira<br> 
- [x] Sofia Alves<br> 
- [ ] Sofia Santos<br> 
- [x] Sofia Neves<br>
- [x] Tatiana Almeida<br>

Participantes: 21/24

<br>

---

## **DOCUMENTO**

**Autoria:** Francisco Carreira<br>
**Recolha de informação:** Francisco Carreira, João Catré<br>
**Revisão:** Duarte Meneses<br>
**Reescrita:** Sofia Alves <br>
