# **Ata nº6** | Engenharia de Software PL10 
## 08/11/2021
### Discussão da evolução e estado atual do projeto<br><br>

<div align="justify">

## **ESTADO DO PROJETO**

**Estruturais:**<br>
- [x] Criar um organograma com os elementos da equipa e os respetivos cargos;<br>
- [x] Definir as prioridades dos requisitos a implementar.<br>

**Arquiteturas:**<br>
- [x] Criar uma arquitetura para html;<br>
- [x] Criar uma arquitetura para o código.<br>

**Projeto:**<br>
- [ ] Fazer o deploy da página pública na máquina virtual do DEI.<br>
    - O deploy foi feito, mas ainda não na máquina virtual do DEI.<br>



**Progresso:** 4/5.<br>

---

## **PONTOS DEBATIDOS**

1. **Discussão**<br>
    Durante a última aula, foram abordados os pontos:
   - Exposição dos modelos de arquitetura do projeto;
   - Apresentação do deploy da página pública do projeto;
   - Listagem dos requisitos e apresentação da prioridade associada a cada um;
   - Discussão do peso associado a cada um dos requisitos, definido pela equipa de desenvolovimento;
        - Baseado na sequência de Fibonacci, cada unidade corresponde a 20 minutos e o peso máximo (em situações excecionais) corresponde a 34 unidades (11 horas).
   - Apresentação do organograma da equipa;
   - Apresentação de um issue destinado à equipa de desenvolvimento e que permitiu a confirmação dos seus conhecimentos na linguagem a utilizar, Django.


<br>

2. **Sugestões e Observações**<br>
    No decorrer da aula, foram feitas as seguintes observações:
   - Relativamente aos documentos apresentados no formato pdf (ficheiros binários), foi recomendada a sua substituição por ficheiros de imagem ou de texto: apenas estes podem ser comparados posteriormente linha a linha;
   - Foi questionada a necessidade de utilizar o método DoD aquando do fecho de issues, tendo sido considerado demasiado rigoroso e, portanto, opcional de ser utilizado;
   - Ao nível dos requisitos, foi apontado o facto de a prioridade de cada um surgir em dois locais distintos: no título e numa label.
        - "Don't repeat yourself": o facto de a mesma informação surgir em dois locais diferentes pode levar a que não seja totalmente atualizada aquando de uma qualquer mudança;
        - Para além disso, as labels são consideradas mais visíveis;
    - Relativamente às vistas inicialmente criadas pela equipa de UI, foi questionado como seria possível listar os elementos de cada uma das equipas: conclui-se que a melhor forma de o fazer seria basear-se nas pastas que um dado elemento alterava e/ou nos issues que realizava;
    - Foi decidida a remoção de um dos requisitos considerados inicialmente e que se baseava na permissão de eliminação de documentos, apenas por parte de administradores:
        - O projeto a desenvolver permite apenas a visualização do respositório, mas não a sua alteração por parte do utilizador.

<br>

---

## **OBJETIVOS A CONCRETIZAR**

**Arquiteturas:**<br>
- Melhorar/alterar as arquiteturas criadas para o projeto;<br>

**Estruturais:**<br>
- Colocar o Manual de Qualidade na pasta [proc] e atualizar o documento Read Me da página inicial do repositório;<br>

**Projeto:**<br>
- Disponibilizar as vistas atualizadas no repositório.<br>

---

## **PARTICIPANTES**

- [x] Professor Doutor Mário Zenha-Rela
- [x] Alexy Almeida<br>
- [x] Ana Martinez<br>
- [x] Duarte Meneses<br>
- [x] Edgar Duarte<br>
- [x] Eva Teixeira<br>
- [x] Filipe Viana<br>
- [x] Francisco Carreira<br>
- [x] Gonçalo Pimenta<br>
- [x] Gonçalo Lopes<br>
- [x] Inês Marçal <br>
- [x] Joana Antunes<br>
- [ ] João Lourenço<br>
- [x] João Catré<br>
- [ ] João Freire<br>
- [x] Luís Braga<br>
- [ ] Luís Vieira <br>
- [x] Miguel Barroso<br>
- [x] Patrícia Costa<br>
- [ ] Ricardo Vieira <br>
- [x] Rodrigo Ferreira <br>
- [x] Sofia Alves <br>
- [x] Sofia Santos <br>
- [x] Sofia Neves<br>
- [x] Tatiana Almeida<br>

Participantes: 20/24

<br>

---

## **DOCUMENTO**

**Autoria:** Joana Antunes<br>
**Recolha de informação:** Joana Antunes, Inês Marçal<br>
**Revisão:** Gonçalo Lopes, Filipe Viana