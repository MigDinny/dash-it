# **Ata nº7** | Engenharia de Software PL10 
## 15/11/2021
### Discussão da evolução e estado atual do projeto<br><br>

<div align="justify">

## **ESTADO DO PROJETO**

**Estruturais:**<br>
- [x] Colocar Manual de Requisitos no GitLab.
    - O Manual de Requisitos já se encontra no GitLab;
- [x] Colocar Manual de Requisitos no GitLab. <br>
    - O Manual de Requisitos já se encontra no GitLab; <br>
    - Cada parte foi distribuida pela equipa de requisitos às diferentes equipas.<br>
    - Link: https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md <br>

**Arquiteturas:**<br>
- [ ] Melhorar/alterar as arquiteturas criadas para o projeto;<br>
    - Arquitetura de Software: https://gitlab.com/MigDinny/dash-it/-/blob/dev/arch/diagramas/arquitetura%20software.png <br>
    - Arquitetura python django: https://gitlab.com/MigDinny/dash-it/-/blob/dev/arch/diagramas/arquitetura%20python%20django.png <br>

**Projeto:**<br>
- [ ] Fazer o deploy da página pública na máquina virtual do DEI.<br>
- [ ] Fazer o deploy da página pública na máquina virtual do DEI. 
    >[MZR:Link?]
    - O deploy encontra-se feito, mas ainda não na máquina virtual do DEI.<br>
- [x] Disponibilizar as vistas feitas no repositório.<br>



**Progresso:** 2/4.<br>

---

## **PONTOS DEBATIDOS**

1. **Discussão**<br>
    - Foi mencionada a revisão do diagrama Entidade-Relacionamento, com as várias equipas, sendo estas: equipa de development, equipa de requisitos e a equipa de qualidade;
        - A primeira versão do diagrama não se encontrava correta, nomeadamente no que toca às relações entre entidades. Foi refeito e, no momento de escrita desta ata, a segunda versão do diagrama encontra-se na fase de avaliação pela equipa de qualidade, requisitos e development;
    - Os models para o django encontra-se a ser desenvolvidos por elementos da equipa de development ao mesmo tempo que o ER;
    - O deploy que vai ser feito na cloud do DEI encontra-se em desenvolvimento;
    - As vistas cobrem metade dos requitos, o weight só existe para aqueles em que existe vista.
    - As vistas cobrem metade dos requisitos, o weight só existe para aqueles em que existe vista.
    - Foi pedido ao docente que fosse disponibilizado um manual de requisitos no Slack, ao que recebemos resposta afirmativa.
    - Foi referido que os próprios testes são documentação devido à existência de comentários;
        - A documentação inconscistente, isto é, alteração de código e sem alterações na documentação, poderá ser um problema, daí se querer implementar um gerador automático de documentação a partir de tags. Esta solução aparenta ser muito mais eficiente.

        - A documentação inconsistente, isto é, alteração de código e sem alterações na documentação, poderá ser um problema, daí se querer implementar um gerador automático de documentação a partir de tags. Esta solução aparenta ser muito mais eficiente. 
        >[MZR:check pydoc or Sphinx] 

<br>

2. **Sugestões e Observações**<br>
    - Toda a equipa deveria trabalhar no development e realizar trabalho em todos os cargos existentes
    - Todos na equipa devem implementar pelo menos um requisito. 
        - Antes de pegar no requisito tem de passar no teste criado para testar os conhecimentos de Django. O docente sugeriu que apenas os que desejam fazer a implementação é que o deveriam fazer;
        - Será que se justifica que todos façam isso? 
                - Pode ainda atrasar mais a equipa;
                - A equipa de testes tem que saber o que se vai testar, por isso vai ter de estar incluída;
                - Não é uma decisão linear serem todos.
        - Foi pedido pelo docente que não fossem entregues objetivos incompletos, visto estes não serem utilizáveis.
    - O manual é um pouco geral, se algum elemento do grupo não achar que a sua parte se adequa ao projeto, retira-se e asssim, efetua-se escabilidade;
    - O manual é um pouco geral, se algum elemento do grupo não achar que a sua parte se adequa ao projeto, retira-se e assim, efetua-se escabilidade;
   

<br>

---

## **OBJETIVOS A CONCRETIZAR**

**Arquiteturas:**<br>
- Melhorar/alterar as arquiteturas criadas para o projeto;<br>
- Melhorar/alterar as arquiteturas criadas para o projeto; <br>
    - Arquitetura de Software: https://gitlab.com/MigDinny/dash-it/-/blob/dev/arch/diagramas/arquitetura%20software.png <br>
    - Arquitetura python django: https://gitlab.com/MigDinny/dash-it/-/blob/dev/arch/diagramas/arquitetura%20python%20django.png <br>

**Estruturais:**<br>
- Atualizar o documento "Read Me" da página inicial do repositório;<br>
    - Link: https://gitlab.com/MigDinny/dash-it/-/blob/dev/README.md <br>

**Projeto:**<br>
- Documentos a entregar: 
    - Manual de requisitos;
    - Manual de qualidade;
    - README do gitlab;
    - how-to-install (manual de instalação, instalar num servidor externo);
    - A arquitetura;
    - Plano de testes.<br>
- Fazer o deploy da página pública na máquina virtual do DEI.<br>
    - O deploy está feito, mas ainda não na máquina virtual do DEI.<br>
---

## **PARTICIPANTES**

- [x] Docente Doutor Mário Zenha-Rela<br>
- [x] Alexy Almeida<br>
- [x] Ana Martinez<br>
- [x] Duarte Meneses<br>
- [x] Edgar Duarte<br>
- [x] Eva Teixeira<br>
- [ ] Filipe Viana<br>
- [x] Francisco Carreira<br>
- [x] Gonçalo Pimenta<br>
- [x] Gonçalo Lopes<br>
- [x] Inês Marçal <br>
- [x] Joana Antunes<br>
- [x] João Lourenço<br>
- [x] João Catré<br>
- [x] João Freire<br>
- [x] Luís Braga<br>
- [x] Luís Vieira <br>
- [x] Miguel Barroso<br>
- [x] Patrícia Costa<br>
- [x] Ricardo Vieira <br>
- [x] Rodrigo Ferreira <br>
- [x] Sofia Alves <br>
- [ ] Sofia Santos <br>
- [x] Sofia Neves<br>
- [x] Tatiana Almeida<br>

Participantes: 22/24

<br>
---

## **DOCUMENTO**

**Autoria:** Francisco Carreira<br>
**Recolha de informação:** Francisco Carreira, Inês Marçal<br>
**Revisão:** Gonçalo Lopes, Patrícia Costa, Tatiana Almeida<br>
