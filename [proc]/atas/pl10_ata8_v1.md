# **Ata nº8** | Engenharia de Software PL10 
## 22/11/2021
### Discussão da evolução e estado atual do projeto<br><br>

<div align="justify">

## **ESTADO DO PROJETO**

**Estruturais:**<br>
- [x] Atualizar o documento “Read Me” da página inicial do repositório;<br>

**Arquiteturas:**<br>
- [x] Melhorar/alterar as arquiteturas criadas para o projeto;<br>
	- Como estas arquiteturas foram aprovadas pelo docente, a sua alteração foi considerada desnecessária.<br>

**Projeto:**<br>
- [x] Fazer o deploy da página pública na máquina virtual do DEI;<br>
- [ ] Manual de Requisitos;
- [ ] Manual de Qualidade;
- [ ] Manual de Instalação;
- [ ] Plano de Testes.

**Progresso:** 3/7.<br>

---

## **PONTOS DEBATIDOS**

1. **Discussão**<br>
    Durante a última aula, foram abordados os pontos:
   - Apresentação do deploy da página pública do projeto no servidor do DEI;
   - Descrição do plano para o desenvolvimento dos requisitos prioritários (prioridade = must): foram definidas as vistas por parte da equipa de UI. A cada equipa de desenvolvimento foi atribuida uma vista para poder começar a trabalhar. <br>
        - A equipa inicial de desenvolvimento foi dividida em pares distribuídos por cada equipa. As vistas foram repartidas pelas equipas, de forma a permitir uma melhor e mais eficiente comunicação entre todos os elementos.<br>
   - Para além dos elementos da equipa de desenvolvimento inicial, foi dada a oportunidade aos restantes de escolherem uma das opções: fazer parte da <u>equipa de testes</u> ou da <u>equipa de desenvolvimento</u>:
        - Equipa de desenvolvimento: 4 elementos + 6 elementos (equipa original);
        - Equipa de testagem: 5 elementos (de 6 vagas);
        - UI: 6 elementos;
   - Foram discutidas as mudanças feitas ao nível de <b>questões estruturais</b> do projeto:
        - Apesar de, inicialmente, todos os elementos da equipa terem criado um requisito, como estes não estavam explicados da melhor forma, não foi fácil à equipa de desenvolvimento (que nunca tinha analisado os requisitos anteriormente) entendê-los. Isto criou dificuldade à equipa em questão em perceber os pontos a implementar;
        - Ainda relativamente aos requisitos, concluiu-se que a issue deve ser criada apenas quando o requisito está a ser trabalhado: as etapas a serem realizadas são criadas pelos desenvolvedores responsáveis;
            - Os primeiros requisitos a serem implementados são aqueles com maior prioridade, uma vez que seria tempo perdido começar pelos de menor importância caso o cliente decidisse mudar de ideias a meio do projeto (já que os de menor importância têm maior probabilidade de ser descartados);
        - Foi pedido à equipa de gestão que os requisitos surgissem associados às vistas: <b>à equipa de desenvolvimento interessa os requisitos do produto (funcionalidades) e não os do cliente.</b>

<br>

2. **Sugestões e Observações**<br>
    No decorrer da aula, foram feitas as seguintes observações:
    - Representação do número da versão que está a ser visualizada naquele momento na página;
    - Criação de um branch <b>stage</b> onde são realizados todos os testes antes de uma funcionalidade ser adicionada ao branch principal;
    - A autenticação foi considerado um aspeto não prioritário;
    - Login e autenticação foram considerados o mesmo requisito;
    - Foi definido que, a equipa que está a trabalhar numa respetiva vista, mudaria para outra assim que todos os requisitos must estivessem concluídos - de realçar a importância de analisar a cadência da produção das vistas e dos testes feitos, de forma a evitar constrangimentos no desenvolvimento.

<br>

---

## **OBJETIVOS A CONCRETIZAR**

**Estruturais:**<br>
- Desenvolver os Manuais de Requisitos, de Instalação e de Qualidade.<br>

**Projeto:**<br>
- Desenvolver o Plano de Testes.<br>

---

## **PARTICIPANTES**

- [x] Professor Doutor Mário Zenha-Rela
- [x] Alexy Almeida<br>
- [x] Ana Martinez<br>
- [x] Duarte Meneses<br>
- [x] Edgar Duarte<br>
- [x] Eva Teixeira<br>
- [x] Filipe Viana<br>
- [x] Francisco Carreira<br>
- [x] Gonçalo Pimenta<br>
- [x] Gonçalo Lopes<br>
- [x] Inês Marçal <br>
- [x] Joana Antunes<br>
- [x] João Lourenço<br>
- [x] João Catré<br>
- [ ] João Freire<br>
- [x] Luís Braga<br>
- [x] Luís Vieira <br>
- [x] Miguel Barroso<br>
- [x] Patrícia Costa<br>
- [x] Ricardo Vieira <br>
- [x] Rodrigo Ferreira <br>
- [x] Sofia Alves <br>
- [x] Sofia Santos <br>
- [x] Sofia Neves<br>
- [x] Tatiana Almeida<br>

Participantes: 23/24

<br>

---

## **DOCUMENTO**

**Autoria:** Joana Antunes<br>
**Recolha de informação:** Joana Antunes, Patrícia Costa<br>
**Revisão:** Inês Marçal, Duarte Meneses
