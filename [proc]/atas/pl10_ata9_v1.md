# **Ata nº9** | Engenharia de Software PL10 
## 29/11/2021
### Discussão da evolução e estado atual do projeto<br><br>

<div align="justify">

## **ESTADO DO PROJETO**

**Estruturais:**<br>
- [ ] Manual de Requisitos (em desenvolvimento);
- [ ] Manual de Qualidade (em desenvolvimento);
- [ ] Manual de Instalação;
- [ ] Plano de Testes. <br>



**Progresso:** 0/4.<br>

---

## **PONTOS DEBATIDOS**

1. **Discussão**<br>
    Durante a última aula, foram abordados os pontos:
   - Apresentação dos mockups, ao que o professor disse que não é explicado o que se faz e consequentemente o leitor tem de adivinhar o que cada coisa faz; 
   - Falta um mapa de navegação para saber o que está a ser mostrado; 
   - Apresentação dos avanços desta semana; 
   - Foi perguntado se já sabiamos o que ia ser entregue na deadline do próximo dia 4 de Novembro, ao que foi respondido que não. Contudo definimos prioridades para cada um dos requisitos e começamos por impletamenter os que têm a prioridade ''MUST''.É nesses que nos pretendemos focar para a primeira entrega;
   - Relativamente aos testes, a equipa destes foi definida e quem assumiu o papel de gestor de testes foi o gestor de qualidade; a equipa é constítuida por seis pessoas;
   - Foi esclarecido ao professor que já se tinha feito a atribuição das vistas para a criação do plano de testes; 
   - Um dos trabalhos do gestor de projeto é que não haja um estrangulamento, isto é, uma equipa de testagem não pode estar sobrecarregada e a outra não ter nada para fazer por causa das subdivisões da equipa de desenvolvimento;
   - É de boa prática ter maneiras de trabalhar consistentes, neste caso, a subdivisão de equipas tanto no desenvolvimento como na testagem;
   - Relativamente ao plano de testes foi dito o seguinte:
      - Validar se a expectativa de produção de código estável bate certo com a equipa de desenvolvimento; 
      - Criar outra branch para o UI e DEV testing (para trabalhar apenas em código estável);
      - Quando existirem erros tem de ser comunicados à equipa DEV.
   - Todos os branches foram vistos;
   - Foi sugerido para se dar uma revisão nos mesmos pois existem alguns inativos.

<br>

2. **Sugestões e Observações**<br>
    No decorrer da aula, foram feitas as seguintes observações:
    - A branch main não deve estar abandonada e passar o que temos na DEV para o main;
    - Depois de passar nos testes passar a UI para outra branch;
    - O desenvolvimento tem de se decompor em objetivos mais concretos;
    - Prática habitual para a UI team: desenvolver os designs numa ferramenta que automatize o css logo como por exemplo o figma.
    
<br>

---

## **OBJETIVOS A CONCRETIZAR**

**Estruturais:**<br>
- Desenvolver os Manuais de Requisitos, de Instalação e de Qualidade;<br>
- Rever os branches e limpar os que estiverem inativos. <br>

**Projeto:**<br>
- Desenvolver o Plano de Testes;<br>
- Voltar a fazer o documento dos requisitos para ser mais discriminado;<br>
- Criação de um mapa de navegação para saber o que está a ser mostrado. 


---

## **PARTICIPANTES**

- [x] Professor Doutor Mário Zenha-Rela
- [x] Alexy Almeida<br>
- [x] Ana Martinez<br>
- [x] Duarte Meneses<br>
- [x] Edgar Duarte<br>
- [x] Eva Teixeira<br>
- [ ] Filipe Viana<br>
- [x] Francisco Carreira<br>
- [x] Gonçalo Pimenta<br>
- [x] Gonçalo Lopes<br>
- [x] Inês Marçal <br>
- [x] Joana Antunes<br>
- [x] João Lourenço<br>
- [x] João Catré<br>
- [x] João Freire<br>
- [x] Luís Braga<br>
- [x] Luís Vieira <br>
- [x] Miguel Barroso<br>
- [x] Patrícia Costa<br>
- [ ] Ricardo Vieira <br>
- [x] Rodrigo Ferreira <br>
- [x] Sofia Alves <br>
- [x] Sofia Santos <br>
- [x] Sofia Neves<br>
- [x] Tatiana Almeida<br>

Participantes: 22/24

<br>

---

## **DOCUMENTO**

**Autoria:** João Freire<br>
**Recolha de informação:** Inês Marçal, João Freire<br>
**Revisão:** Gonçalo Lopes, Filipe Viana 
