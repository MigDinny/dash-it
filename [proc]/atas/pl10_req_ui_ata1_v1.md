# **Ata reunião nº1** | Engenharia de Software PL10

## **Requisitos, UI** 

## 06/10/2021 via Zoom

### Foi analisado o documento com os requisitos e foram estabelecidos os objetivos a serem cumpridos até à aula prática seguinte

---

## **PONTOS DEBATIDOS**
   * Foram decididas as atividades a serem realizadas até à aula prática da semana seguinte, sendo estas o desenvolvimento dos layouts, por parte da **equipa de UI**, das três vistas consideradas prioritárias pelo docente: a página principal, uma árvore dinâmica onde são apresentados todos os ficheiros do repositório e ainda a lista dos membros da equipa;

   * Foi analisado o documento fornecido pelo docente e, a partir deste, foram identificados os requisitos a serem realizados por cada um dos elementos da equipa correspondente. Na determinação da ordem de realização dos requisitos foram considerados a complexidade e prioridade de cada um.

---

## **PROBLEMAS E SOLUÇÕES**

1. **Atribuição de requisitos**  
   * Depois de confirmar com o docente, conclui-se que a atribuição de requisitos deveria ser assegurada pela equipa de requisitos, o que foi feito aquando da análise do documento disponibilizado com a sua listagem;
   * Cada um destes requisitos foi identificado e atribuído a um elemento da equipa correspondente, de acordo com a sua posição na lista de alunos por ordem alfabética.

2. **Criação de requisitos**  
   * Detetada a funcionalidade do GitLab que permite criar requisitos, foi criado um como exemplo, seguindo algumas regras estruturais baseadas no **user-story**: cada requisito, quando formulado, terá que responder às perguntas "Quem quer", "O quê" e "Porquê";
   * No exemplo criado, estão presentes as formas gerais de definição do título de cada requisito: uma frase do tipo UST (onde são respondidas as perguntas referidas anteriormente) e a numeração das suas etapas.

---

## **OBJETIVOS A CONCRETIZAR**

1. Criar o layout para as três primeiras vistas do repositório (página principal, árvore e lista de membros);
2. Dar a conhecer a todos os membros os requisitos a si atribuídos;
3. Perceber se é, ou não, necessário incluir a possibilidade de ver os membros inativos (para além dos ativos, requisito já pedido);
  
---

## **PARTICIPANTES**

* [ ] Ana Martinez  
* [x] Eva Teixeira  
* [x] Gonçalo Pimenta  
* [x] Joana Antunes  
* [ ] João Lourenço  
* [ ] Luís Braga
* [ ] Luís Vieira  
* [x] Ricardo Vieira   
* [ ] Sofia Santos   
* [x] Sofia Neves  
* [x] Tatiana Almeida  

Participantes: 6/11

---

## **DOCUMENTO**

**Autoria:** Joana Antunes  
**Recolha de informação:** Joana Antunes  
**Revisão:** Duarte Meneses, Tatiana Almeida
