# **ATAS**

## Procedimento
**-- Antes da aula --**
1. (GESTÃO + QUALIDADE) Selecionar os membros encarregues de recolher informação + escrever + rever, no Discord;
2. (GESTÃO) Dar tag aos membros indicados no canal "atas_info";

**-- Durante a aula --**
1. (RECOLHEDOR + ESCRITOR) Recolher informação;<br>
> **IMPORTANTE:** Quem escrever ou recolher info e atrasar-se à aula mais de 10 minutos depois do prof chegar leva um aviso. Ao segundo aviso deixa de poder participar nessas tarefas;<br>
2. (ESCRITOR) Começar a escrever a ata;<br>

**-- Após a aula --**
1. (ESCRITOR)   Criar ticket;<br>
**ATÉ ÀS 20H:**
2. (ESCRITOR)   Escrever a ata (.md);<br>
3. (ESCRITOR)   Enviar a ata em (.md) aos membros da qualidade (ver canal "atas_info" para saber a quem enviar);<br>
**ATÉ ÀS 22H:**
4. (QUALIDADE)  Rever a ata e mandar pedido de alterações;
4. (ESCRITOR)   Aplicar alterações, reenviar (.md) à equipa de qualidade;
5. (QUALIDADE)  Publicar a ata (.pdf), no canal "atas_discussion", para ser feita a revisão e discussão acerca da mesma por todos os membros;<br>
**ATÉ ÀS 11H, TERÇA-FEIRA**
6. (GESTÃO)  Fazer upload da ata (.md, versão mais recente) na pasta das atas;<br>
7. (GESTÃO)  Enviar email para o docente da pl com um link para a ata.<br>
> **Nota:** Usar [este site](https://md2pdf.netlify.app/) para converter md para pdf.

**-- Após revisão do docente --**
1. (GESTÃO) Mandar alterações exigidas pelo docente ao escritor;
2. (ESCRITOR) Aplicar correções e enviar (.md) à equipa de qualidade;
3. (QUALIDADE) Rever ata e corrigir últimos erros (se aplicável);
4. (QUALIDADE) Enviar ata (.md) à equipa de gestão;
5. (GESTÃO) Fazer upload e enviar email;

<br>

## **Exemplos formatos de filename**
- Ata de aula
  - pl10_ata2_v2.md
  - pl10_ata10_v1.md

> Nota: o nº da ata é sempre cumulativo, isto é, uma ata de aula que seja a nº7, se a próxima ata será de ui/req então passará a ser a ata nº8

<br>

## **Formato do ticket**
  - Título: "Redigir ata x" (onde x é o nº da ata)
  - Descrição: escolher o template "Ata"
  - Dar sempre assign a todos os envolventes da ata (quem recolhe info, quem escreve, quem revê, quem envia ao docente)
  - Deadline para o dia seguinte (24h)
  - Labels: documentation, work-in-progress (alterar consoante o estado do ticket)

<br>

## **Outras indicações**

- 2 pessoas a recolher informação na aula;
> Nota: pode haver backup caso alguma das pessoas não esteja presente, mas é de evitar!<br>

- Quem escreve a ata ambém recolhe informação;<br>

- Quem escreve tem de tentar seguir o mais parecido possível a estrutura e formatação das duas primeiras atas, visto que ter consistência facilita o trabalho de quem vai escrever, rever e ler (podem alterar algumas coisas livremente desde que não seja nada excessivo);<br>

- Existe rotatividade para todos os interessados participarem nas tarefas das atas.<br>

