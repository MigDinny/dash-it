# **Ata nºx** | Engenharia de Software PL10 
## dd/mm/yyyy
### Resumo da aula/reunião numa frase <br><br>

<div align="justify">


## **ESTADO DO PROJETO**
- [x] Objetivo 1 da semana anterior<br>
- [x] Objetivo 2 da semana anterior<br>
- [ ] Objetivo 3 da semana anterior<br>
- [ ] (ir acrescentado todos os objetivos da ata anterior)

**Progresso:** objetivos cumpridos/objetivos totais.

<br>

---

## **PONTOS DEBATIDOS**

1. **Tópico 1**<br>
   - Explicar ponto nº1
   - Explicar ponto nº2
   - ...<br><br>

2. **Tópico 2**<br>
   - Explicar ponto nº1
   - Explicar ponto nº2
   - ...<br><br>
3. **Categoria ...**<br>
   - ...

<br>

---

## **OBJETIVOS A CONCRETIZAR**
**Categoria 1**<br>
- Objetivo 1<br>
- Objetivo 2<br>
- ...<br>

**Categoria 2**<br>
- Objetivo 1<br>
- Objetivo 2<br>
- ...<br>

**Categoria...** 
- ...

<br>

---

## **PARTICIPANTES**

- [x] Professor Doutor Mário Zenha-Rela
- [x] Alexy Almeida<br>
- [x] Ana Martinez<br>
- [x] Duarte Meneses<br>
- [x] Edgar Duarte<br>
- [x] Eva Teixeira<br>
- [x] Filipe Viana<br>
- [x] Francisco Carreira<br>
- [x] Gonçalo Pimenta<br>
- [x] Gonçalo Lopes<br>
- [x] Inês Marçal <br>
- [x] Joana Antunes<br>
- [x] João Lourenço<br>
- [x] João Catré<br>
- [x] João Freire<br>
- [x] Luís Braga<br>
- [x] Luís Vieira <br>
- [x] Miguel Barroso<br>
- [x] Patrícia Costa<br>
- [x] Ricardo Vieira <br>
- [x] Rodrigo Ferreira <br>
- [x] Sofia Alves <br>
- [x] Sofia Santos <br>
- [x] Sofia Neves<br>
- [x] Tatiana Almeida<br>

Participantes: presentes/total (ex: 22/24)

<br>

---

## **DOCUMENTO**

**Autoria:** Aluno 1<br>
**Recolha de informação:** Aluno 1, Aluno 2<br>
**Revisão:** Aluno 1, Aluno3
