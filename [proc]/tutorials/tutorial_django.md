## **Django**

[**Python Backend Web Development Course (with Django)**](https://youtu.be/jBzwzrDvZ18)
- 1 vídeo
- **Estimação:** 10h11min
- **Melhor para:** se quiserem num só vídeo aprender TUDO; se tiverem tempo livre.

<br>

[**Django Tutorials**](https://www.youtube.com/playlist?list=PL-osiE80TeTtoQCKZ03TU5fNfx2UY6U4p)
- 17 vídeos
- **Estimação:** 9h46min
- **Melhor para:** as mesmas razões que o vídeo maior, mas encontra-se mais dividido; considerado dos melhores tutoriais.

<br>

[**Try Django Tutorial Series**](https://www.youtube.com/playlist?list=PLEsfXFp6DpzTD1BD1aWNxS2Ep06vIkaeW)
- 47 vídeos
- **Estimação:** 4h42min
- **Melhor para:** Existe um vídeo para cada coisa, logo só precisam de selecionar os vídeos que vos interessam e pode ajudar se não souberem fazer uma coisa específica.

<br>

[**Django by Example**](https://www.youtube.com/watch?v=-FBJUtFoe-k&list=PLAF3anQEEkzS-mjdX7s-D63bjLWRdhuFM&index=1)
- 17 vídeos
- **Estimação:** 2h17min
- **Melhor para:** Mais straightforward e equilibrado em termos de tempo/aprendizagem.

<br>

[**Django walkthrough**](https://docs.google.com/document/d/1eqe9WtwRfdeO0ULOzwCjEXH7ojUd-kquGfOu2TzaQuw/edit)
- 1 documento
- **Estimação:** 2h
- **Melhor para:** Dúvidas mais técnicas e pormenores que tenham escapado; quem não estiver à vontade com inglês.

<br>

[**Python Django Crash Course 2021**](https://youtu.be/IHTP8-KskcQ?list=PLOLrQ9Pn6cax6x4UhfCGb24JqZOUl9qLL)
- 1 vídeo
- **Estimação:** 1h
- **Melhor para:** Quem não tem absolutamente tempo nenhum, mas acaba por perder imensa informação essencial.

