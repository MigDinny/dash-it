# 1ª CONTRIBUIÇÃO - FICHEIRO PESSOAL MD

## **1. Criar uma issue/ticket para essa tarefa**
1.  Na barra lateral esquerda, carregar em Issues -> List
2.  New issue
3.  Título: "Criar perfil - //o vosso 1º e último nome//"
    - Exemplo: "Criar perfil - Alexy Almeida"
4.  Descrição:<br> 
    " - [ ] Escrita ficheiro md "<br>
    " - [ ] Revisão ficheiro "
    - Têm que ficar com duas checkboxs, uma por linha
5. Assign to me 
6. Milestone -> Issue + ficheiro de perfil
7. Labels -> documentation
8. Due date -> 02-10-2021
9. Create issue<br>
    **Nota 1:** Tudo aquilo que não se encontra descrito não deve ser alterado.<br>
    **Nota 2:** Assim que o vosso ticket for criado é-lhe atribuído um número, **é importante que decorem o mesmo.**

<br> 

## **2. Criar uma branch**
1. Na barra lateral esquerda, carregar em Repository -> Branches
2. New Branch
3. Branch Name -> "perfil-<numero_da_issue>" (número que decoraram em 1.8)
   - Exemplo: "perfil-22"
4. Create from -> dev
5. Create Branch

<br> 

## **3. Escrever o ficheiro de perfil**
1. Na barra lateral esquerda, carregar em Snippets
2. Fazer download do ficheiro_pessoal_md_template 
3. Editar o ficheiro e colocar as vossas informações pessoais
    1. Substituir apenas o relevante (ex: o "br" não é relevante porque faz parte da formatação md)
    2. Deixar o ficheiro em formato md (podem editá-lo em qualquer editor de texto)
4. Alterar o nome do ficheiro para "fich_pessoal_//nome//"
    - Exemplo: "fich_pessoal_alexy"

<br> 

## **4. Dar upload do ficheiro**
1. Voltar a Repository -> Branches -> Active 
2. Encontrem a vossa Branch e entrem na mesma
3. Verificar que estão na branch correta (tem que aparecer o nome da branch que criaram, seguido de dash-it/ + **(importante)**
4. Entrar na pasta pm/profiles
5. Carregar no símbolo de + -> Upload File 
6. Upload File
7. Create merge request

<br> 

## **5. Merge**
1. Título - Upload ficheiro de perfil - // Vosso 1º e último nome //
2. Descrição ''ticket #numero_da_issue'' (ex: ticket #6)
3. Assign to me 
4. Reviewers -> Tatiana Almeida **(importante)**
5. Milestone -> Issue + ficheiro de perfil
6. Labels -> documentation
7. Deixar o resto como está
8. Create Merge Request 

<br> 

## **6. Concluir**
1. Na barra lateral esquerda, Issues -> Boards
2. Carregar no título do vosso issue 
3. Dar check apenas na box da ''Escrita ficheiro md'' 
4. Voltar ao Boards
5. Arrastar o Issue para ''waiting-approval''

<br>

**Autoria de**: Tatiana Almeida e Alexy Almeida
