## **Git** 
### **Básicos do Git**

- [Instalar o Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [Usar Git no VSCode](https://youtu.be/F2DBSH2VoHQ)
- [20 comandos essenciais](https://dzone.com/articles/top-20-git-commands-with-examples)

### **Branches**
- [O que são branches?](https://youtu.be/iJKIxrJ40ss)
- [Como usar branches no VSCode](https://youtu.be/qY6IooRlNGI)

### **GitLab**
- [GitLab para iniciantes](https://www.youtube.com/playlist?list=PLhW3qG5bs-L8YSnCiyQ-jD8XfHC2W1NL_) (ver até 3º vídeo)
- [GitLab básicos](https://docs.gitlab.com/ee/gitlab-basics/#gitlab-basics)
