# Tutorial de HTML | Engenharia de Software PL10

### Destinado a: **Todos**

#### **Requisitos**: nenhum <br><br>

Este tutorial de HTML tem como principal objetivo ajudar todos os membros da equipa a adquirir as competências básicas necessárias à linguagem em questão uma vez que todos terão, em algum momento, que realizar uma tarefa envolvendo HTML.
<br>
O tutorial consiste numa pequena compilação de vídeos e algumas extensões opcionais para o VSCode. O vídeo mais curto é de carácter obrigatório, uma vez que apresenta de forma prática as principais funcionalidades desta linguagem, enquanto que o vídeo mais longo é opcional, visto apresentar uma visão sobre HTML mais detalhada.

<div align="justify">

<br>

---

## **Vídeos**

- [Tutorial simples de HTML (30 min)](https://www.youtube.com/watch?v=PlxWf493en4) (Obrigatório)<br>

- [Curso completo HTML (2 horas)](https://www.youtube.com/watch?v=pQN-pnXPaVg) (Facultativo)<br>

---

## **Extensões VSCode**

Estas extensões ajudam na programação HTML e são apenas uma recomendação para um melhor desempenho por parte da equipa.


### **Extensões:**

- [Auto Close tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-close-tag)
- [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)
- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) 
---



## **DOCUMENTO**

**Autoria:** João Lourenço, Joana Antunes, Luís Vieira<br>