# 2ª CONTRIBUIÇÃO - Criar Requirement

Ficheiro de Apoio: https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/Lista_de_requisitos.pdf

No ficheiro de apoio encontram-se alguns dos requisitos que vão ter de ser feitos. Os mesmos, foram distribuidos por ordem alfabética, e atribuidos a todos os membros do grupo. **É IMPORTANTE** que abram o ficheiro e vejam qual o número e descrição do requisito que vos ficou atribuido.

**Atenção:** o vosso nome aparece mais do que uma vez. <br>
-Se aparecer a verde significa que já criaram aquele requisito <br>
-Se aparecer a vermelho é porque aquele é um requisito que vos falta criar

## **1. Criar um Requirement**

1.  Na barra lateral esquerda, carregar em Issues -> Requirements
2.  New Requirement
3.  Título: "(Título que está no ficheiro de apoio)"
    - Exemplo: "[RF-C] Informação Estática Ficheiro"
4.  Descrição: "(Descrição que está no ficheiro de apoio)"
5. Create issue<br>
    **Nota 1:** Tudo aquilo que não se encontra descrito não deve ser alterado.<br>
    **Nota 2:** Assim que o vosso requirement for criado é-lhe atribuído um número, **é importante que decorem o mesmo.**

<br> 

## **2. Criar uma issue/ticket para este requirement**

1.  Na barra lateral esquerda, carregar em Issues -> List
2.  New issue
3.  Título: "UST: (Nº decorado em Nota 2 da parte 1.) - (Título que está no ficheiro de apoio)"
    - Exemplo: "UST:1.0 - [RF-M] Informação Estática Ficheiro"
4.  Choose a template: 'Requisito'
5.  Descrição:<br> 
    1. **Problema**: (Descrição que está no ficheiro de apoio)
    2. Remover a linha da descrição
    3. **Prioridade:** (label que está no ficheiro de apoio)
    4. Colocar '' ** UST **: O utilizador quer (...) porque (...)". 
        1. A substituição dos (...) tem de ser feita por vocês com aquilo que acham mais adequado. Para saberem como fazer uma User Story (UST), matéria dada na aula teórica da semana do 03-10, recomendo a visualização dos videos que o docente disponibilizou nos materiais da cadeira. 
        2. Exemplo: "O utilizador quer poder visualizar a informação estática referida acima porque são dados relevantes para análise."

    5. Deixar apenas ** Etapas: ** e apagar tudo o que se encontra em baixo 
6. Labels 
    1. escolher a prioridade respectiva da label que se encontra no ficheiro de apoio <br>
        -Exemplo: priority::must <br>
        -**Nota:** No final do ficheiro de requisitos encontram-se as legendas. É importante segui-las para saberem ao certo qual a vossa label. <br>
7. Weight -> (colocar a que está no ficheiro de apoio. Se não existir deixar em branco)
7. Create issue<br>
    **Nota 1:** Tudo aquilo que não se encontra descrito não deve ser alterado.<br>

<br> 

## **3. Concluir**
1. Na barra lateral esquerda, Issues -> Boards
2. Arrastar o Issue para ''waiting-assignment''

<br>

**Autoria de**: Ricardo Vieira e Tatiana Almeida
