# Weight


<div style="text-align: justify">
Weight (peso) tem como objetivo medir aproximadamente o esforço colocado na execução de uma dada issue. Este peso é medido através do tempo despendido para terminar a respetiva tarefa:<br>
<br>
1 un = 20 min<br>
2 un = 40 min<br>
3 un = 1h<br>
5 un = 1h 40 min<br>
8 un = 2h 40 min<br>
13 un = 4h 20 min (uma tarde inteira)<br>
21 un = 7h (um dia inteiro)<br>
34 un = 11h 20 min (muito excecional, raramente se usa)<br>
<br>
Deverá ser incluido em todas as issues o seu weight seguindo esta convenção, sendo que devem apenas contabilizar o tempo de esforço e arredondar para o valor mais próximo nos casos intermédios.
  
</div>
