let coll = document.getElementsByClassName("collapsible");
let coll2 = document.getElementById("collapse");
let i;

for (i = 0; i < coll2.length; i++) {
    coll2[i].addEventListener("click", function() {
        this.classList.toggle("active");
        let content = coll.nextElementSibling;
        if (content.style.display === "block") {
            content.style.display = "none";
        } else {
            content.style.display = "block";
        }
    });
}