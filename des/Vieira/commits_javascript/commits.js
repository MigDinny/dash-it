//data-----------------------------------------------------------------

var dataArray = [
    {'descricao':'vistas', 'autor':'luis', 'data':'8/12/2021', 'hora':'14' , 'categoria':'UI'},
    {'descricao':'vistas', 'autor':'tatiana', 'data':'7/12/2021', 'hora':'14', 'categoria':'DEV'},
    {'descricao':'vistas', 'autor':'joao', 'data':'7/12/2021', 'hora':'14', 'categoria':'DEV'},
    {'descricao':'vistas', 'autor':'zé', 'data':'8/12/2021', 'hora':'14', 'categoria':'UI'},
    {'descricao':'vistas', 'autor':'ana', 'data':'7/12/2021', 'hora':'14', 'categoria':'UI'},
    {'descricao':'vistas', 'autor':'maria', 'data':'6/12/2021', 'hora':'14', 'categoria':'REQ'},
    {'descricao':'vistas', 'autor':'luis', 'data':'6/12/2021', 'hora':'14', 'categoria':'UI'},
]

//---------------------------------------------------------------------------

//main-----------------------------------------------------------------------

console.log(dataArray)

dataArray.sort(function (a, b) {
    if (a.autor > b.autor) {
      return 1;
    }
    if (a.autor < b.autor) {
      return -1;
    }
    // a must be equal to b
    return 0;
});

var datesArray = insertDates(dataArray,datesArray)
console.log(datesArray)
buildTable(dataArray,datesArray)


buildListAuthor(dataArray)
buildListCategory(dataArray)

//-----------------------------------------------------------------------------

//Functions----------------------------------------------------------------------

function filterAuthor(autor){
    myArray = []

    var filtros = document.getElementsByTagName("LI");

    for (var i = 0; i < filtros.length; i++) {
        filtros[i].style.fontWeight = "200";
        if(filtros[i].innerHTML == autor) filtros[i].style.fontWeight = "900";
    }
    
    for(var i = 0; i < dataArray.length; i++){
        if(dataArray[i].autor == autor){
            myArray.push(dataArray[i])
        }
    }

    var indatesArray = insertDates(myArray,indatesArray)
    buildTable(myArray,indatesArray)
}

function filterCategory(categoria){
    myArray = []

    var filtros = document.getElementsByTagName("LI");

    for (var i = 0; i < filtros.length; i++) {
        filtros[i].style.fontWeight = "200";
        if(filtros[i].innerHTML == categoria) filtros[i].style.fontWeight = "900";
    }

    for(var i = 0; i < dataArray.length; i++){
        if(dataArray[i].categoria == categoria){
            myArray.push(dataArray[i])
        }
    }

    var indatesArray = insertDates(myArray,indatesArray)
    buildTableCategory(myArray,indatesArray,categoria)
}


function insertDates(dataArray, datesArray){
    datesArray = []
    for(var i = 0; i < dataArray.length; i++){
        if(!datesArray.includes(dataArray[i].data)){
            datesArray.push(dataArray[i].data)
        }
    }
    datesArray.sort()
    return datesArray
}


function buildTable(data,dates){
    var table = document.getElementById('myTable')
    table.innerHTML = ''
    var days = 0

    for (var j = dates.length-1; j >= 0; j--){
        var numb = 0

        var row =   `<tr>
                        <th class="commit_th" >${dates[j]} </th>
                    </tr>`

        table.innerHTML += row
        
        for (var i = 0; i < data.length; i++){
            var colautor = `autor-${i}`
            var colcategoria = `categoria-${i}`
            
            
            if(data[i].data == dates[j]){
                numb++
                var row = `<tr class="commit">
                                <td>${data[i].descricao}</td>
                                <td>${data[i].autor}</td>
                                <td>${data[i].data}</td>
                        </tr>`
                table.innerHTML += row
            }

        }

        var row = `- ${numb} Commits`

        var commit_th = document.getElementsByClassName('commit_th')
        commit_th[days].innerHTML += row
        days++
        console.log("days = "+days)
        
    }
    days = 0
    
}


function buildTableSpec(data,dates,string){
    var table = document.getElementById('myTable')
    table.innerHTML = ''
    var days = 0

    var filtros = document.getElementsByTagName("LI");

    for (var i = 0; i < filtros.length; i++) {
        filtros[i].style.fontWeight = "200";
        if(filtros[i].innerHTML == string) filtros[i].style.fontWeight = "900";
    }


    for (var j = dates.length-1; j >= 0; j--){
        var numb = 0

        var row =   `<tr>
                        <th class="commit_th" >${dates[j]} </th>
                    </tr>`

        table.innerHTML += row
        
        for (var i = 0; i < data.length; i++){
            var colautor = `autor-${i}`
            var colcategoria = `categoria-${i}`
            
            
            if(data[i].data == dates[j]){
                numb++
                var row = `<tr class="commit">
                                <td>${data[i].descricao}</td>
                                <td>${data[i].autor}</td>
                                <td>${data[i].data}</td>
                        </tr>`
                table.innerHTML += row
            }

        }

        var row = `- ${numb} Commits`

        var commit_th = document.getElementsByClassName('commit_th')
        commit_th[days].innerHTML += row
        days++
        console.log("days = "+days)
        
    }
    days = 0
    
}



function buildTableCategory(data,dates,categoria){
    var table = document.getElementById('myTable')
    table.innerHTML = ''

    data.sort(function (a, b) {
        if (a.data > b.data) {
          return -1;
        }
        if (a.data < b.data) {
          return 1;
        }
        // a must be equal to b
        return 0;
    });

    var row = ` <tr>
                    <th>Pasta de ${categoria}</th>
                </tr>`
    table.innerHTML += row
        
    for (var i = 0; i < data.length; i++){
        var colautor = `autor-${i}`
        var colcategoria = `categoria-${i}`
        
        var row = `<tr class="commit">
                        <td>${data[i].descricao}</td>
                        <td>${data[i].autor}</td>
                        <td>${data[i].data}</td>
                </tr>`
        table.innerHTML += row

    }
}



function buildTableAllCategory(data,string){
    var table = document.getElementById('myTable')
    table.innerHTML = ''

    var filtros = document.getElementsByTagName("LI");

    for (var i = 0; i < filtros.length; i++) {
    filtros[i].style.fontWeight = "200";
    if(filtros[i].innerHTML == string) filtros[i].style.fontWeight = "900";
    }

    data.sort(function (a, b) {
        if (a.data > b.data) {
          return -1;
        }
        if (a.data < b.data) {
          return 1;
        }
        // a must be equal to b
        return 0;
    });




    var catArray = []
    for(var i = 0; i < dataArray.length; i++){
        if(!catArray.includes(dataArray[i].categoria)){
            catArray.push(dataArray[i].categoria)
        }
    }
    console.log(catArray)
    catArray.sort()
    for(var j = 0; j < catArray.length; j++){
        var row = ` <tr>
                        <th>Pasta de ${catArray[j]}</th>
                    </tr>`
        table.innerHTML += row
        
        for (var i = 0; i < data.length; i++){
            if(data[i].categoria == catArray[j]){
                var row = `<tr class="commit">
                                <td>${data[i].descricao}</td>
                                <td>${data[i].autor}</td>
                                <td>${data[i].data}</td>
                        </tr>`
                table.innerHTML += row
            }
        }
    }     
}



function buildListAuthor(data){
    var list = document.getElementById('myList_authors')
    
    var authors = []

    for (var i = 0; i < data.length; i++){

        if(!authors.includes(data[i].autor)){
            authors.push(data[i].autor)
            var row = `<li id = "autor${i}"onclick="filterAuthor(this.innerHTML)">${data[i].autor}</li>`
            list.innerHTML += row
        }
    }
}


function buildListCategory(data){
    var list = document.getElementById('myList_categories')
    
    var categories = []

    for (var i = 0; i < data.length; i++){

        if(!categories.includes(data[i].categoria)){
            categories.push(data[i].categoria)
            var row = `<li id = "categoria${i}" onclick="filterCategory(this.innerHTML)">${data[i].categoria}</li>`
            list.innerHTML += row
        }
    }
}

