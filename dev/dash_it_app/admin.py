from django.contrib import admin

# Register your models here.
from .models import Repo, User, UserRepo, Branch, Commit, BranchCommit, Folder, File, Issue, UserIssue, Label, IssueLabel, MergeRequest, MergeRequestLabel, IssueMergeRequest, MergeRequestCommit

admin.site.register(Repo)
admin.site.register(User)
admin.site.register(UserRepo)
admin.site.register(Branch)
admin.site.register(Commit)
admin.site.register(BranchCommit)
admin.site.register(Folder)
admin.site.register(File)
admin.site.register(Issue)
admin.site.register(UserIssue)
admin.site.register(Label)
admin.site.register(IssueLabel)
admin.site.register(MergeRequest)
admin.site.register(MergeRequestLabel)
admin.site.register(IssueMergeRequest)
admin.site.register(MergeRequestCommit)
