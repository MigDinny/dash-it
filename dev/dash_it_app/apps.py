from django.apps import AppConfig


class DashItAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dash_it_app'
