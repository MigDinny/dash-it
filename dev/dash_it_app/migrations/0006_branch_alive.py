# Generated by Django 3.2.7 on 2021-12-01 00:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dash_it_app', '0005_commit_commit_gitlab_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='branch',
            name='alive',
            field=models.BooleanField(default=True),
        ),
    ]
