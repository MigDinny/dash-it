# Generated by Django 3.2.7 on 2021-12-02 22:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dash_it_app', '0007_auto_20211202_2208'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='file',
            name='last_commit',
        ),
        migrations.AddField(
            model_name='file',
            name='last_commit_id',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='file',
            name='repo_id',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='dash_it_app.repo'),
            preserve_default=False,
        ),
    ]
