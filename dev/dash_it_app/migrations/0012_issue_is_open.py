# Generated by Django 3.2.7 on 2021-12-04 13:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dash_it_app', '0011_folder_repo_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='issue',
            name='is_open',
            field=models.CharField(default=1, max_length=100),
            preserve_default=False,
        ),
    ]
