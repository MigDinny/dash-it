# Generated by Django 3.2.7 on 2021-12-12 16:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dash_it_app', '0014_commit_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='file',
            name='size',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='issue',
            name='time_spent',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='repo',
            name='last_updated',
            field=models.CharField(default=1, max_length=25),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='user',
            name='time_spent',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='repo',
            name='name',
            field=models.CharField(max_length=50),
        ),
    ]
