from django.db import models
from datetime import datetime

class Repo(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(null=False, max_length=50)
    last_updated = models.CharField(max_length=25)

    def __str__(self):
        return 'id: ' + str(self.id) + ', name: ' + self.name + ", last_updated: " + str(self.last_updated)

class User(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(null=False, max_length=100)
    username = models.CharField(null=False, max_length=50)
    image = models.ImageField(blank= True, null = True)
    time_spent = models.IntegerField(null=True, blank= True)

    def __str__(self):
        return 'id: ' + str(self.id) + ', name: ' + self.name + ', username: ' + self.username + ', image: ' + str(self.image)
        

class UserRepo(models.Model):
    user_id = models.ForeignKey(User, on_delete = models.CASCADE)
    repo_id = models.ForeignKey(Repo, on_delete = models.CASCADE)

    def __str__(self):
        return 'user_id: ' + str(self.user_id) + ', repo_id: ' + str(self.repo_id)

class Branch(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(null=False, max_length=28)
    alive = models.BooleanField(default=True, null=False)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    repo_id = models.ForeignKey(Repo, on_delete=models.CASCADE)

    def __str__(self):
        return 'id: ' + str(self.id) + ', name: ' + self.name + ',user_id: ' + str(self.user_id.pk) + ', repo_id: ' + str(self.repo_id.pk) + ', alive' + str(self.alive)

class Commit(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(null=False, max_length=50)
    time_creation = models.DateTimeField(default=datetime.now, blank=True)
    user_id = models.ForeignKey(User, on_delete = models.CASCADE)
    description = models.TextField(blank=True, null = True)
    commit_gitlab_id = models.CharField(null=False, max_length=50)
    repo_id = models.ForeignKey(Repo, on_delete = models.CASCADE)

    def __str__(self):
        return 'id: ' + str(self.id) + ', name: ' + self.name + ', commit id gitlab: ' + self.commit_gitlab_id + ', time_creation: ' + str(self.time_creation)+ ', user_id: ' + str(self.user_id.pk) + 'repo_id' + str(self.repo_id.pk)
class BranchCommit(models.Model):
    branch_id = models.ForeignKey(Branch, on_delete=models.CASCADE)
    commit_id = models.ForeignKey(Commit, on_delete=models.CASCADE)

    def __str__(self):
        return 'branch_id: ' + str(self.branch_id.pk) + ', commit_id: ' + str(self.commit_id.pk)

class Folder(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(null=False, max_length=100)
    path = models.CharField(null=False, max_length=255)
    repo_id = models.ForeignKey(Repo, on_delete = models.CASCADE)

    def __str__(self):
        return 'id: ' + str(self.id) + ', name: ' + self.name + ', dir: ' + self.path + "repo : " + str(self.repo_id.pk)

class File(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(null=False, max_length=100)
    content = models.TextField(blank= True, null = True)
    path = models.CharField(null=False, max_length=255)
    size = models.IntegerField(null = True, blank = True)
    last_commit_id = models.IntegerField(null = True, blank = True)
    repo_id = models.ForeignKey(Repo, on_delete=models.CASCADE)

    def __str__(self):
        return 'id: ' + str(self.id) + ', name: ' + self.name + ', content: ' + self.content + ', dir: ' + self.path + ', commit_id: ' + str(self.last_commit_id) + "repo_id" + str(self.repo_id.pk) + "size" + str(self.size)

class Issue(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(null=False, max_length=100)
    description = models.TextField(null=False)
    user_id_create = models.ForeignKey(User, on_delete=models.CASCADE)
    requirement = models.CharField(null=False, max_length=255)
    time_creation = models.DateTimeField(default=datetime.now, blank=True)
    time_spent = models.IntegerField(null=True, blank=True)
    repo_id = models.ForeignKey(Repo, on_delete=models.CASCADE)
    is_open = models.CharField(null=False, max_length=100)

    def __str__(self):
        return 'id: ' + str(self.id) + ', title: ' + self.title + ', description: ' + self.description + ', user_id_create: ' + str(self.user_id_create.pk) \
            + ', requirement: ' + self.requirement + ', time_creation: ' + str(self.time_creation) + ', repo_id: ' + str(self.repo_id.pk) + "time_spent" + str(self.time_spent)

class UserIssue(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    issue_id = models.ForeignKey(Issue, on_delete=models.CASCADE)

    def __str__(self):
        return 'user_id: ' + str(self.user_id.pk) + ', issue_id: ' + str(self.issue_id.pk)

#Atencao nao meter a description no str visto que pode ser null
class Label(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(null=False, max_length=255)
    description = models.TextField(blank= True, null = True)
    repo_id = models.ForeignKey(Repo, on_delete=models.CASCADE)

    def __str__(self):
        return 'id: ' + str(self.id) + ', name: ' + self.name +', repo_id: ' + str(self.repo_id.pk)

class IssueLabel(models.Model):
    issue_id = models.ForeignKey(Issue, on_delete=models.CASCADE)
    label_id = models.ForeignKey(Label, on_delete=models.CASCADE)

    def __str__(self):
        return 'issue_id: ' + str(self.issue_id.pk) + ', labels_id: ' + str(self.label_id.pk)

class MergeRequest(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(null=False, max_length=255)
    source_branch_id = models.ForeignKey(Branch, on_delete=models.CASCADE)
    target_branch_id = models.IntegerField(null=False)
    description = models.TextField(blank= True, null = True)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    time_creation = models.DateTimeField(default=datetime.now, blank=True)
    time_merged = models.DateTimeField(blank = True, null = True)
    open_merge_request = models.CharField(null=False, max_length=255)

    def __str__(self):
        return 'id: ' + str(self.id) + ', name: ' + self.name + ', source_branch_id: ' + str(self.source_branch_id.pk) +'target_branch_id, :' + str(self.target_branch_id) \
            + 'description, :' + self.description + 'user_id, :' + str(self.user_id.pk) + 'time_creation, :' + str(self.time_creation) + 'time_merged, :' + str(self.time_merged) \
            +  'open_merge_request, :' + str(self.open_merge_request)

class MergeRequestLabel(models.Model):
    merge_request_id = models.ForeignKey(MergeRequest, on_delete=models.CASCADE)
    labels_id = models.ForeignKey(Label, on_delete=models.CASCADE)

    def __str__(self):
        return 'merge_request_id: ' + str(self.merge_request_id) + ', labels_id: ' + str(self.labels_id)

class IssueMergeRequest(models.Model):
    issue_id = models.ForeignKey(Issue, on_delete=models.CASCADE)
    merge_request_id = models.ForeignKey(MergeRequest, on_delete=models.CASCADE)

    def __str__(self):
        return 'issue_id: ' + str(self.issue_id.pk) + ', merge_request_id: ' + str(self.merge_request_id.pk)
    
class MergeRequestCommit(models.Model):
    merge_request_id = models.ForeignKey(MergeRequest, on_delete=models.CASCADE)
    commit_id = models.ForeignKey(Commit, on_delete=models.CASCADE)

    def __str__(self):
        return 'merge_request_id: ' + str(self.merge_request_id.pk) + ', commit_id: ' + str(self.commit_id.pk)
