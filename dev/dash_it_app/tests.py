from django.test import TestCase
from .models import User

class testUser(TestCase):
    def setUp(self):
        User.objects.create(name = 'Catre', username = 'Descatres')
        User.objects.create(name = 'Rodrigo', username = 'IronFalcon')
        for i in range(1000):
            User.objects.create(name = 'Name'+str(i), username = 'username'+str(i))

    #teste para verificar o tempo que é necessário para ir buscar mil entradas à db
    def test1(self):
        for i in range(1000):
            user = User.objects.get(name='Name'+str(i))
            #print(user.name, user.username)
