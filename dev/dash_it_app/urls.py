from django.conf.urls import url
from . import views
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from .views import FileDetailView, FileList, FileHistory

urlpatterns = [
    url(r'^$', views.homepage, name= 'homepage'),
    url(r'^lista-commits', views.lista_commits.as_view(), name= 'lista-commits'),
    url(r'^lista-ficheiros', FileList.as_view(), name= 'lista-ficheiros'),
    url(r'^issues', views.issues.as_view(), name = 'issues'),
    path('fich/<int:id>', FileDetailView.as_view(), name='ficheiro_individual'),
    url(r'^departamento', views.lista_users_ativos.as_view(), name = 'departamento'),
    url(r'^historico', FileHistory.as_view(), name= 'historico_ficheiro'),
    path('perfil_individual/<int:id>', views.userProfile.as_view(), name= 'perfil_individual')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
