from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse

from django.views.generic import View, DetailView
from .models import User, Commit, File, Folder, Issue, Repo, UserRepo, UserIssue

#from pydriller import Repository
import gitlab as gl
from .crawler import Crawler as cw
from django.template import loader
from django.shortcuts import redirect
from django.contrib import messages

def homepage(request):

    c = None
    
    repo_name = request.COOKIES.get("repo-name")
    # retrieve selected repo
    if (request.method == 'POST'):

        refresh = request.POST.get('refresh')
        repo_input = request.POST.get("repo-name", "")

        if refresh is not None and repo_name != "None":
            try:
                c = cw(repo_name)
                if c.exists_database is True:
                    c.refresh()
                else:
                    c.fetch()
            except:
                repo_name = None

        elif repo_input is not None:
            try:
                c = cw(repo_input)
  
                if c.exists_database is False:
                    c.fetch()

                repo_name = repo_input

            except Exception as e:
                print(e)
                messages.info(request, 'Repositório não existe/não é público!')
                repo_name = None

    last_updated = "N/A"
    if (c is None):
        try:
            c = cw(repo_name)
            if (c.exists_database is True):
                last_updated = str(c.repo_obj.last_updated)

        except Exception:
            last_updated = "N/A"


    context = {
        'repo_name': repo_name,
        'last_updated': last_updated
    }

    response = render(request, 'homepage.html', context)
    response.set_cookie("repo-name", repo_name)

    return response


class lista_commits(View):
    
    def get(self, request):

        #See if cookie is set. If not sends to homepage
        repo_name = request.COOKIES.get("repo-name")
        if(repo_name == "None"):

            context = {
                'repo_name': repo_name
            }

            messages.info(request, 'Para aceder a esta funcionalidade selecione um repositório!')
            return render(request, 'homepage.html', context)
        
        # vai buscar os users
        try:
            #Gets the repository being used
            repo = Repo.objects.filter(name = repo_name)[0]

            users = UserRepo.objects.filter(repo_id = repo)   

            user_list = []

            for u in users:
                user_list.append(User.objects.filter(id = u.user_id.pk)[0])

            user_list.sort(key=lambda u: u.name)
            

        except User.DoesNotExist:
            user_list = None

        try:
            repo = Repo.objects.filter(name = repo_name)[0]
            commits = Commit.objects.filter(repo_id = repo.pk)
 
        except Commit.DoesNotExist:
            commits = None
        
        all_files = []
        for commit in commits:
            files = File.objects.filter(last_commit_id=commit.id)

            all_files.append(files)
        
        commits_and_files = list(zip(commits, all_files))

        context = {
            'users': user_list,
            'commits_and_files': commits_and_files,
        }
        return render(request, 'lista-commits.html', context)

    def post(self, request):
        # vai buscar os users
        try:
            repo_name = request.COOKIES.get("repo-name")

            if(repo_name == "None"):

                context = {
                    'repo_name': repo_name
                }

                messages.info(request, 'Para aceder a esta funcionalidade selecione um repositório!')
                return render(request, 'homepage.html', context)
            
            repo = Repo.objects.filter(name = repo_name)[0]

            users = UserRepo.objects.filter(repo_id = repo.pk)   

            user_list = []

            for u in users:
                user_list.append(User.objects.filter(id = u.user_id.pk)[0])

            user_list.sort(key=lambda u: u.name)

        except User.DoesNotExist:
            user_list = None
        #opção de todos os users
        if (request.POST.get("filtro") == '0'):
            try:
                
                commits = Commit.objects.filter(repo_id = repo.pk)
            except Commit.DoesNotExist:
                commits = None

        else: 
            # commits do user com filtro associado
            try:
                commits = Commit.objects.filter(user_id=request.POST.get("filtro"), repo_id = repo.pk)
            except Commit.DoesNotExist:
                commits = None

        all_files = []
        for commit in commits:
            files = File.objects.filter(last_commit_id=commit.id)
            all_files.append(files)

        commits_and_files = list(zip(commits, all_files))

        context = {
            'users': user_list,
            'commits_and_files': commits_and_files,
        }

        return render(request, 'lista-commits.html', context)


class lista_users_ativos(View):
    
    def get(self, request):
        #See if cookie is set. If not sends to homepage
        repo_name = request.COOKIES.get("repo-name")
        if(repo_name == "None"):

            context = {
                'repo_name': repo_name
            }

            messages.info(request, 'Para aceder a esta funcionalidade selecione um repositório!')
            return render(request, 'homepage.html', context)


        try:

            repo = Repo.objects.filter(name = repo_name)[0]

            
            users = UserRepo.objects.filter(repo_id = repo.pk)

   


            user_list = []
            
            for u in users:
                user_list.append(User.objects.filter(id = u.user_id.pk)[0])
            

        except User.DoesNotExist:
            user_list = None

        try:
            commits = Commit.objects.filter(repo_id = repo.pk)
        except Commit.DoesNotExist:
            commits = None


        result = []
        
        for i in commits: 
            if i.user_id not in result: 
                temp = self.find_user_time_spent(i.user_id)
                i.user_id.time_spent = temp
                result.append(i.user_id)
        
        result.sort(key=lambda u: u.name)

        context = {
            'users': users,
            'user_info': result, 
        }
        return render(request, 'departamento.html',context)
        
    def find_user_time_spent(self, user):
        
        try:
            issues = UserIssue.objects.filter(user_id=user)
        except Issue.DoesNotExist:
            issues = None

        time_spent = 0
  
        for issue in issues:
            try:
                num_users = UserIssue.objects.filter(issue_id = issue.issue_id).count()
            except User.DoesNotExist:
                num_users = 1

            time_to_add = issue.issue_id.time_spent / num_users
            time_spent += time_to_add

        time_spent = round(time_spent / 3600, 2)
        user.time_spent = time_spent

        return time_spent

    


    def post(self, request):

        #See if cookie is set. If not sends to homepage
        repo_name = request.COOKIES.get("repo-name")
        if(repo_name == "None"):

            context = {
                'repo_name': repo_name
            }
            messages.info(request, 'Para aceder a esta funcionalidade selecione um repositório!')
            return render(request, 'homepage.html', context)

        try:
            repo = Repo.objects.filter(name = repo_name)[0]
            users = UserRepo.objects.filter(repo_id = repo.pk)
            user_list = []
            
            for u in users:
                user_list.append(User.objects.filter(id = u.user_id.pk)[0])
            user_list.sort(key=lambda u: u.name)

        except User.DoesNotExist:
            user_list = []

        if (request.POST.get("filtro") == '0'):
            try:
                commits = Commit.objects.filter(repo_id = repo.pk)
            except Commit.DoesNotExist:
                commits = None

        else: 
            # commits do user com filtro associado
            try:
                commits = commits = Commit.objects.filter(repo_id = repo.pk, user_id=request.POST.get("filtro"))
            except Commit.DoesNotExist:
                commits = None
        
        context = {
            'users': user_list,
            'commits': commits,
        }
        return render(request, 'departamento.html', context)

class FileList(View):

    def get(self, request):

       #See if cookie is set. If not sends to homepage
        repo_name = request.COOKIES.get("repo-name")
        if(repo_name == "None"):

            context = {
                'repo_name': repo_name
            }

            messages.info(request, 'Para aceder a esta funcionalidade selecione um repositório!')
            return render(request, 'homepage.html', context)

        try:
            repo = Repo.objects.filter(name = repo_name)[0]

            users = UserRepo.objects.filter(repo_id = repo.pk)
            user_list = []
            
            for u in users:
                user_list.append(User.objects.filter(id = u.user_id.pk)[0])

            user_list.sort(key=lambda u: u.name)

        except User.DoesNotExist:
            user_list = None

        try:
            fileF = File.objects.filter(repo_id = repo.pk).order_by('name').order_by('path')
        except File.DoesNotExist:
            fileF = None

            
        context = {
            'users': user_list,
            'files': fileF,
        }
        return render(request, 'lista-ficheiros.html', context)

    def post(self, request):

        #See if cookie is set. If not sends to homepage
        repo_name = request.COOKIES.get("repo-name")
        if(repo_name == "None"):

            context = {
                'repo_name': repo_name
            }

            messages.info(request, 'Para aceder a esta funcionalidade selecione um repositório!')
            return render(request, 'homepage.html', context)
            
        try:
            repo = Repo.objects.filter(name = repo_name)[0]

            users = UserRepo.objects.filter(repo_id = repo.pk)
            user_list = []
            
            for u in users:
                user_list.append(User.objects.filter(id = u.user_id.pk)[0])

            user_list.sort(key=lambda u: u.name)

        except Folder.DoesNotExist:
            usersF = None
            return render(request, 'lista-ficheiros.html')
        
        #opção de todos os users
        if (request.POST.get("filtro_user") == '0'):
            try:
                fileF = File.objects.filter(repo_id = repo.pk).order_by('name').order_by('path')
            except File.DoesNotExist:
                fileF = None
                return render(request, 'lista-ficheiros.html')

        else: 
            try:
                user_special = User.objects.get(id=request.POST.get("filtro_user"))
                commitsFromUser = user_special.commit_set.all()
                fileF = []
                for commit in commitsFromUser:
                    fileF += File.objects.filter(last_commit_id=commit.id)
                
                fileF.sort(key=lambda f:f.name)
                
            except File.DoesNotExist:
                fileF = None

        if (request.POST.get("filtro_categoria") == '0'):
            context = {
                'files': fileF,
                'users': user_list,
            }
            return render(request, 'lista-ficheiros.html', context)

        else: 
            cat_filtered = []
            for file in fileF:
                if request.POST.get('filtro_categoria') in file.path:
                    cat_filtered.append(file)
            context = {
                'files': cat_filtered,
                'users': user_list,
            }
            return render(request, 'lista-ficheiros.html', context)

        

class FileDetailView(DetailView):
    
    template_name = "ficheiro_individual.html"
    queryset = File.objects.all()

    def get_object(self):

        id_ = self.kwargs.get("id")
        return get_object_or_404(File, id=id_)

class issues(View):
    
    def get(self, request):

        #See if cookie is set. If not sends to homepage
        repo_name = request.COOKIES.get("repo-name")
        if(repo_name == "None"):

            context = {
                'repo_name': repo_name
            }

            messages.info(request, 'Para aceder a esta funcionalidade selecione um repositório!')

            return render(request, 'homepage.html', context)

        try:
            repo = Repo.objects.filter(name = repo_name)[0]

            issues = Issue.objects.filter(repo_id = repo.pk)
        except Issue.DoesNotExist:
            issues = None

        context = {
            'issues': issues,
        }
        return render(request, 'issues.html', context)

    def post(self, request):

        #See if cookie is set. If not sends to homepage
        repo_name = request.COOKIES.get("repo-name")
        if(repo_name == "None"):

            context = {
                'repo_name': repo_name
            }
            
            messages.info(request, 'Para aceder a esta funcionalidade selecione um repositório!')

            return render(request, 'homepage.html', context)
        
        if (request.POST.get("filtro") == '0'):
            try:
                repo = Repo.objects.filter(name = repo_name)[0]

                issues = Issue.objects.filter(repo_id = repo.pk)
            except Issue.DoesNotExist:
                issues = None

        else: 
            # issues com filtro associado (fechado ou aberto)
            try:
                repo = Repo.objects.filter(name = repo_name)[0]
                if request.POST.get("filtro") == '1':
                    issues = Issue.objects.filter(repo_id = repo, is_open="opened")
                elif request.POST.get("filtro") == '2':
                    issues = Issue.objects.filter(repo_id = repo, is_open="closed")
                else:
                    issues = None
            except Commit.DoesNotExist:
                issues = None


        context = {
            'issues': issues,
        }
        return render(request, 'issues.html', context)

class FileHistory(View):

    def get(self,request):
        return render(request,'historico_ficheiro.html')


class userProfile(View):

    def get(self, request, id):
        
        #See if cookie is set. If not sends to homepage
        repo_name = request.COOKIES.get("repo-name")
        if(repo_name == "None"):

            context = {
                'repo_name': repo_name
            }

            messages.info(request, 'Para aceder a esta funcionalidade selecione um repositório!')
            return render(request, 'homepage.html', context)

        try:
            user_info = User.objects.filter(id=id)[0]

            try:
                repo = Repo.objects.filter(name = repo_name)[0]
                commits_user = Commit.objects.filter(repo_id = repo.pk, user_id = user_info.id)

            except Commit.DoesNotExist:
                commits_user = None

        except User.DoesNotExist:
            user_info = None
            commits_user = None

        user_info.time_spent = self.find_user_time_spent(user_info)


        context = {
            'user': user_info,
            'commits_user': commits_user,
        }

        return render(request,'perfil_individual.html', context)

    def post(self, request, id):
        return render(request,'perfil_individual.html')

    def find_user_time_spent(self, user):
        
        try:
            issues = UserIssue.objects.filter(user_id=user)
        except Issue.DoesNotExist:
            issues = None

        time_spent = 0
  
        for issue in issues:
            try:
                num_users = UserIssue.objects.filter(issue_id = issue.issue_id).count()
            except User.DoesNotExist:
                num_users = 1

            time_to_add = issue.issue_id.time_spent / num_users
            time_spent += time_to_add

        time_spent = round(time_spent / 3600, 2)
        user.time_spent = time_spent

        return time_spent
    
