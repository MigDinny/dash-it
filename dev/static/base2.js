let coll = document.getElementsByClassName("collapsible");
let coll2 = document.getElementById("collapse");
let i2;

for (i2 = 0; i2 < coll2.length; i2++) {
    coll2[i2].addEventListener("click", function() {
        this.classList.toggle("active");
        let content = coll.nextElementSibling;
        if (content.style.display === "block") {
            content.style.display = "none";
        } else {
            content.style.display = "block";
        }
    });
}