# **SOBRE MIM**

O meu nome é Gonçalo Lopes, tenho 19 anos e estou no 3º ano do curso de Engenharia Informática, na universidade de Coimbra. Ainda não tenho uma preferencia definida mas neste projeto o meu interesse consiste em desempenhar funções dos cargos de Desenvolvimento, Qualidade e Requisitos.
<br>
# **Contribuição Individual** 
<br>
**Papel no Projeto:** Fui membro da equipa de Qualidade e realizei tarefas de testagem.

**[proc]**

Escrita de Ata -> #50 -> [MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/42)<br>
Escrita de Ata -> #97 <br>
Revisão de Ata -> #154 <br>
Revisão de Ata -> #196 <br>
Revisão de Ata -> #240<br>
Escrita de Ata -> #270<br>


**arch** <br>

**des** <br>

**dev** <br>

**env** <br>

**pm** <br>

**prod** <br>

**qa**<br>

Aprovação de Layouts -> #90 <br>
Revisão do Manual de Qualidade -> #102 <br>
Conversão do formato do Manual de Requisitos para Markdown -> #131 -> [MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/91)<br>
Testagem da Dashboard -> #246<br>
Testagem da Dashboard -> #264<br>
Criar plano de testes - PL9   -> #268<br>
Testar dashboard PL9 e documentar -> #269<br>
Escrever secção 5 do Manual de Qualidade -> #291<br>
Rever Manual de Qualidade e Acrescentar pequenas coisas -> #292<br>
Escrever secção 4 do Manual de Qualidade -> #294<br>
Escrever secção do 1 e 6 do Manual de Qualidade -> #295<br>
Testagem da Dashboard -> #299<br>

**req**<br>

Criação de um plano de testes -> #228 <br>

**outros**<br>

Criar ficheiro de perfil -> #36 -> [MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/29)<br>
Visualizar tutorias de Gitlab -> #45<br>
Visualizar tutorias de Django -> #52<br>
Visualizar tutorias de HTML -> #98<br>
Realização de teste de conhecimentos de Django -> #201<br>
Atualizar ficheiro de perfil -> #300 ->[MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/174) <br>



### **CONTACTOS**

**email:** goncalonlopes.02@gmail.com
**email Dei:** goncalolopes@student.dei.uc.pt

