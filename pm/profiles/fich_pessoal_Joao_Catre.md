# **SOBRE MIM**

O meu nome é João Catré, tenho 19 anos e estou no 3º ano do curso de Engenharia Informática, na Universidade de Coimbra. Tenho uma preferência na área de Desenvolvimento, sendo que neste projeto o meu interesse consiste em desempenhar funções dos cargos de Desenvolvimento, Requisitos e UI Design.

<br>

### **CONTACTOS**

**email:** joaomcatre@hotmail.com
**linkedin:** https://www.linkedin.com/in/joaocatre/

**Papel no projeto:** Neste projeto fui developer; Participei na criação dos modelos da base de dados; Participei na recolha de informação para algumas atas; Fiz alguns requisitos com a minha equipa (listados abaixo); dei algumas ideias de como tornar o programa mais eficiente ao apresentar as vistas e participei na integração do CSS.

<br>

## **[proc]**
- Recolha de informação para ata &rarr; [https://gitlab.com/MigDinny/dash-it/-/issues/16] &rarr; [https://gitlab.com/MigDinny/dash-it/-/merge_requests/19]
- Recolha de informação para ata &rarr; [https://gitlab.com/MigDinny/dash-it/-/issues/17] &rarr; [https://gitlab.com/MigDinny/dash-it/-/merge_requests/19]
- Recolha de informação para ata &rarr; [https://gitlab.com/MigDinny/dash-it/-/issues/97]
<br>

## **arch**
- Models atualizados &rarr; [https://gitlab.com/MigDinny/dash-it/-/issues/253] &rarr; [https://gitlab.com/MigDinny/dash-it/-/merge_requests/129]
<br>

## **des**
- Ver Tutoriais HTML &rarr; [https://gitlab.com/MigDinny/dash-it/-/issues/98]

<br>

## **dev**
- Visualizar tutoriais de Gitlab &rarr; [https://gitlab.com/MigDinny/dash-it/-/issues/45]
- Visualizar tutoriais de Django &rarr; [https://gitlab.com/MigDinny/dash-it/-/issues/52]
- Check-up de conhecimentos Django da Dev Team &rarr; [https://gitlab.com/MigDinny/dash-it/-/issues/114]
- Integração dos Templates da UI &rarr; [https://gitlab.com/MigDinny/dash-it/-/issues/127]
- Visualização da lista de commits &rarr; [https://gitlab.com/MigDinny/dash-it/-/issues/208] &rarr; [https://gitlab.com/MigDinny/dash-it/-/merge_requests/113]
- Criação de função __str__ no models e resolução de bug no models &rarr; [https://gitlab.com/MigDinny/dash-it/-/issues/223] &rarr; [https://gitlab.com/MigDinny/dash-it/-/merge_requests/107]
- Ajuda a resolver problemas a extrair dados oferecidos pela API para a base de dados &rarr; [https://gitlab.com/MigDinny/dash-it/-/issues/229]
- Lista de commits de cada membro &rarr; [https://gitlab.com/MigDinny/dash-it/-/issues/234] &rarr; [https://gitlab.com/MigDinny/dash-it/-/merge_requests/113]
- Expandir ao clicar num commit da lista de commits &rarr; [https://gitlab.com/MigDinny/dash-it/-/issues/235] &rarr; [https://gitlab.com/MigDinny/dash-it/-/merge_requests/138]
- Ver issues abertos, fechados e total &rarr; [https://gitlab.com/MigDinny/dash-it/-/issues/242] &rarr; [https://gitlab.com/MigDinny/dash-it/-/merge_requests/132]
- Resolução de bugs &rarr; [https://gitlab.com/MigDinny/dash-it/-/issues/251] &rarr; [https://gitlab.com/MigDinny/dash-it/-/merge_requests/128]
- Histórico de ficheiros e folders (não implementado) &rarr; [https://gitlab.com/MigDinny/dash-it/-/issues/254]
- Criar vista perfil-individual &rarr; [https://gitlab.com/MigDinny/dash-it/-/issues/285] &rarr; [https://gitlab.com/MigDinny/dash-it/-/merge_requests/152]
- Inserir link para o perfil do user &rarr; [https://gitlab.com/MigDinny/dash-it/-/issues/286] &rarr; [https://gitlab.com/MigDinny/dash-it/-/merge_requests/152]
- Obter /spent de cada issue &rarr; [https://gitlab.com/MigDinny/dash-it/-/issues/287] &rarr; [https://gitlab.com/MigDinny/dash-it/-/merge_requests/153]
 


<br>

## **env**

<br>

## **pm**
- Criar perfil &rarr; [https://gitlab.com/MigDinny/dash-it/-/issues/47] &rarr; [https://gitlab.com/MigDinny/dash-it/-/merge_requests/39]

<br>

## **prod**


<br>

## **qa**

<br>

## **req**
