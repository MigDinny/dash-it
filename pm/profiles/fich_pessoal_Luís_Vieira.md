# **SOBRE MIM**

O meu nome é Luís Vieira, tenho 20 anos e estou no 3º ano do curso de Engenharia Informática, na universidade de Coimbra. Tenho uma preferência na área de engenharia de software, sendo que neste projeto o meu interesse consiste em desempenhar funções dos cargos de UI Design e Qualidade.

<br>

### **CONTACTOS**

**email:** lpedro.vieira18@gmail.com

<br>
<br>

# **CONTRIBUIÇÃO INDIVIDUAL**

**Papel no projeto:** Ao longo deste projeto fui membro da equipa de UI , tendo desonvildo código HTML , CSS e Javascript. Ajudei na organização do layout das diferentes vistas assim como na escolha da sua palete de cores. Fiz também alguma documentação e desenhei logos para a dashboard.

<br>

## **[proc]**
- Escrita de um tutorial de HTML. -> [documento] → #110 → !78;

<br>

## **des**

-  Atualização do HTML [commits.html](https://gitlab.com/MigDinny/dash-it/-/tree/dev/des/Vieira/commits_javascript) e criação do CSS [style.css](https://gitlab.com/MigDinny/dash-it/-/tree/dev/des/Vieira/commits_javascript/style.css) &rarr; Vista das commits → #266
- Atualização do HTML [commits.html](https://gitlab.com/MigDinny/dash-it/-/tree/dev/des/Vieira/commits_javascript) e criação de Javascript [commits.js](https://gitlab.com/MigDinny/dash-it/-/tree/dev/des/Vieira/commits_javascript/commits.js) para inclusão da vista de commits de 1 autor &rarr; Vista commits 1 autor → #266
- Atualização do HTML [commits.html](https://gitlab.com/MigDinny/dash-it/-/tree/dev/des/Vieira/commits_javascript) e criação de Javascript [commits.js](https://gitlab.com/MigDinny/dash-it/-/tree/dev/des/Vieira/commits_javascript/commits.js) para inclusao da vista de commits de 1 categoria &rarr; Vista commits 1 categoria → #266
- Atualização do HTML [oneissue.html](https://gitlab.com/MigDinny/dash-it/-/tree/vista_oneissue/des/Vieira/vista_oneissue/oneissue.html), criação do CSS [style.css](https://gitlab.com/MigDinny/dash-it/-/tree/vista_oneissue/des/Vieira/vista_oneissue/style.css) e criação de javascript [oneissue.js](https://gitlab.com/MigDinny/dash-it/-/tree/vista_oneissue/des/Vieira/vista_oneissue/oneissue.js) &rarr; Vista one issue → #284
- Criação de logos para o projeto &rarr; [pasta logos](https://gitlab.com/MigDinny/dash-it/-/tree/vista_oneissue/des/Vieira/logos) &rarr; #305
<br>


## **pm**
- Criar ficheiro pessoal &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/blob/dev/pm/profiles/fich_pessoal_Lu%C3%ADs_Vieira.md) &rarr; #57

<br>

## **req**
- Escrita dos tópicos de UI do Manual de requisitos (que posteriormente foram dados como não aplicavéis) &rarr; #236

## **OUTROS**
Para além das contribuições acima referidas , participei também em diversas reuniões da equipa de UI , na visualização de tutoriais e na alteração de documentação:

- Visualizar tutoriais GitLab &rarr; [#45](https://gitlab.com/MigDinny/dash-it/-/issues/45)
- Visualizar tutoriais Django &rarr; [#52](https://gitlab.com/MigDinny/dash-it/-/issues/52)
- Visualização de tutoriais de HTML. &rarr; #98;
- Correção de labels das issues UST &rarr; #197;
- Reunião da equipa de UI, no dia 11 de outubro. &rarr; #79;
- Reunião da equipa de UI, no dia 21 de novembro. &rarr; #214;





