# **SOBRE MIM**

O meu nome é Alexy de Almeida, tenho 26 anos e estou no 3º ano do curso de Engenharia Informática, na universidade de Coimbra. Tenho uma preferência na área de engenharia de software e algoritmos, sendo que, neste projeto o meu interesse consiste em desempenhar funções dos cargos de Gestão e Qualidade.

<br>

### **CONTACTOS**

**email:** alexx.da95@gmail.com <br>
**GitHub:** https://github.com/Squalexy

<br>
<br>

# **CONTRIBUIÇÃO INDIVIDUAL**

**Papel no projeto** <br>
&rarr; Fiz parte da equipa de gestão e fui mantainer. <br>
&rarr; Fui responsável pelo processo das atas: escrever, rever, enviar, escolher membros para a redação semanal, verificar se o processo e prazos estão a ser cumpridos, etc. <br>
&rarr; Criei vários tipos de documentação (templates, tutoriais, referências, readme etc). <br>
&rarr; Organizei o fluxo de trabalho de vários membros: criar announcements no canal de comunicação, ajudar membros em alguns processos de contribuição, etc. <br>
&rarr; Participei no desenvolvimento de código dos requisitos. <br>
&rarr; Enquanto mantainer, dei manage todos os Merge Requests que não tinham a ver com dev ou des, aceitando, recusando ou comentando neles; assim como dei manage das branches (ex: apagar branches inativas). <br>

<br>

## **[proc]**

- Escrita ata 1 &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/atas/pl10_ata1_v2.md) &rarr; [#16](https://gitlab.com/MigDinny/dash-it/-/issues/16) &rarr; [MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/19) 
- Escrita ata 2 &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/atas/pl10_ata2_v2.md) &rarr; [#17](https://gitlab.com/MigDinny/dash-it/-/issues/17) &rarr; [MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/19) 
- Criar tutorial ficheiro pessoal &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/tutorials/tutorial_ficheiro_pessoal.md) &rarr; [#20](https://gitlab.com/MigDinny/dash-it/-/issues/20) &rarr; [MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/15)
- Criar template atas &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/snippets/2208621) &rarr; [#51](https://gitlab.com/MigDinny/dash-it/-/issues/51) 
- Criar planilha das atas &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/planilha_atas.pdf)
- Criar tutoriais Django &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/tutorials/tutorial_django.md) &rarr; [#52](https://gitlab.com/MigDinny/dash-it/-/issues/52) 
- Criar tutoriais Git &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/tutorials/tutorial_git.md) &rarr; [#45](https://gitlab.com/MigDinny/dash-it/-/issues/45)

<br>

## **arch**
x

<br>

## **des**
x

<br>

## **dev**

- Visualizar tutoriais GitLab &rarr; [#45](https://gitlab.com/MigDinny/dash-it/-/issues/45)
- Visualizar tutoriais Django &rarr; [#52](https://gitlab.com/MigDinny/dash-it/-/issues/52)
- Passar no teste de conhecimentos de Django &rarr; [#201](https://gitlab.com/MigDinny/dash-it/-/issues/201)
- Desenvolver código REQ31 &rarr; [#234](https://gitlab.com/MigDinny/dash-it/-/issues/234) &rarr; [MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/113)
- Desenvolver código REQ29 &rarr; [#235](https://gitlab.com/MigDinny/dash-it/-/issues/235) &rarr; [MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/134) + [MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/138)


<br>

## **env**
x

<br>

## **pm**

- Criar ficheiro pessoal &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/blob/dev/pm/profiles/fich_pessoal_alexy.md) &rarr; [#19](https://gitlab.com/MigDinny/dash-it/-/issues/19) 
- Reuniões de gestão &rarr; [#118](https://gitlab.com/MigDinny/dash-it/-/issues/118) + [119](https://gitlab.com/MigDinny/dash-it/-/issues/119) (+ outras reuniões não mencionadas em issues)
- Aceitar vários Merge Requests (ver perfil com contribuição)
- Dar manage e apagar várias branches inativas (ver perfil com contribuição) &rarr; [#280](https://gitlab.com/MigDinny/dash-it/-/issues/280)
- Comentar em vários issues, commits e Merge Requests (ver perfil com contribuição)

<br>

## **prod**

x

<br>

## **qa**

- Criar readme (readme) &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/blob/dev/README.md) &rarr; [#3](https://gitlab.com/MigDinny/dash-it/-/issues/3) + [#100](https://gitlab.com/MigDinny/dash-it/-/issues/100)
- Criar procedimentos e guidelines (manual qualidade) &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/manual_qualidade.md) &rarr; [#3](https://gitlab.com/MigDinny/dash-it/-/issues/3)
- Criar tópico "how to to run" (readme) &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/blob/dev/README.md) &rarr; [#43](https://gitlab.com/MigDinny/dash-it/-/issues/43) &rarr; [MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/44)
- Criar templates para issues &rarr; [documentos](https://gitlab.com/MigDinny/dash-it/-/tree/dev/.gitlab/issue_templates) &rarr; [#44](https://gitlab.com/MigDinny/dash-it/-/issues/44) &rarr; [MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/46)
- Criar procedimento das atas (manual de qualidade) [documento](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/manual_qualidade.md#3-atas) &rarr; [#51](https://gitlab.com/MigDinny/dash-it/-/issues/51) + [#100](https://gitlab.com/MigDinny/dash-it/-/issues/100) 
- Criar referências (readme) &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/blob/dev/README.md) &rarr; [#135](https://gitlab.com/MigDinny/dash-it/-/issues/135)
- Criar template ficheiro pessoal &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/snippets/2182530)
- Criar template update contribuição pessoal &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/snippets/2220999)


<br>

## **req**

- Criar REQ10 &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/requirements_management/requirements?page=3&next=eyJjcmVhdGVkX2F0IjoiMjAyMS0xMC0xMCAyMTowMDowOC4yODMzNDIwMDAgVVRDIiwiaWQiOiIxNDExNyJ9&state=opened&sort=created_desc)
- Criar REQ28 &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/requirements_management/requirements?page=2&next=eyJjcmVhdGVkX2F0IjoiMjAyMS0xMS0xNSAxNDoxNzoxMS4zODMxMDkwMDAgVVRDIiwiaWQiOiIxNDYzNyJ9&state=opened&sort=created_desc)
