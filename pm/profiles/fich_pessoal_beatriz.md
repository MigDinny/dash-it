# **SOBRE MIM**

O meu nome é Beatriz Martinez, tenho 19 anos e estou no 3º ano do curso de Design e Multimédia, na universidade de Coimbra. Tenho uma preferência na área de design e ilustração, sendo que neste projeto o meu interesse consiste em desempenhar funções dos cargos de UI Design e Requisitos.

<br>

### **CONTACTOS**

**email:** beatriz.martinez209@gmail.com

## **CONTRIBUIÇÃO INDIVIDUAL**

##### **Papel no projeto**
→ Fiz parte da equipa de UI. <br>
→ Fiz os layouts e mockups da Dashboard. <br>
→ Defeni a paleta de cores, logo e tipografia da Dash'it. <br>
→ Fiz 5 vistas cada uma com HTML/PHP e CSS (Vistas principais: Landing page, Equipa, Login, e sub-vistas: Home Churn, Home Todas as Categorias). <br>


### **[proc]**

x



### **arch**

x


### **des**

Criar layouts das vistas da Dashboard → https://gitlab.com/MigDinny/dash-it/-/issues/75 <br>
Reunião UI Team → https://gitlab.com/MigDinny/dash-it/-/issues/79 <br>
Ver Tutoriais HTML → https://gitlab.com/MigDinny/dash-it/-/issues/98 <br>
Reunião para integrar os requisitos nos Layouts das Vistas 22/10 → https://gitlab.com/MigDinny/dash-it/-/issues/107 <br>
Finalização dos Layouts das Vistas → https://gitlab.com/MigDinny/dash-it/-/issues/111 https://gitlab.com/MigDinny/dash-it/-/merge_requests/97 !97 <br>
Layouts finalizadas: Home page, Social, Árvore, → https://gitlab.com/MigDinny/dash-it/-/issues/148 <br>
Reunião para terminar os Layouts das Vistas → https://gitlab.com/MigDinny/dash-it/-/issues/187 <br>
Reunião da Equipa de UI para terminar os requisitos → https://gitlab.com/MigDinny/dash-it/-/issues/214 <br>
Reformatação das Vistas → https://gitlab.com/MigDinny/dash-it/-/issues/216 <br>
Mockups das Vistas do Repositório → https://gitlab.com/MigDinny/dash-it/-/issues/233 https://gitlab.com/MigDinny/dash-it/-/merge_requests/115 https://gitlab.com/MigDinny/dash-it/-/merge_requests/123<br>
Alteração de descrições dos requisitos e distribuição pelas respetivas vistas → #222 https://gitlab.com/MigDinny/dash-it/-/merge_requests/108 <br>




### **dev**


x




### **env**

x


### **pm**


Criar ficheiro pessoal → documento →  https://gitlab.com/MigDinny/dash-it/-/issues/37 https://gitlab.com/MigDinny/dash-it/-/merge_requests/31   <br>

Visualizar tutoriais de Gitlab →   https://gitlab.com/MigDinny/dash-it/-/issues/45 <br>



### **prod**

x


### **qa**


x




### **req**


x
