# **SOBRE MIM**

O meu nome é Francisco Carreira, tenho 20 anos e estou no 3.º ano do curso de Engenharia Informática, na Universidade de Coimbra. Tenho uma preferência na área de Gestão de Desenvolvimento, sendo que neste projeto o meu interesse consiste em desempenhar diversas ajudas e acompanhar todo o processo de programação deste projeto.

Apesar da minha função neste projeto, o meu curriculum vitae consiste numa forte expriência no mundo da edição de vídeo, tendo já exercido funções em grandes empresas (i.e. ASUS Portugal) e mais recentemente, na criação de uma agência criativa especializada em criptoativos e em blockchain (i.e. Cardano e Ethereum).

<br>

### **CONTACTOS**

**email:** fmbcarreira@gmail.com<br>
**linkedin:** https://www.linkedin.com/in/carreiradotdev/

<br><br>

### **CONTRIBUIÇÃO INDIVIDUAL**

Durante a totalidade do projeto, as minhas contribuições foram nomeadamente para a equipa de desenvolvimento. Fiz 2 atas e coordenei 2 equipas de desenvolvimento com elementos fora da equipa principal de desenvolvimento.


### **CONTRIBUIÇÕES**

### DEV <br>

**1 -** Criação do primeiro modelo Entidade - Relacionamento para o projeto. Este, após revisão com a equipa de qualidade e desenvolvimento, pareceu desadequado e por isso foi revisto, contando também com a minha contríbuição.<br>
https://gitlab.com/MigDinny/dash-it/-/issues/84 
<br><br>

**2 -** Realização do exercício de verificação de conhecimentos do Django, este direcionado para a dev team. <br>
https://gitlab.com/MigDinny/dash-it/-/issues/114
<br><br>

**3 -** Implementação da primeira versão dos modelos de acordo com o antigo modelo Entidade - Relacionamento. <br>
https://gitlab.com/MigDinny/dash-it/-/merge_requests/100
<br><br>

**4 -** Implementação da vista *lista de ficheiros* onde são mostrados todos os ficheiros do repositório, requisito 19. <br>
https://gitlab.com/MigDinny/dash-it/-/issues/255
<br><br>

**5 -** Implementação de filtros na vista *lista de ficheiros*, é possível filtrar os ficheiros por autor e categoria, requisito 20. <br>
https://gitlab.com/MigDinny/dash-it/-/merge_requests/125
<br>

**6 -** Implementação da vista *ficheiro individual*, esta que conta com o título, dimensão e conteúdo do ficheiro, requisito 24. <br>
https://gitlab.com/MigDinny/dash-it/-/merge_requests/159
<br>


### REQUISITOS/UI <br>

-------------------------------------------------------------------------
**7 -** Participação numa das primeiras reuniões de User Interface, onde ajudei na coordenação desta equipa de acordo com os objetivos a curto termo da equipa de desenvolvimento. <br>
https://gitlab.com/MigDinny/dash-it/-/issues/75


-------------------------------------------------------------------------

### OUTROS: <br>

**8 -** Criação do Discord para comunicação das equipas via voice channels. <br>
https://gitlab.com/MigDinny/dash-it/-/issues/29 <br>

**9 -** Distribuição de tarefas para as equipas C, D. Durante a fase de desenvolvimento, implementei e ajudei os elementos das equipas a implementarem os requisitos, esclarecendo as dúvidas que surgissem. <br>

Equipas de desenvolvimento: <br>
https://gitlab.com/MigDinny/dash-it/-/issues/203 <br>
Issues que acompanhei/ajudei o desenvolvimento: <br>
https://gitlab.com/MigDinny/dash-it/-/issues/231 <br>
https://gitlab.com/MigDinny/dash-it/-/issues/227
<br>

**10 -** Ao longo do projeto fui comentando em issues pertinentes. <br>
https://gitlab.com/MigDinny/dash-it/-/issues/229

**11 -** Recolha de informação para a terceira ata.  <br>
https://gitlab.com/MigDinny/dash-it/-/issues/50

**12 -** Redação da sétima ata.<br>
https://gitlab.com/MigDinny/dash-it/-/issues/196



