# **SOBRE MIM**

O meu nome é Duarte Meneses, tenho 20 anos e estou no 3º ano do curso de Engenharia Informática, na Universidade de Coimbra. Tenho preferência na área de Sistemas Inteligentes, sendo que neste projeto o meu interesse consiste em desempenhar funções no cargo de Gestor de Qualidade.

<br>

### **CONTACTOS**

**email:** duartemeneses@student.dei.uc.pt

<br>
<br>

# **CONTRIBUIÇÃO INDIVIDUAL**

**Papel no projeto:** Fui gestor de qualidade, fazendo parte desta subunidade. Fui ainda membro da equipa de dev e gestor da equipa de testes;

<br>

## **[proc]**
- Tarefa 1 &rarr; Revisão da ata 1;
    - [#16] 
- Tarefa 2 &rarr; Revisão e envio da ata 2;
    - [#17] 
- Tarefa 3 &rarr; Revisão e envio da ata 3;
    - [#50] 
- Tarefa 4 &rarr; Revisão da ata 4;
    - [#61] 
- Tarefa 5 &rarr; Envio da ata 5;
    - [#87] 
- Tarefa 6 &rarr; Revisão e envio da ata 6;
    - [#97] &rarr; !61
- Tarefa 7 &rarr; Revisão da reescrita da ata 5;
    - [#108] 
- Tarefa 8 &rarr; Revisão da reescrita da ata 8;
    - [#219] - https://gitlab.com/MigDinny/dash-it/-/merge_requests/103#note_740557974
- Tarefa 9 &rarr; Merge Request da ata 9;
    - &rarr; !118


<br>

## **pm**
- Tarefa 1 &rarr; Criar perfil - Duarte Meneses:.
    - [#24] &rarr; !14
- Tarefa 2 &rarr; Fazer update ficheiro pessoal com contribuição
    - [#300]

<br>

## **qa**
- Tarefa 1 &rarr; Revisão do ficheiro de distribuição de cargos.
    - [#23]
- Tarefa 2 &rarr; Revisão do tutorial para criação de requisitos.
    - [#60]
- Tarefa 3 &rarr; Verificação da estrutura dos requisitos.
    - [#73]
- Tarefa 4 &rarr; Verificação do Diagrama ER Database.
    - [#84]- https://gitlab.com/MigDinny/dash-it/-/issues/84#note_729291994
           - https://gitlab.com/MigDinny/dash-it/-/issues/84#note_733534342     
- Tarefa 5 &rarr; Aprovação de Layouts.
    - [#90]
- Tarefa 6 &rarr; Revisão do tutorial de HTML.
    - [#110]
- Tarefa 7 &rarr; Revisão do update do manual de qualidade.
    - [#125]
- Tarefa 8 &rarr; Revisão da alteração de descrições dos requisitos e distribuição pelas respetivas vistas.
    - [#222]
- Tarefa 9 &rarr; Aprovar Update Verificacao_da_estrutura_dos_requisitos.
    - &&rarr !124
- Tarefa 10 &rarr; Aprovar Verificação dos tempos dos issues.
    - &&rarr !185 - https://gitlab.com/MigDinny/dash-it/-/merge_requests/120#note_749205702
- Tarefa 11 &rarr; Upload da Verificaçao da estrutura dos requisitos.
    - &&rrr !117 - https://gitlab.com/MigDinny/dash-it/-/merge_requests/117#note_746941357
- Tarefa 12 &rarr; Criar e rever Manual de Qualidade;  
    - [#291]
    - [#293]
    - [#294]
    - [#295]
    - [#292]


<br>

## **dev**
- Tarefa 1 &rarr; REQ 26: ver issues abertos, fechados e total
    - [#242] &rarr; !132 - https://gitlab.com/MigDinny/dash-it/-/merge_requests/132#note_752547347

<br>


## **Outros**
- Visualização de tutoriais django, gitlab e html.
    - [#45] - Visualizar tutoriais de Gitlab;
    - [#52] - Visualizar tutoriais de Django;
    - [#98] - Ver Tutoriais HTML;
    - [#201] - Teste de conhecimentos Django - https://gitlab.com/MigDinny/dash-it/-/issues/201#note_740214777
