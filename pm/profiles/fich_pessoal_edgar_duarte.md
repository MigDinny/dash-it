O meu nome é Edgar Duarte, tenho 20 anos e estou no 3º ano do curso de engenharia informática, na Universidade de Coimbra. Tenho uma preferência na área de engenharia de software, sendo que neste projeto o meu interesse consiste em desempenhar funções nos cargos de desenvolvimento. Gosto de pandas!

<br>

### **CONTACTOS**

**email:** edgartip2003@gmail.com

### **CONTRIBUIÇÕES**
<br>

### DEV <br> 


1- Criação do esqueleto inicial do código django <br>
https://gitlab.com/MigDinny/dash-it/-/issues/7;
<br>
<br>
2- Realização do exercício de verificação de aprendizagem do django <br>
https://gitlab.com/MigDinny/dash-it/-/issues/114#note_712408427
<br>
<br>
3- Discussão sobre arquitetura a implementar (resposta discutida em reunião privada com o tech lead)<br>
https://gitlab.com/MigDinny/dash-it/-/issues/105#note_720331280
<br>
<br>
4- Criação da versão final da base de dados visto que as versões anteriores não eram possíveis de ser implementadas/não respondiam a muitos requisitos. (requisitos/dev) <br>
https://gitlab.com/MigDinny/dash-it/-/issues/84#note_724498504 (no fundo)
<br>
<br>
5- Criação de um exemplo simples de pydriller para a equipa de dev rapidamente puder começar a programar. Devido a limitações da livraria, rapidamente foi abandonada esta ferramenta.<br>
https://gitlab.com/MigDinny/dash-it/-/issues/198
<br>
<br>
6- Implementação da search bar com o uso de uma cookie que guarda o útlimo registo <br>
https://gitlab.com/MigDinny/dash-it/-/issues/205
<br>
<br>
7- Implementação da lógica toda de buscar elementos da api para a base de dados (o crawler). Foram precisos vários ajustes na base de dados visto que se foram notando fraquezas na api. Também tentei fazer este processo o mais eficiente possível dentro do possível. Assim o resto da equipa dev apenas se tem de preocupar em ir buscar os dados à base de dados, não tendo de mexer com a api <br>
https://gitlab.com/MigDinny/dash-it/-/issues/207 <br>
https://gitlab.com/MigDinny/dash-it/-/issues/252 <br>
https://gitlab.com/MigDinny/dash-it/-/issues/256 
<br>
<br>
8- Discussão com a equipa de dev acerca de problemas com a api (dev/requisitos) <br>
https://gitlab.com/MigDinny/dash-it/-/issues/229
<br>
<br>
9- Correção de bugs <br>
https://gitlab.com/MigDinny/dash-it/-/issues/232 <br>
https://gitlab.com/MigDinny/dash-it/-/issues/257 <br>
https://gitlab.com/MigDinny/dash-it/-/merge_requests/135 <br>
https://gitlab.com/MigDinny/dash-it/-/merge_requests/141 <br>
https://gitlab.com/MigDinny/dash-it/-/merge_requests/142 <br>
https://gitlab.com/MigDinny/dash-it/-/merge_requests/148 <br>
https://gitlab.com/MigDinny/dash-it/-/merge_requests/151 <br>
https://gitlab.com/MigDinny/dash-it/-/issues/264#note_752686037 
<br>
<br>
10- Criação de um botão de refresh e da lógica para ir buscar novamente os dados à api a partir do valor da COOKIE e a sua posterior melhoria<br>
https://gitlab.com/MigDinny/dash-it/-/issues/239
https://gitlab.com/MigDinny/dash-it/-/issues/281
<br>
<br>
11- Participação, numa fase inicial, no deploy do projeto na máquina do DEI<br>
https://gitlab.com/MigDinny/dash-it/-/issues/126 <br>
<br>
(...)

### QUALIDADE <br>
<br>
12- Função de maintainer. <br>
Em geral: assegurar que o ambiente do projeto está saudável. <br>
Em específico: responsabilidade de verificar se issues estão corretas, de verificar se merge requests estão corretas e quando estas forem aprovadas, de verificar que ao dar merge nada se perde/a branch dev fique não funcional, verificar se não há branches esquecidas e se houverem apagá-las, verificar se não há issues abertas que já devam estar fechadas e se estas têm as labels corretas, verificar se merge requests têm issue associada, etc. Se alguma irregularidade for detetada fazer pedido de alteração aos criadores da issue/merge request <br>
exemplo: https://gitlab.com/MigDinny/dash-it/-/issues/18#note_691705993
<br>
<br>
13- Reviewer principal de várias merge requests <br>
alguns exemplos: <br>
https://gitlab.com/MigDinny/dash-it/-/merge_requests/79#note_726899685 <br>
https://gitlab.com/MigDinny/dash-it/-/merge_requests/94/diffs#note_726944108

### REQUISITOS <br>
<br>
14- Muitas discussões com os responsáveis da criação da base de dados sobre as suas propostas de base de dados e se era possível a sua implementação a partir das ferramentas disponibilizadas pela api do gitlab. (requisitos/qualidade) <br>
https://gitlab.com/MigDinny/dash-it/-/issues/84#note_724498504 <br>
https://gitlab.com/MigDinny/dash-it/-/issues/128#note_739258197 <br>
https://gitlab.com/MigDinny/dash-it/-/merge_requests/100#note_739263635 <br>
https://gitlab.com/MigDinny/dash-it/-/issues/254#note_751553444
<br>
<br>
15- Discussão com a equipa de requisitos acerca da implementação ou não de autenticação e login de utilizadores (requisitos) <br>
https://gitlab.com/MigDinny/dash-it/-/issues/206

### UI <br>

16-Criação do organograma em conjunto com a Joana Antunes. Fiz o esboço inicial e ela depois aperfeiçoou-o consoante feedback recebido.<br>
https://gitlab.com/MigDinny/dash-it/-/issues/109
<br>
<br>

### OUTROS: <br> 
17- Foi gasto também um tempo considerável em tarefas não públicas no gitlab, como, por exemplo, ajudar pessoas a utilizar o git, a discutir sobre requisitos fazíveis e não fazíveis com a gestora de requisitos e na atribuição de tarefas aos membros da equipa de dev. 

<br>
<br>
Resumidamente, fui o developer responsável pela ligação entre a base de dados e a API do gitlab (implementando requisitos associados),sendo que tive vários cargos em qualidade, visto ser maintainer, e em requisitos, visto  ser a única pessoa que conseguia contactar os membros da equipa de requisitos quanto à possibilidade de fazer ou não determinados requisitos, atendendo às funcionalidades disponíveis na API do gitlab.
