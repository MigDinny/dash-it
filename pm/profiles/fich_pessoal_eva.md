# **SOBRE MIM**

O meu nome é Eva Teixeira, tenho 19 anos e estou no 3º ano do curso de Engenharia Informática, na universidade de Coimbra. Tenho uma preferência na área de Software, sendo que neste projeto o meu interesse consiste em desempenhar funções dos cargos de Requisitos e Qualidade .



<br>

### **CONTACTOS**

**email:** evateixeira@student.dei.uc.pt

# **CONTRIBUIÇÃO INDIVIDUAL**

**Papel no projeto:** Ao longo do projeto fui membro da equipa de Requisitos e da equipa de Testes.

<br>

## **DEV**
- Visualizar tutoriais Gitlab &rarr; [#45](https://gitlab.com/MigDinny/dash-it/-/issues/45)
- Visualizar tutoriais Django &rarr; [#52](https://gitlab.com/MigDinny/dash-it/-/issues/52)
- Realizar um teste de conhecimentos de Django &rarr; [#201](https://gitlab.com/MigDinny/dash-it/-/issues/201)
- Visualizar tutorial de Html &rarr; [#98](https://gitlab.com/MigDinny/dash-it/-/issues/98)


<br>

## **PM**
- Criar ficheiro pessoal &rarr; [#46](https://gitlab.com/MigDinny/dash-it/-/issues/46) &rarr; [MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/38)

<br>

## **QA**
- Rever a conversão do manual de requisitos para formato Markdown &rarr; [#131](https://gitlab.com/MigDinny/dash-it/-/issues/131)
- Criar plano de testes de algumas vistas da nossa dashboard &rarr; [#228](https://gitlab.com/MigDinny/dash-it/-/issues/228)
- Testar a nossa dashboard &rarr; [#248](https://gitlab.com/MigDinny/dash-it/-/issues/248)
- Detetar e documentar erros encontrados na nossa dashboard &rarr; [#264](https://gitlab.com/MigDinny/dash-it/-/issues/264) + [#299](https://gitlab.com/MigDinny/dash-it/-/issues/299)
- Criar documento para enviar para outra turma &rarr; [#267](https://gitlab.com/MigDinny/dash-it/-/issues/267)
- Testar dashboard da PL9 e documentar &rarr; [#269](https://gitlab.com/MigDinny/dash-it/-/issues/269)

<br>

## **REQ**
- Reuniões de Requisitos para definir os requisitos e as suas prioridades &rarr; [#91](https://gitlab.com/MigDinny/dash-it/-/issues/46) + [#99](https://gitlab.com/MigDinny/dash-it/-/issues/99) + [#106](https://gitlab.com/MigDinny/dash-it/-/issues/106)
- Reunião de Requisitos para integrar os requisitos nos layouts das vistas &rarr; [#107](https://gitlab.com/MigDinny/dash-it/-/issues/107)
- Reunião de Requisitos sobre o Manual de Requisitos &rarr; [#121](https://gitlab.com/MigDinny/dash-it/-/issues/121)
- Criar REQ-9 &rarr; [#70](https://gitlab.com/MigDinny/dash-it/-/issues/70)
- Criar REQ-26 &rarr; [#141](https://gitlab.com/MigDinny/dash-it/-/issues/141)
- Dividir os primeiros 24 requisitos &rarr; [#60](https://gitlab.com/MigDinny/dash-it/-/issues/60)
- Atribuir requisitos a todos os membros da equipa &rarr; [#71](https://gitlab.com/MigDinny/dash-it/-/issues/71) &rarr; [MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/55)
- Escrever os tópicos 1, 1.1, 1.2, 1.5, 2, 2.6, 3, 3.1 e 3.6 do Manual de Requisitos &rarr; [#226](https://gitlab.com/MigDinny/dash-it/-/issues/226) +  [#260](https://gitlab.com/MigDinny/dash-it/-/issues/260)

<br>


