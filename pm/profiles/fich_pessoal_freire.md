# **SOBRE MIM**

O meu nome é João Freire, tenho 20 anos e estou no 3º ano do curso de Engenharia Informática, na universidade de Coimbra. Tenho uma preferência na área de Ciencia de Dados, sendo que neste projeto o meu interesse consiste em desempenhar funções dos cargos de Desenvolvimento, UI Design, Gestão.


<br>

### **CONTACTOS**

**email:** joaofreire.0710@gmail.com

915661408

### **Contribuição Individual** <br>

Para este projeto, o meu foco principal foi na minha equipa, a equipa de desenvolvimento. Desde o modelo ER da base de dados inicial as issues que foram feitas, o trabalho em equipa, mais em concreto a equipa C e D do desenvolvimento e todas as reuniões de DEV; Além disso fiz pequenos trabalhos para a equipa como uma redação de uma ata e recolha de informação de outra. 


### **DEV: ** <br>
**1 -** Criação do primeiro modelo de ER para a Base de Dados do projeto
https://gitlab.com/MigDinny/dash-it/-/issues/84 <br>

**2 -** Teste de conhecimento de Django para a equipa de Desenvolvimento 
https://gitlab.com/MigDinny/dash-it/-/issues/114 <br>

**3 -** Desenvolvimento de umas issues associadas a equipa, nomeadamente a parte de categorias e algum traalho de html 
https://gitlab.com/MigDinny/dash-it/-/issues/227 <br>
**4 -** Reunião de equipa para discutir alguns problemas: 
https://gitlab.com/MigDinny/dash-it/-/issues/231 <br>



### **Outros: ** <br>

**1 -** Reunião com equipa de desenvolvimento para a distribuição das tarefas entre todos os membros da equipa mas em concreto eequipa C e D. Fiz algumas e issues e ajudei os elementos que não eram de DEV a implementarem as suas issues <br>

**2 -** Criação das quipas de desenvolvimento: <br>
https://gitlab.com/MigDinny/dash-it/-/issues/203 <br>

**3 -** Recolha de informação para a 5ª ata.  <br>
https://gitlab.com/MigDinny/dash-it/-/issues/87

**4 -** Redação da 9ª ata.<br>
https://gitlab.com/MigDinny/dash-it/-/issues/240

