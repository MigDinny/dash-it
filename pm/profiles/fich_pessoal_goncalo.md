# **SOBRE MIM**

O meu nome é Gonçalo Pimenta, tenho 20 anos e estou no 3º ano do curso de Engenharia Informática, na universidade de Coimbra. Tenho uma preferência na área de programação, sendo que neste projeto o meu interesse consiste em desempenhar funções dos cargos de Requisitos.


<br>

### **CONTACTOS**

**087f.goncalopimenta@gmail.com** <EMAIL>
<br>
<br>

# **CONTRIBUIÇÃO INDIVIDUAL**

**Papel no projeto:** "Fiz requisitos nas 4 primeiras semanas e depois fiz trabalho de qualidade e desenvolvi código até ao final do projeto";

<br>

## **[proc]**

<br>

## **arch**


<br>

## **des**

<br>

## **dev**
1- Reunião para atribuir tarefas; &rarr; [#231]<br>
2- Implementação do requisito 15; &rarr; [#258] &rarr; [MR]https://gitlab.com/MigDinny/dash-it/-/merge_requests/133<br>
3- Implementação do requisito 6; &rarr; [#298]https://gitlab.com/MigDinny/dash-it/-/issues/298 &rarr; [MR]https://gitlab.com/MigDinny/dash-it/-/merge_requests/156<br>
<br>

## **env**
<br>


## **pm**

<br>

## **prod**

<br>

## **qa**
1- Verificar se todos os requisitos foram criados tal como era pedido &rarr; [#189]
<br>

## **req**

1- Reunião para discutir/definir a prioridade dos requisitos &rarr; [#91]<br>
2- Criação de um requisito &rarr; [#66]<br>
3- Reunião para organizar a prioridade dos requisitos &rarr; [#99]<br>
4- Update de todos os requisitos criados e respetivos issues &rarr; [#101]<br>
5- Reunião para escolher um template de ficheiro de requisitos; Dividir o ficheiro e atribuir as diferentes partes a cada uma das equipas;Distribuir as tarefas da equipa de requisitos pelos diferentes membro; &rarr; [#121]<br>
6- Criação de outro requisito &rarr; [#144]<br>


## **Outros**

1- Fiz trabalho de pesquisa para encontrar vários tipos de templates, de forma a automatizar o trabalho &rarr; [#44]<br>
5- Aprender a trabalhar com o Gitlab &rarr; [#45]<br>
6- Aprender a trabalhar com o Django &rarr; [#52]<br>
