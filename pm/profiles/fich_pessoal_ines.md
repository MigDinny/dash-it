# **SOBRE MIM**

O meu nome é Inês Marçal, tenho 20 anos e estou no 3º ano do curso de Engenharia Informática, na universidade de Coimbra. Tenho uma preferência na área de Sistemas Inteligentes, sendo que neste projeto o meu interesse consiste em desempenhar funções dos cargos de Qualidade, UI Design, Requisitos.

<br>

### **CONTACTOS**

**email:** inesmarcal@student.dei.uc.pt

**nº de telemóvel:** 926072241


<br>
<br>

# **CONTRIBUIÇÃO INDIVIDUAL**

**Papel no projeto:**
- Fiz parte da equipa de qualidade;
- Fiz parte da equipa de testes;
- Fui responsável por recolher informação para as atas assim como a sua revisão;
- Efetuei a revisão de documentos (readme...);
- Efetuei a verificação da estrutura dos requisitos assim como a colocação dos tempos nos issues de cada elemento.

<br>

## **[proc]**
- Tarefa 1 &rarr; Escrita da Ata nº6 - Aula, no dia 8 de novembro. &rarr; [#154] &rarr; !95;
- Tarefa 2 &rarr; Recolha de informação para as atas 6, 7 e 9;
	- [#154]; 
    - [#196];
    - [#240].
- Tarefa 3 &rarr; Revisão da ata 8. &rarr; [#219].

<br>

## **pm**

- Tarefa 1 &rarr; Criação do ficheiro de perfil pessoal. &rarr; [#31] &rarr; !26.

<br>

## **qa**

- Tarefa 1 &rarr; Aprovação de Layouts: Aprovação dos Layout criados pela UI Team; &rarr; [#90];
- Tarefa 2 &rarr; Verificacao_da_estrutura_dos_requisitos. &rarr; [#73] &rarr; !117; !124;
- Tarefa 3 &rarr; Verificacao_de_tempos_nos_issues. &rarr; [#185] &rarr; !120;
- Tarefa 4 &rarr; Contagem dos tempos feitos por cada membro. &rarr; [#303];
- Tarefa 5 &rarr; Revisão do ficheiro READme - Criação tutorial Git GUIs e SSH; [#103];
- Tarefa 6 &rarr; Testar dashboard e documentar;
	- [#247];
    - [#264]; 
    - [#299].
- Tarefa 7 &rarr; Criar plano de testes para os requisitos indicados na dashboard da PL9. &rarr; [#268];
- Tarefa 8 &rarr; Testar dashboard da PL9 e documentar os respetivos erros. &rarr;  https://gitlab.com/MigDinny/dash-it/-/blob/Plano_testes_pl9/qa/Plano_de_testes_pl9.md;
	- [#269].
- Tarefa 9 &rarr; Criar e rever Manual de Qualidade. &rarr; https://gitlab.com/MigDinny/dash-it/-/blob/criar_manual_qualidade/qa/manual_qualidade.md 
	- [#291]; 
    - [#294]; 
    - [#295]; 
    - [#292].

<br>

## **req**
- Tarefa 1 &rarr; Criação do plano de testes: Em conjunto com a Patrícia Costa foi criado um plano de testes para os requisitos 22, 30, 31, 43, mais especificamente para a Vista dos commits, Subvista dos commits - Vista de 1 autor, Subvista dos commits - Vista de 1 categoria,  Subvista dos commits - Vista de categorias, Subvista dos commits - Vista todos os autores e Subvista do histórico do ficheiro. &rarr; [#228]. 

<br>
<br>

*Outras contribuições:** Para além das contribuições acima, participei na visualização de tutoriais e ainda na realização de um teste de conhecimentos:

- Visualização de tutoriais de GitLab. &rarr; [#45];
- Visualização de tutoriais de Django. &rarr; [#52];
- Visualização de tutoriais de HTML. &rarr; [#98];
- Realização de um teste de conhecimentos de Django. &rarr; [#201].


