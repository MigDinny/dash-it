# **SOBRE MIM**

O meu nome é Joana Antunes, tenho 20 anos e estou no 3º ano do curso de Engenharia Informática, na universidade de Coimbra. Tenho uma preferência na área de Design, sendo que neste projeto o meu interesse consiste em desempenhar funções dos cargos de UI Design e Qualidade.

### **CONTACTOS**

**email:** <joanaantunes@student.dei.uc.pt>

<br>
<br>

# **CONTRIBUIÇÃO INDIVIDUAL**

**Papel no projeto:** Ao longo do projeto fui membro da equipa de UI, tendo desenvolvido código em CSS e HTML. Para além disso, fiz alguma documentação e escrevi 3 atas.

<br>

## **[proc]**
- Criação do Organograma da equipa. &rarr; #109 &rarr; !89;
- Escrita de um tutorial de HTML. &rarr; #110 &rarr; !78;
- Escrita da Ata nº1 - Reunião de UI/Requisitos, no dia 6 de outubro. &rarr; #61 &rarr; !51;
- Escrita da Ata nº6 - Aula, no dia 8 de novembro. &rarr; #154 &rarr; !95;
- Escrita da Ata nº8 - Aula, no dia 22 de novembro. &rarr; #219 &rarr; !103.

<br>

## **des**
- Criação do CSS [des/Joana/vistaPerfil.css] e atualização do HTML [des/Joana/perfil.html] - Vista do perfil individual. &rarr; #262;
- Criação do CSS [des/Joana/home_1_autor.css] e do HTML [des/Joana/home_1_autor.html] - Vista de um autor. &rarr; #282; 
- Criação do CSS [des/Joana/home_1_categoria.css] e do HTML [des/Joana/home_1_categoria.html] - Vista de uma categoria. &rarr; #283.

<br>

## **dev**
- Criação do HTML - Vista dos issues. &rarr; #215 &rarr; !104.

<br>

## **pm**
- Criação do ficheiro de perfil pessoal. &rarr; #33 &rarr; !27.

<br>
<br>

**Outras contribuições:** Para além das contribuições acima, participei em algumas reuniões entre equipas, na pesquisa para documentação a utilizar no projeto, na visualização de tutoriais e ainda na realização de um teste de conhecimentos:
- Pesquisa para a criação de templates para issues. &rarr; #44;
- Visualização de tutoriais de GitLab. &rarr; #45;
- Visualização de tutoriais de Django. &rarr; #52;
- Criação de layouts das vistas da Dashboard. &rarr; #75;
- Visualização de tutoriais de HTML. &rarr; #98;
- Reunião da equipa de UI, no dia 11 de outubro. &rarr; #79;
- Reunião das equipas de UI e de Requisitos, no dia 22 de outubro. &rarr; #107;
- Reunião da equipa de UI, no dia 21 de novembro. &rarr; #214;
- Realização de um teste de conhecimentos de Django. &rarr; #201.


