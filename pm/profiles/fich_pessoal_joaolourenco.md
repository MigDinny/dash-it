# **SOBRE MIM**

O meu nome é João António Simão Lourenço, tenho 20 anos e estou no 3º ano do curso de Engenharia Informática, na universidade de Coimbra. Tenho uma preferência na área de programacao de videojogos, sendo que neste projeto o meu interesse consiste em desempenhar funções dos cargos de UI e Qualidade.


<br>

### **CONTACTOS**

**email:** joao.antonio.s.lourenco@gmail.com

<br>
<br>

# **CONTRIBUIÇÃO INDIVIDUAL**

**Papel no projeto:** Durante este projeto fiz parte da equipa de UI, ou seja, fiz vistas (HTML, CSS e Javascript), ajudei a escolher o logotipo, palete de cores, e designs de páginas, e também participei em reuniões da equipa em questão. Fiz também um pouco de testagem e revisão de atas já na meta final do projeto.

<br>

## **[proc]**

- Revisão da ata nº10 &rarr; [pl10_ata10_v1.md] (https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/atas/pl10_ata10_v1.md) &rarr; [#270](https://gitlab.com/MigDinny/dash-it/-/issues/270) &rarr; [!44](https://gitlab.com/MigDinny/dash-it/-/merge_requests/144)

<br>

## **arch**

<br>

## **des**

- Criar layouts das vistas da Dashboard &rarr; [#75](https://gitlab.com/MigDinny/dash-it/-/issues/75)
- Reunião UI Team &rarr; [#79](https://gitlab.com/MigDinny/dash-it/-/issues/79)
- Criar Tutorial HTML &rarr;[tutorial_html.md](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/tutorials/tutorial_html.md) [#110](https://gitlab.com/MigDinny/dash-it/-/issues/110) &rarr; [!78](https://gitlab.com/MigDinny/dash-it/-/merge_requests/78)
- Criação do código CSS da página de Issues &rarr;[images][issues.html][style.css](https://gitlab.com/MigDinny/dash-it/-/tree/dev/des/Jo%C3%A3o) [#265](https://gitlab.com/MigDinny/dash-it/-/issues/265)
- Atualização do código JavaScript (Todas as categorias e Todos os autores) [commits.js](https://gitlab.com/MigDinny/dash-it/-/tree/dev/des/Vieira/commits_javascript) &rarr; [#266](https://gitlab.com/MigDinny/dash-it/-/issues/266)
- Ajuda no desenvolvimento da vista de uma issue &rarr;[des/Vieira/vista_oneissue](https://gitlab.com/MigDinny/dash-it/-/tree/vista_oneissue/des/Vieira/vista_oneissue) [#284](https://gitlab.com/MigDinny/dash-it/-/issues/284) 

<br>

## **dev**

<br>

## **env**

<br>

## **pm**

- Criar ficheiro pessoal &rarr; [fich_pessoal_joaolourenco.md](https://gitlab.com/MigDinny/dash-it/-/blob/dev/pm/profiles/fich_pessoal_joaolourenco.md) &rarr; [#53](https://gitlab.com/MigDinny/dash-it/-/issues/53) &rarr; [!52](https://gitlab.com/MigDinny/dash-it/-/merge_requests/52) 

<br>

## **prod**

<br>

## **qa**

<br>

## **req**
- Manual de Requisitos - Escrever tópicos UI  &rarr; [#236](https://gitlab.com/MigDinny/dash-it/-/issues/236)
<br>

## **TESTES**
- Criar plano de testes &rarr; [#228](https://gitlab.com/MigDinny/dash-it/-/issues/228)
- Testar dashboard - par 3 &rarr; [#248](https://gitlab.com/MigDinny/dash-it/-/issues/248)
- Criar plano de testes - PL9 &rarr; [#268](https://gitlab.com/MigDinny/dash-it/-/issues/268)
- Testar dashboard PL9 e documentar &rarr;[Plano_de_testes_pl9.md](https://gitlab.com/MigDinny/dash-it/-/blob/Plano_testes_pl9/qa/Plano_de_testes_pl9.md) [#269](https://gitlab.com/MigDinny/dash-it/-/issues/269)
<br>

## **Others**

- Visualizar tutoriais de Gitlab &rarr; [#45](https://gitlab.com/MigDinny/dash-it/-/issues/45) 
- Visualizar tutoriais de Django &rarr; [#52](https://gitlab.com/MigDinny/dash-it/-/issues/52) 
- Ver Tutoriais HTML &rarr; [#98](https://gitlab.com/MigDinny/dash-it/-/issues/98)
