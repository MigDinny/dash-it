# **SOBRE MIM**

O meu nome é Luís Braga, tenho 27 anos e estou no 3º ano do curso de Licenciatura em Design e Multimédia, na universidade de Coimbra. Tenho uma preferência na área de multimédia, sendo que neste projeto o meu interesse consiste em desempenhar funções dos cargos de <Requisitos e UI Design>.

<br>

### **CONTACTOS**

**email:** lfcbraga94@gmail.com

<br>
<br>

# **CONTRIBUIÇÃO INDIVIDUAL**

**Papel no projeto:** "Estive na equipa de requisitos durante 3 semanas e depois foi para UI onde me tornei o Gestor da Equipa de UI"

<br>

## **[proc]**

<br>

## **arch**


<br>

## **des**

- Tarefa 1 &rarr; [#75] https://gitlab.com/MigDinny/dash-it/-/issues/75 
- Reunião de UI para desenvolver 3 layouts &rarr; [#79] https://gitlab.com/MigDinny/dash-it/-/issues/79 
- Tarefa 3 &rarr; [#107] https://gitlab.com/MigDinny/dash-it/-/issues/107 
- Criação Inicial da Estrutura HTML do Projeto &rarr; [#120] https://gitlab.com/MigDinny/dash-it/-/issues/120 &rarr; [!65] https://gitlab.com/MigDinny/dash-it/-/merge_requests/65 [!66] https://gitlab.com/MigDinny/dash-it/-/merge_requests/66  [!67] https://gitlab.com/MigDinny/dash-it/-/merge_requests/67  [!68] https://gitlab.com/MigDinny/dash-it/-/merge_requests/68  [!69] https://gitlab.com/MigDinny/dash-it/-/merge_requests/69  [!70] https://gitlab.com/MigDinny/dash-it/-/merge_requests/70  [!71] https://gitlab.com/MigDinny/dash-it/-/merge_requests/71  [!79] https://gitlab.com/MigDinny/dash-it/-/merge_requests/79 
- Finalização dos Layouts das Vistas &rarr; [#187] https://gitlab.com/MigDinny/dash-it/-/issues/187 
- Finalização dos Mockups &rarr; [#214] https://gitlab.com/MigDinny/dash-it/-/issues/214 
- Reformatação das Vistas por motivos de legibilidade e simplicidade [#216] https://gitlab.com/MigDinny/dash-it/-/issues/216 
- Associação dos requisitos nos Mockups &rarr; [#222] https://gitlab.com/MigDinny/dash-it/-/issues/222 &rarr; [!108] https://gitlab.com/MigDinny/dash-it/-/merge_requests/108 
- Pequeno fix na Estrutura HTML &rarr; [#225] https://gitlab.com/MigDinny/dash-it/-/issues/225 &rarr; [!109] https://gitlab.com/MigDinny/dash-it/-/merge_requests/109 
- Redesenhamento dos Mockups finais das Vistas para satisfazer os novos requisitos &rarr; [#233] https://gitlab.com/MigDinny/dash-it/-/issues/233 &rarr; [!115] https://gitlab.com/MigDinny/dash-it/-/merge_requests/115  [123] https://gitlab.com/MigDinny/dash-it/-/merge_requests/123 
- Código HTML e CSS das Vistas que me ficaram designadas &rarr; [#259] https://gitlab.com/MigDinny/dash-it/-/issues/259 

<br>

## **dev**

<br>

## **env**

<br>

## **pm**

- Criação do Perfil &rarr; [#32] https://gitlab.com/MigDinny/dash-it/-/issues/32 &rarr; [!30] https://gitlab.com/MigDinny/dash-it/-/merge_requests/30 
- Visualização dos Tutoriais do GitLab &rarr; [#45] https://gitlab.com/MigDinny/dash-it/-/issues/45 
- Criação dos Tutorias de HTML &rarr; [#98] https://gitlab.com/MigDinny/dash-it/-/issues/98 
- Escrita da secção 3.4 do Manual de Qualidade &rarr; [#293] https://gitlab.com/MigDinny/dash-it/-/issues/293 
- Atualizar o Ficheiro Pessoal &rarr; [#300] https://gitlab.com/MigDinny/dash-it/-/issues/300 

<br>

## **prod**

<br>

## **qa**

<br>

## **req**

- Reunião de Requisitos para definir as suas prioridades &rarr; [#91] https://gitlab.com/MigDinny/dash-it/-/issues/91


**Outras tarefas pela quais fui responsável foram de documentação da equipa UI, distribuição de tarefas pela mesma, comunicação com outras equipas, Code Review relativo ao HTML e CSS da equipa de UI e esclarecimento de dúvidas relativos à programação Front-End.
