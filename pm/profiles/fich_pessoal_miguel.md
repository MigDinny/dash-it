# **SOBRE MIM**

O meu nome é Miguel Dinis, tenho 20 anos e estou no 3º ano do curso de Engenharia Informática, na universidade de Coimbra. Tenho uma preferência na área de programação back-end e de software, sendo que neste projeto o meu interesse consiste em desempenhar funções dos cargos de Gestão e Desenvolvimento.


<br>

### **CONTACTOS**

**email:** miguelbarroso@student.dei.uc.pt
<br>
**GitHub:** https://github.com/MigDinny
<br>
**Ainda não tenho portefólio :(**

# **CONTRIBUIÇÃO INDIVIDUAL**

**Papel no projeto:** Fui Tech Lead, arquitetei (com ajuda) o software e as suas componentes, fiz escolhas técnicas cruciais ao desempenho do software e orientei as tarefas dentro da minha equipa, prestei ajuda técnica e criei algum código inicial para orientar os devs. Mantive contacto com as restantes equipas para o bom workflow. Apesar de ser apenas Tech Lead, tive algumas contribuições importantes a nível de qualidade ao rever código vindo da UI e código dos devs (que precisavam de review antes do merge). Também contribuí (a nível de qualidade e requisitos) ao apontar algumas falhas na estrutura da equipa, no workflow e na forma como os requisitos estavam apresentados aos developers. 

<br>

## **[proc]**
- Sistema de Labels &rarr; [#6](https://gitlab.com/MigDinny/dash-it/-/issues/6)
- Processo de Contribuição Iterativo &rarr; [#204](https://gitlab.com/MigDinny/dash-it/-/issues/204)
- Criação de sub-equipas Dev para auxiliar na paralelização de trabalho para entregar numa deadline de 7 dias &rarr; [#203](https://gitlab.com/MigDinny/dash-it/-/issues/203)
- Criação de um Teste Django para avaliar o conhecimento da equipa (foi reutilizado 2x) &rarr; [#114](https://gitlab.com/MigDinny/dash-it/-/issues/114)
- Tutorial para Executar o Software no Docker &rarr; [#241](https://gitlab.com/MigDinny/dash-it/-/issues/241)
- Informação de algumas settings do VSCode para que todos as partilhem de igual forma (provavalmente não adotado) &rarr; [#112](https://gitlab.com/MigDinny/dash-it/-/issues/112)
- Atribuição de Pesos aos requisitos e definição uniforme de weights para a equipa usar &rarr; [#113](https://gitlab.com/MigDinny/dash-it/-/issues/113)
- Update ao Manual de Qualidade &rarr; [#125](https://gitlab.com/MigDinny/dash-it/-/issues/125)
- Primeiras Guidelines &rarr; [#3](https://gitlab.com/MigDinny/dash-it/-/issues/3)

<br>

## **arch**

- Desenho da arquitetura Django e do Software &rarr; [#105](https://gitlab.com/MigDinny/dash-it/-/issues/105)
- Desenho da arquitetura do Crawler (que navega pelas APIs para fazer fetch à informação) e desenvolvimento inicial, complementado pelo Edgar &rarr; [#207](https://gitlab.com/MigDinny/dash-it/-/issues/207) 

<br>

## **des**

- Primeira integração dos templates da UI, convertendo para notação Django &rarr; [#127](https://gitlab.com/MigDinny/dash-it/-/issues/127)
- Integração de CSS (feito parcialmente, devido a incompatibilidades) &rarr; [#288](https://gitlab.com/MigDinny/dash-it/-/issues/288)


<br>

## **dev**

- Correção de Bug &rarr; [#230](https://gitlab.com/MigDinny/dash-it/-/issues/230)
- Barra de pesquisa e primeiro código em Django que serviu como referência &rarr; [#205](https://gitlab.com/MigDinny/dash-it/-/issues/205)
- Apoio geral a todos os developers, orientação do trabalho a seguir e de **como** seguir, para manter a consistência entre todos os developers, para que usem as mesmas estratégias algorítmicas e reutilizem código eficientemente.

- (...) existem muitas outras contribuições que não aparecem nem como merge-request nem como issue porque foram diretamente para a Dev: resolução de bugs pequenos mas que crashavam o software, correção de problemas nas branches que estavam para entrar na dev, etc. 

A maior parte dos commits abaixo são de merges e pequenas alterações, mas existem alguns outros que foram contribuições diretas ([este](https://gitlab.com/MigDinny/dash-it/-/commit/26062dcfaa3abc20f0ca01cd5f7707e3fcd12df6), [este](https://gitlab.com/MigDinny/dash-it/-/commit/1e6311ba128cea0760263fdfe64fbf9de326c76a), [este](https://gitlab.com/MigDinny/dash-it/-/commit/bdb340d912dc29f3ffc4e5e8d30a25fc9938ab99) e [este](https://gitlab.com/MigDinny/dash-it/-/commit/515adf70c87426987e1397472f5d072c3f4893e8) )

https://gitlab.com/MigDinny/dash-it/-/commits/dev?author=Miguel%20Dinis

<br>

## **env**

<br>

## **pm**

- Não existe muita coisa documentada, mas houve algum trabalho junto da Project Management Team: discussões acerca dos trabalhos para a semana, orientações entre equipas, etc. Existem coisas nas atas, e outras não. Esse trabalho não é documentável porque envolve a comunicação entre os vários team-leaders para o bom flow do projeto.

<br>

## **prod**

- Deploy parte 1 &rarr; [#39](https://gitlab.com/MigDinny/dash-it/-/issues/39)
- Deploy, Docker, Pipeline e configuração de máquina virtual &rarr; [#126](https://gitlab.com/MigDinny/dash-it/-/issues/126)

- (...) existe algum trabalho off-stage não documentado, visto que o Deploy existe muita research e muito tempo dispendido. Com isso, houve muitas contribuições a nível de pipeline (ficheiros .yml) e a nível de Dockerfiles.

<br>

## **qa**

- (...) existem diversas MRs que foram testadas, aprovadas e merged por mim. Os testes feitos eram simples, apenas para garantir que o software não crashava ao receber a branch e era minimamente estável. Havia de vez em quando problemas que eram reportados ao criador da MR que eventualmente corrigia.
https://gitlab.com/MigDinny/dash-it/-/merge_requests?scope=all&state=all&reviewer_username=MigDinny
https://gitlab.com/MigDinny/dash-it/-/merge_requests?scope=all&state=all&approved_by_usernames[]=MigDinny


<br>

## **req**

