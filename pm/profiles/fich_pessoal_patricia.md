# **SOBRE MIM**

O meu nome é Patricia Costa, tenho 20 anos e estou no 3º ano do curso de Engenharia Informática, na universidade de Coimbra. Tenho uma preferência na área de Sistemas Inteligentes, sendo que neste projeto o meu interesse consiste em desempenhar funções dos cargos de Qualidade, UI Design, Requisitos.

<br>

### **CONTACTOS**

**email:** patriciabscosta2001@gmail.com

**nº de telemóvel:** 925429260

<br>
<br>

# **CONTRIBUIÇÃO INDIVIDUAL**

**Papel no projeto:** Fiz parte da equipa de qualidade e da equipa de testes;

<br>

## **[proc]**
- Tarefa 1 &rarr; Revisão da ata 2;
    - [#17] 
- Tarefa 2 &rarr; Revisão da ata 5;
    - [#87] 
- Tarefa 3 &rarr; Envio da ata 6;
    - [#154] 
- Tarefa 4 &rarr; Revisão da ata 7;
    - [#196] 
- Tarefa 5 &rarr; Recolha de informação para a ata 8;
    - [#219] 
- Tarefa 6 &rarr; Comentário na ata 9 para pedir correções na ata;
    - https://gitlab.com/MigDinny/dash-it/-/merge_requests/118#note_746964068
- Tarefa 7 &rarr; Comentário na ata 10 para pedir correções na ata.
    - https://gitlab.com/MigDinny/dash-it/-/merge_requests/144#note_754598936

<br>

## **pm**
- Tarefa 1 &rarr; Criar perfil - Patrícia Costa: criação de uma primeira versão do ficheiro de perfil;
    - [#30] &rarr; !25
- Tarefa 2 &rarr; Update ficheiro de perfil - Patricia Costa: segunda versão do ficheiro de perfil.
    - [#300] &rarr; !172

<br>

## **qa**
- Tarefa 1 &rarr; Aprovação de Layouts: Aprovação dos Layout criados pela UI Team;
    - [#90]
- Tarefa 2 &rarr; Verificar se foi colocado o tempo gasto nos issues: Verifiquei em conjunto com a Inês Marçal se os membros da equipa colocaram os tempos despendidos na realização das tarefas;
    - [#185] &rarr; !120
- Tarefa 3 &rarr; Correção do ficheiro de Verificação da estrutura dos requisitos;
    - [#73] &rarr; !124
- Tarefa 4 &rarr; Criar plano de testes - PL9 - Criar plano de testes para o requisito 106 da dashboard da PL9;
    - [#268] &rarr; https://gitlab.com/MigDinny/dash-it/-/blob/Plano_testes_pl9/qa/Plano_de_testes_pl9.md
- Tarefa 5 &rarr; Criar e rever Manual de Qualidade &rarr; https://gitlab.com/MigDinny/dash-it/-/blob/criar_manual_qualidade/qa/manual_qualidade.md
    - [#291]
    - [#294]
    - [#295]
    - [#292]
- Tarefa 6 &rarr; Contagem dos tempos feitos por cada membro para o manual de qualidade;
    - [#303]
- Tarefa 7 &rarr; Testar dashboard e documentar;
    - [#247]
    - [#264]
    - [#299]
- Tarefa 8 &rarr; Testar dashboard PL9 e documentar.
    - [#269]

<br>

## **req**
- Tarefa 1 &rarr; Criação do plano de testes: Em conjunto com a Inês Marçal foi criado um plano de testes para os requisitos 22,30, 31, 43 para a Vista dos commits, Subvista dos commits - Vista de 1 autor, Subvista dos commits - Vista de 1 categoria,  Subvista dos commits - Vista de categorias, Subvista dos commits - Vista todos os autores e Subvista do histórico do ficheiro.
    - [#228] Ponto 3.2 &rarr; https://gitlab.com/MigDinny/dash-it/-/blob/Update_requisitos/req/ManualRequisitos.md

<br>

## **Outros**
- Visualização de tutoriais django, gitlab e html de forma a perceber o que está a ser feito para rever e testar o que era pedido.
    - [#45] - Visualizar tutoriais de Gitlab;
    - [#52] - Visualizar tutoriais de Django;
    - [#98] - Ver Tutoriais HTML;
    - [#201] - Teste de conhecimentos Django - https://gitlab.com/MigDinny/dash-it/-/issues/201#note_739492585

