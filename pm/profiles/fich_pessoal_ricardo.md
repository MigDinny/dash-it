SOBRE MIM

O meu nome é Ricardo Vieira, tenho 25 anos e estou no 3º ano da Licenciatura em Engenharia Informática na Universidade de Coimbra. Tenho uma preferência na área de Requisitos, sendo que neste projeto o meu interesse consiste em desempenhar funções dos cargos de Gestor de Requesitos, Qualidade.


CONTACTOS

email: ricardojrsmvieira@gmail.com

<br>
<br>

# **CONTRIBUIÇÃO INDIVIDUAL**

**Papel no projeto:** Fui Gestor de Requisitos. Depois fiz parte da equipa de requisitos.

<br>

## **[proc]**
- Criar o ficheiro pessoal &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/42
- Reunião de requisitos &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/55
- Criação de um issue para um requisito &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/56
- Reunião com o cliente para esclarecimento sobre alguns pontos dos requisitos &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/59
- Criação do tutorial para a equipa saber como criar um issue e um requisito &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/60
- Criação do diagrama entidades-relacionamentos &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/84
- Atualizar ficheiro pessoal &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/304
<br>

## **arch**

- Criação de um issue para um requisito &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/56
- Criação do tutorial para a equipa saber como criar um issue e um requisito &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/60

<br>

## **des**

<br>

## **dev**

- Criação do diagrama entidades-relacionamentos &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/84
<br>

## **env**

<br>

## **pm**

- Criar o ficheiro pessoal &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/42
- Atualizar ficheiro pessoal &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/304
<br>

## **prod**


<br>

## **qa**

<br>

## **req**

- Reunião de requisitos &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/55
- Reunião com o cliente para esclarecimento sobre alguns pontos dos requisitos &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/59
