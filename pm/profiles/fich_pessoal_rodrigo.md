# **SOBRE MIM**

O meu nome é Rodrigo Ferreira, tenho 20 anos e estou no 3 º ano do curso de Engenharia Informática, na universidade de Coimbra. Tenho uma preferência na área de desenvolvimento, sendo que neste projeto o meu interesse consiste em desempenhar funções dos cargos de desenvolvimento e Qualidade.


<br>

### **CONTACTOS**

**email:** rodrigoff988@gmail.com <br>
**github:** https://github.com/IronFalcon0

<br><br>

### **CONTRIBUIÇÕES**

### DEV <br>

**1 -** Criação de um modelo ER de raiz após feedback de o modelo inicial estar bastante incompleto, bem como ir dando updates ao diagrama e ao models à medida que se descobriam erros ou ouve-se a necessidade de acrescentar parâmetros. Este foi um processo longo e demorado, onde ouve muita discussão. <br>
https://gitlab.com/MigDinny/dash-it/-/issues/84 
<br><br>

**2 -** Criação dos models baseados no diagrama ER, como o diagrama ER foi sofrendo alterações durante bastante tempo, a discussão passou para a issue #84. <br>
Issue original do models: <br>
https://gitlab.com/MigDinny/dash-it/-/issues/128 <br>
Issues criadas posteriormente para realizar alterações: <br>
https://gitlab.com/MigDinny/dash-it/-/issues/223 <br>
https://gitlab.com/MigDinny/dash-it/-/issues/253 <br>
(A issue 84 tb possui alguns updates ao models)
<br><br>

**3 -** Implementação da vista *lista de commits* onde são mostrados todos os commits do repositório bem como os filtros de pesquisa, requisito 30 e 31 respetivamente. <br>
https://gitlab.com/MigDinny/dash-it/-/issues/208 <br>
https://gitlab.com/MigDinny/dash-it/-/issues/234
<br><br>

**4 -** Implementação do requisito 15, na vista *departamento* é possível ver os membros ativos do repositório ordenados alfabeticamente. <br>
https://gitlab.com/MigDinny/dash-it/-/issues/250
<br><br>

**5 -** Criação da vista *perfil-individual*. <br>
https://gitlab.com/MigDinny/dash-it/-/issues/285
<br><br>

**6 -** Update ao requisito 15, inserir links na lista de membros ativos que redirecionam para a vista *perfil-individual* do respetivo membro. <br>
https://gitlab.com/MigDinny/dash-it/-/issues/286

**7 -** Implementação do requisito 19, obter o time_spent de cada issue e dividir pelo número de membros pertencentes à issue, é possível observar o tempo total de cada membro na lista de membros ativos bem como em cada perfil individual. Este valor nem sempre corresponde à verdade por não ser possível obter o tempo gasto de cada membro em cada issue, apenas o valor total da issue, pelo que irá inflacionar certos membros, e prejudicar outros. <br>
https://gitlab.com/MigDinny/dash-it/-/issues/287
<br><br>

**Entre outros<br>...** <br>

-------------------------------------------------------------------------

### REQUISITOS/UI <br>

**8 -** Bastante discussão com as equipas de requisitos, UI, gestão de projeto sobre mockups, requisitos não explícitos ou impossíveis de implementar, discussão essa que foi realizada fora do gitlab. 
<br><br>

-------------------------------------------------------------------------

### OUTROS: <br>

**9 -** Distribuição de tarefas para as equipas A, B e no req 15 para a equipa D. Durante a fase de desenvolvimento, implementei e ajudei os elementos das equipas a implementarem os requisitos, esclarecendo as dúvidas que surgissem. <br>

Equipas de desenvolvimento: <br>
https://gitlab.com/MigDinny/dash-it/-/issues/203 <br>
Issues que acompanhei/ajudei o desenvolvimento: <br>
https://gitlab.com/MigDinny/dash-it/-/issues/242 <br>
https://gitlab.com/MigDinny/dash-it/-/issues/250 <br>
https://gitlab.com/MigDinny/dash-it/-/issues/235 <br>
https://gitlab.com/MigDinny/dash-it/-/issues/208
<br><br>

**10 -** Ao longo do projeto fui comentando em algumas issues. <br>
https://gitlab.com/MigDinny/dash-it/-/issues/254 <br>
https://gitlab.com/MigDinny/dash-it/-/issues/84 <br>



