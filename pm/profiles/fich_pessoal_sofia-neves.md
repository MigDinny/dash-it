---
---

<p align="center">
    SOFIA SANTOS NEVES<br>
    <small> 2019220082
</p>

---

---

\
Sou uma rapariga com 20 anos de idade que, atualmente, está no 3º ano de Licenciatura em Engenharia Informática na FCTUC - Universidade de Coimbra. Nasci e vivi em Leiria a maior parte da minha vida.

\
**Competências:**

Sou uma pessoa interessada no que faço com curiosidade em aprender mais. Tenho experiência em linguagens como C, Java e Python e em ferramentas como Visual Studio Code e, brevemente, Git ;) .\
Tenho nível C2 tanto na língua portuguesa como inglesa.

\
**Objetivos:**

Pretendo terminar a licenciatura este ano letivo 2021/2022 e continuar os estudos indo para Mestrado na mesma área e, futuramente, trabalhar remotamente enquanto exploro o mundo.

\
**Interesses:**

- Informática
- Jogos de mesa
- Pássaros
- Psicologia
- Trilhos

Espero que tenha dado para me conhecer um pouco melhor. O meu email de trabalho é at.work.sofia@gmail.com.\
Obrigada por ter lido até aqui.
<br><br>

# **CONTRIBUIÇÃO INDIVIDUAL**

**Papel no projeto:** Fiz parte da equipa de gestão de projeto e fui a gestora de requisitos.

<br>

## **[proc]**

- Redigir ATA nº10 &rarr; [#270](https://gitlab.com/MigDinny/dash-it/-/issues/270)
- Divisão dos requisitos por toda a equipa PL10 para criação das UST &rarr; [#60](https://gitlab.com/MigDinny/dash-it/-/issues/60)
- Recolha de informação ATA 1 &rarr; [#16](https://gitlab.com/MigDinny/dash-it/-/issues/16)

<br>

## **arch**

- <br>

## **des**

- Visualização de tutoriais HTML &rarr; [#98](https://gitlab.com/MigDinny/dash-it/-/issues/98)
  <br>

## **dev**

- Visualização de tutoriais GitLab &rarr; [#45](https://gitlab.com/MigDinny/dash-it/-/issues/45)
  <br>

## **env**

- <br>

## **pm**

- Gestão do projeto &rarr; [#238](https://gitlab.com/MigDinny/dash-it/-/issues/238)
- Reunião de gestão 24/10 &rarr; [#119](https://gitlab.com/MigDinny/dash-it/-/issues/119)
- Reunião de gestão 31/10 &rarr; [#118](https://gitlab.com/MigDinny/dash-it/-/issues/118)
- Organização do canal de comunicação (Discord) &rarr; [#29](https://gitlab.com/MigDinny/dash-it/-/issues/29)
- Criação do perfil pessoal &rarr; [#27](https://gitlab.com/MigDinny/dash-it/-/issues/27)
- Criação de sheet com informação pessoal de todos os membros &rarr; [#22](https://gitlab.com/MigDinny/dash-it/-/issues/22)
- Criar estrutura do Repositório &rarr; [#5](https://gitlab.com/MigDinny/dash-it/-/issues/5)
  <br>

## **prod**

-

<br>

## **qa**

- Escrita do ponto req na secção 3.4 do Manual de Qualidade &rarr; [#293](https://gitlab.com/MigDinny/dash-it/-/issues/293)
- Atualização do Processo de Contribuição no Manual de Qualidade &rarr; [#204](https://gitlab.com/MigDinny/dash-it/-/issues/204)
- Criação de tutorial de integração Git em máquina local &rarr; [#83](https://gitlab.com/MigDinny/dash-it/-/issues/83)
  <br>

## **req**

- Adição de USTs ao Manual de Requisitos &rarr; [#306](https://gitlab.com/MigDinny/dash-it/-/issues/306)
- Adição de tabela de requisitos no Manual de Requisitos &rarr; [#279](https://gitlab.com/MigDinny/dash-it/-/issues/279)
- Documento enviado à outra turma dia 04.12.2021 &rarr; [#267](https://gitlab.com/MigDinny/dash-it/-/issues/267)
- Escrita dos tópicos 2.3, 4 e 5 do Manual de Requisitos &rarr; [#263](https://gitlab.com/MigDinny/dash-it/-/issues/263)
- Mapa de Navegação &rarr; [#249](https://gitlab.com/MigDinny/dash-it/-/issues/249)
- Ordenação dos requisitos por vista &rarr; [#222](https://gitlab.com/MigDinny/dash-it/-/issues/222)
- Resposta a questões sobre o requisito 39 &rarr; [#206](https://gitlab.com/MigDinny/dash-it/-/issues/206)
- Distribuição dos títulos do manual de Requisitos pelas diferentes equipas &rarr; [#121](https://gitlab.com/MigDinny/dash-it/-/issues/121)
- Reunião de integração de requisitos nas vistas &rarr; [#107](https://gitlab.com/MigDinny/dash-it/-/issues/107)
- Reunião requisitos pt.3 &rarr; [#106](https://gitlab.com/MigDinny/dash-it/-/issues/106)
- Reunião requisitos pt.2 &rarr; [#99](https://gitlab.com/MigDinny/dash-it/-/issues/99)
- Reunião requisitos pt.1 &rarr; [#91](https://gitlab.com/MigDinny/dash-it/-/issues/91)
