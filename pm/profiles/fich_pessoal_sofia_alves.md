# **SOBRE MIM**

O meu nome é Sofia Alves, tenho 20 anos e estou no 3º ano do curso de Engenharia Informática, na universidade de Coimbra. Tenho uma preferência na área de Design, sendo que neste projeto o meu interesse consiste em desempenhar funções dos cargos de  Gestão e UI Design.


<br>

### **CONTACTOS**

**email:** sofbotalves@gmail.com

**contacto telefonico:** 917718629

**github:** sbalves

# **CONTRIBUIÇÃO INDIVIDUAL**

**Papel no projeto:** <br> 
&rarr; Fiz parte da equipa de gestão do progeto e fui developer na antepenúltima semana do projeto; <br>
&rarr; Fui responsável por comunicar com os membros do projeto acerca de issues que não foram implementadas dentro do prazo definido;<br>
&rarr; Criei e contribuí para alguns tipos de documentação (tutoriais e diagramas);<br>
&rarr; Escrevi uma ata;<br>
&rarr; Recolhi infomação para 3 atas;<br>
&rarr; Participei no desenvolvimento de código de 1 requisito;<br>
<br>

## **[proc]**

- Tutorial de como adicionar weight &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/tutorials/tutorial_weights.md) &rarr;[#125](https://gitlab.com/MigDinny/dash-it/-/issues/125) &rarr; [MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/92)
- Diagrama BPMN &rarr; [diagrama](https://gitlab.com/MigDinny/dash-it/-/blob/criar_manual_qualidade/qa/Imagens/Fluxo_de_Trabalho_de_um_requisito.png) &rarr; [#290](https://gitlab.com/MigDinny/dash-it/-/issues/290) &rarr; [MR](https://gitlab.com/MigDinny/dash-it/-/issues/290)
- Escrita da ata 5 &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/atas/pl10_ata5_v3.md) &rarr; [#108](https://gitlab.com/MigDinny/dash-it/-/issues/108) &rarr; [MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/72)
- Recolha de informação da ata 1 &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/atas/pl10_ata1_v2.md) &rarr; [#16](https://gitlab.com/MigDinny/dash-it/-/issues/16) &rarr; [MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/19)
- Recolha de informação da ata 2 &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/atas/pl10_ata2_v2.md) &rarr; [#17](https://gitlab.com/MigDinny/dash-it/-/issues/17) &rarr; [MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/19)
- Recolha de informação da ata 10 &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/atas/pl10_ata10_v1.md) &rarr; [#270](https://gitlab.com/MigDinny/dash-it/-/issues/270) &rarr; [MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/144)

<br>

## **arch**

X

<br>

## **des**

X

<br>

## **dev**

- Visualizar tutoriais GitLab &rarr; [#45](https://gitlab.com/MigDinny/dash-it/-/issues/45)
- Visualizar tutoriais Django &rarr; [#52](https://gitlab.com/MigDinny/dash-it/-/issues/52)
- Passar no teste de conhecimentos de Django &rarr; [#201](https://gitlab.com/MigDinny/dash-it/-/issues/201)
- Desenvolver código REQ8 &rarr; [#227](https://gitlab.com/MigDinny/dash-it/-/issues/227) &rarr; [MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/136)
- Reunião com a equipa C e D &rarr; [#231](https://gitlab.com/MigDinny/dash-it/-/issues/231)
<br>

## **env**
X
<br>

## **pm**
- Criar ficheiro pessoal &rarr; [documento](https://gitlab.com/MigDinny/dash-it/-/blob/dev/pm/profiles/fich_pessoal_sofia_alves.md) &rarr; [#21](https://gitlab.com/MigDinny/dash-it/-/issues/21)
- Reuniões de gestão &rarr; [#118](https://gitlab.com/MigDinny/dash-it/-/issues/118) + [119](https://gitlab.com/MigDinny/dash-it/-/issues/119)


<br>

## **prod**
X
<br>

## **qa**
- Tutorial "Como correr" &rarr; [secção 1](https://gitlab.com/MigDinny/dash-it/-/tree/dev#1-instalar-pip-na-consola) | [secção 2](https://gitlab.com/MigDinny/dash-it/-/tree/dev#2-instalar-depend%C3%AAncias-no-pyhton) | [secção 4](https://gitlab.com/MigDinny/dash-it/-/tree/dev#4-executar-o-programa) &rarr; [#43](https://gitlab.com/MigDinny/dash-it/-/issues/43) &rarr; [MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/44)
- Complementar Manual de Qualidade &rarr; [secção 3](https://gitlab.com/MigDinny/dash-it/-/tree/dev#3-issues) &rarr; [#78](https://gitlab.com/MigDinny/dash-it/-/issues/78) &rarr; [MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/62) 

<br>

## **req**
- Desenvolver código REQ8 &rarr; [#227](https://gitlab.com/MigDinny/dash-it/-/issues/227) &rarr; [MR](https://gitlab.com/MigDinny/dash-it/-/merge_requests/136)
