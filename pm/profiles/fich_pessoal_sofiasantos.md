# **SOBRE MIM**

O meu nome é Sofia Santos, tenho 21 anos e estou no 3º ano do curso de LDM, na universidade de Coimbra. Tenho uma preferência na área de Webdesign, sendo que neste projeto o meu interesse consiste em desempenhar funções dos cargos de UI Design e Desenvolvimento.

### **CONTACTOS**

**email:** soficgs@gmail.com
https://www.behance.net/scgsantos

# **CONTRIBUIÇÃO INDIVIDUAL**

Fiz parte da equipa de UI e participei no desenvolvimento da parte de Front End (HTML + CSS).

### **dev**

- [Visualizar tutoriais de GitLab](https://gitlab.com/MigDinny/dash-it/-/issues/45)
- [Ver tutoriais de HTML](https://gitlab.com/MigDinny/dash-it/-/issues/98)
- [Reunião para terminar os Layouts das Vistas](https://gitlab.com/MigDinny/dash-it/-/issues/187)
- [Adição da stylesheet à página da estrutura Base.html](https://gitlab.com/MigDinny/dash-it/-/issues/224)

### **req**
- [Criar REQ28](https://gitlab.com/MigDinny/dash-it/-/issues/129)
