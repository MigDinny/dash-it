# **SOBRE MIM**

O meu nome é Tatiana Almeida, tenho 20 anos e estou no 3º ano do curso de engenharia informática, na universidade de Coimbra. Tenho uma preferência na área de sistemas inteligentes, sendo que neste projeto o meu interesse consiste em desempenhar funções nos cargos de gestora de projetos e fazer parte da equipa de requisitos.

<br>

### **CONTACTOS**

**email:** tatianasilvaalmeida24@gmail.com


# **CONTRIBUIÇÃO INDIVIDUAL**

**Papel no projeto:** Fui gestora de projeto, membro da subunidade de requisitos e da subunidade de testagem. Coordenei a equipa, interagindo com todos os membros mas com mais relevância aos gestores de cada departamento. Coordenei o departamento de gestão. Distribuí as tarefas semanalmente a toda a equipa e participei na criação e revisão de todos os processos. Contribuí para a documentação (ReadMe, Manual de Qualidade, Manual de Requisitos). Participei na revisão de vários documentos como (atas, contribuições para os Manuais)

## **[PROC]**
- Lista de cargos semana1 &rarr; [MR](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/Lista_de_Cargos_-_Dash_it_-v1.pdf)
- Atualização da lista de cargos &rarr; Manual de qualidade 
- Escrita do tutorial de contribuição pessoal &rarr; [MR](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/tutorials/tutorial_ficheiro_pessoal.md)
- Escrita do tutorial para criar um requirement &rarr; [MR](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/tutorials/tutorial_requirement.md)

## **[DEV]**
- Visualizar tutoriais de django &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/52
- Verificação do modelo ER &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/84
## **[QA]**
- Revisão de atas <br>
    &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/17 <br>
    &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/16 <br>
    &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/61
- Contribuições manual de qualidade <br>
    &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/122 <br>
    &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/302
- Equipa de testagem  <br>
    Criar plano de testes &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/228 <br>
    Testar a dashboard &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/246




## **[PM]**
- Criar ficheiro interno para organização com informações pessoais &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/22
- Reuniões <br>
    &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/118 <br>
    &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/119 <br>
- Aprovação de processos <br>
    &rarr; Divisão da equipa de desenvolvimento https://gitlab.com/MigDinny/dash-it/-/issues/203 <br>
    &rarr; Divisão da equipa de testes 
- Horas de gestão de projeto <br>
    &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/244 <br>
    &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/243 <br>


## **[DES]**
- Visualizar tutoriais de HTML &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/98
## **[REQ]**
- Reuniões <br>
     &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/84 <br>
     &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/91 <br>
     &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/99 <br>
     &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/106 <br>
     &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/121
- Reestruturação dos requisitos &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/123
- Alteração do template de como criar um requisito &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/124
- Alterações na pasta req https://gitlab.com/MigDinny/dash-it/-/issues/130
- Redifinição de prioridades e associação a vistas &rarr; https://gitlab.com/MigDinny/dash-it/-/issues/222

Outros: 
    Existem outras tarefas que não constam no gitlab mas que demoraram algum tempo a ser exercidas como por exemplo: Falar com diferentes membros da equipa para esclarecer dúvidas, alertar pessoas sobre a sua inatividade, distribuição de tarefas, announcements feitos no canal do discord. 


