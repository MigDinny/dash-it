# **Manual de Qualidade** | Dash'it

## Engenharia de Software - PL10

### Licenciatura em Engenharia Informática e Licenciatura em Design e Multimédia

###

#### Faculdade de Ciências e Tecnologia da Universidade de Coimbra

#### Ano letivo 2021/2022<br><br>

<div align="justify">

---

## **Índice**

**1.** [Sobre o manual de qualidade](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#1-sobre-o-manual-de-qualidade)<br>

- **1.1.** [Autores](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#11-autores)<br>
- **1.2.** [Histórico de versões](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#12-hist%C3%B3rico-de-vers%C3%B5es)<br>
- **1.3.** [Histórico de revisões](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#13-hist%C3%B3rico-de-revis%C3%B5es)<br>

**2.** [Em que consiste o projeto?](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#2-em-que-consiste-o-projeto)<br>

- **2.1.** [Apresentação](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#21-apresenta%C3%A7%C3%A3o)<br>
- **2.2.** [Propósito da equipa Dash'it](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#22-prop%C3%B3sito-da-equipa-dashit)<br>

**3.** [Gestão do projeto](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#3-gest%C3%A3o-do-projeto)<br>

- **3.1.** [Estrutura da equipa e divisão de tarefas](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#31-estrutura-da-equipa-e-divis%C3%A3o-de-tarefas)<br>
- **3.2.** [Estrutura do repositório do projeto](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#32-estrutura-do-reposit%C3%B3rio-do-projeto)<br>
- **3.3.** [Processos](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#33-processos)<br>

  - **3.3.1.** [Issues](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#331-issues)<br>
  - **3.3.2.** [Contribuições](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#332-contribui%C3%A7%C3%B5es)<br>
  - **3.3.3.** [Atas](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#333-atas)<br>
  - **3.3.4.** [Método de organização de algumas subunidades](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#334-m%C3%A9todo-de-organiza%C3%A7%C3%A3o-de-algumas-subunidades)
  - **3.3.5.** [Fluxo de trabalho de um requisito](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#335-fluxo-de-trabalho-de-um-requisito)<br>

- **3.4.** [Tarefas e esforço envolvido](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#34-tarefas-e-esfor%C3%A7o-envolvido)<br>
- **3.5.** [Como é realizada a gestão de projeto?](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#35-como-%C3%A9-realizada-a-gest%C3%A3o-de-projeto)<br>

**4.** [Produto](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#4-produto)<br>

- **4.1.** [Arquitetura e design](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#41-arquitetura-e-design)<br>
- **4.2.** [Modelo ER da base de dados](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#42-modelo-er-da-base-de-dados)<br>

**5.** [Qualidade](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#5-qualidade)<br>

- **5.1.** [Como é garantida a qualidade de processos?](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#51-como-%C3%A9-garantida-a-qualidade-de-processos)<br>
- **5.2.** [Como é garantida a qualidade de produto?](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#52-como-%C3%A9-garantida-a-qualidade-do-produto)<br>
- **5.3.** [Como é garantido que cada membro da equipa está apto a realizar as tarefas que lhe foram propostas?](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#53-como-%C3%A9-garantido-que-cada-membro-da-equipa-est%C3%A1-apto-a-realizar-as-tarefas-que-lhe-foram-propostas)<br>
- **5.4.** [Trabalho futuro](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#54-trabalho-futuro)<br>

**6.** [Anexos](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#6-anexos)<br>

- **6.1.** [Equipa](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#61-equipa)<br>
- **6.2.** [Plataformas utilizadas](https://gitlab.com/MigDinny/dash-it/-/blob/dev/qa/Manual_Qualidade.md#62-plataformas-utilizadas)<br><br>

---

## **1. Sobre o manual de qualidade**

### **1.1. Autores**

Para a versão final deste manual de qualidade contribuíram de forma ativa vários membros da equipa, de diversas subunidades. Estes são: <br>

- Alexy de Almeida (Gestão)<br>

- Duarte Meneses (Qualidade)<br>

- Gonçalo Lopes (Qualidade)<br>

- Inês Marçal (Qualidade)<br>

- Luís Braga (Design Visual - UI)<br>

- Miguel Barroso (Desenvolvimento)<br>

- Patrícia Costa (Qualidade)<br>

- Sofia Botelho Alves (Gestão)<br>

- Sofia Neves (Requisitos)<br>

- Tatiana Almeida (Gestão)<br>

### **1.2. Histórico de versões**

| **Versão** | **Data de Início** | **Data de Conclusão** | **Autores** |
| ---------- | ------------------ | --------------------- | ----------- |
| Versão 1.0 | 22/09/2021         | 8/11/2021             | Gestão      |
| Versão 1.1 | 8/11/2021          | 18/11/2021            | Gestão      |
| Versão 2.0 | 5/12/2021          | 13/12/2021            | Qualidade   |
| Versão 2.1 | 14/12/2021         | 18/12/2021            | Qualidade   |

### **1.3. Histórico de revisões**

Revisão 08/11/2021<br>

- Duarte Meneses (Qualidade)

Revisão 21/11/2021<br>

- Gonçalo Lopes (Qualidade)

Revisão 13/12/2021<br>

- Duarte Meneses (Qualidade)
- Gonçalo Lopes (Qualidade)
- Inês Marçal (Qualidade)
- Patricia Costa (Qualidade)
- Tatiana Almeida (Gestão)

Revisão 18/12/2021<br>

- Duarte Meneses (Qualidade)
- Gonçalo Lopes (Qualidade)
- Inês Marçal (Qualidade)
- Patricia Costa (Qualidade)
- Tatiana Almeida (Gestão)

<br>

---

## **2. Em que consiste o projeto?**

### **2.1. Apresentação**

- O grupo Dash'it foi criado no âmbito da cadeira de Engenharia de Sofware da Licenciatura em Engenharia Informática e da Licenciatura em Design e Multimédia do Departamento de Engenharia Informática da Universidade de Coimbra, lecionada pelo docente Mario Zenha-Rela.

### **2.2. Propósito da equipa Dash'it**

- Temos como objectivo o desenvolvimento de um produto que permita satisfazer o problema apresentado pelo cliente. Essa solução passa pelo desenvolvimento de uma dashboard de projeto, acessível via web; <br>
- Essa mesma dashboard deve apresentar várias vistas de um repositotório que se encontre no GitLab. Isto deverá permitir ao utilizador analisar os repositórios de uma forma mais rápida e intuitiva e deverá disponibilizar-lhe várias métricas; <br>
- Para o cumprimento do nosso objectivo, mantemos sempre presente os conceitos de gestão e processos mencionados pelo docente tanto nas aulas teóricas como nas aulas práticas.

<br>

---

## **3. Gestão do projeto**

### **3.1. Estrutura da equipa e divisão de tarefas** <BR>

<br>

> **Nota:** Todos os membros com '- G' depois do nome são gestores de subunidade

<br><br>
**Início do ano** <br><br>

| **Gestão**          | **Qualidade**      | **Desenvolvimento** | **Requisitos**     | **User Interface** |
| ------------------- | ------------------ | ------------------- | ------------------ | ------------------ |
| Tatiana Almeida - G | Duarte Meneses - G | Miguel Barroso - G  | Ricardo Vieira - G | Sofia Santos - G   |
| Alexy Almeida       | Inês Marçal        | Edgar Duarte        | Eva Teixeira       | Ana Martinez       |
| Sofia Botelho       | Gonçalo Lopes      | Francisco Carreira  | Gonçalo Pimenta    | João Lourenço      |
| Sofia Neves         | Filipe Viana       | João Catré          | Sofia Neves        | Luís Vieira        |
|                     | Patrícia Costa     | João Freire         | Tatiana Almeida    | Joana Antunes      |
|                     |                    | Rodrigo Ferreira    | Luís Braga         |                    |

**Semana 3** <br>

- Mudança dos gestores de UI e de Requisitos. <br><br>

| **Gestão**          | **Qualidade**      | **Desenvolvimento** | **Requisitos**  | **User Interface** |
| ------------------- | ------------------ | ------------------- | --------------- | ------------------ |
| Tatiana Almeida - G | Duarte Meneses - G | Miguel Barroso - G  | Sofia Neves - G | Luís Braga - G     |
| Alexy Almeida       | Inês Marçal        | Edgar Duarte        | Eva Teixeira    | Ana Martinez       |
| Sofia Botelho       | Filipe Viana       | Francisco Carreira  | Gonçalo Pimenta | Luís Vieira        |
| Sofia Neves         | Gonçalo Lopes      | João Catré          | Ricardo Vieira  | Joana Antunes      |
|                     | Patrícia Costa     | João Freire         | Tatiana Almeida | João Lourenço      |
|                     |                    | Rodrigo Ferreira    |                 | Sofia Santos       |

**Semana 8**

- Reforço na equipa de Dev e Criação da equipa de testagem;
- Gestor de qualidade assume o papel de gestor da equipa de Testagem. <br> <br>

| **Gestão**          | **Qualidade**      | **Desenvolvimento** | **Requisitos**  | **User Interface** | **Testagem**    |
| ------------------- | ------------------ | ------------------- | --------------- | ------------------ | --------------- |
| Tatiana Almeida - G | Duarte Meneses - G | Miguel Barroso - G  | Sofia Neves - G | Luís Braga - G     | Patrícia Costa  |
| Alexy Almeida       | Inês Marçal        | Alexy Almeida       | Eva Teixeira    | Ana Martinez       | Eva Teixeira    |
| Sofia Botelho       | Gonçalo Lopes      | Duarte Meneses      | Gonçalo Pimenta | Luís Vieira        | Gonçalo Lopes   |
| Sofia Neves         | Filipe Viana       | Edgar Duarte        | Ricardo Vieira  | Joana Antunes      | Inês Marçal     |
|                     | Patrícia Costa     | Francisco Carreira  | Tatiana Almeida | João Lourenço      | Filipe Viana    |
|                     |                    | Gonçalo Pimenta     |                 | Sofia Santos       | Tatiana Almeida |
|                     |                    | João Catré          |                 |                    |                 |
|                     |                    | João Freire         |                 |                    |                 |
|                     |                    | Rodrigo Ferreira    |                 |                    |                 |
|                     |                    | Sofia Alves         |                 |                    |                 |

**Semana 10**

- Desistência de um membro da equipa (Filipe Viana). <br><br>

| **Gestão**          | **Qualidade**      | **Desenvolvimento** | **Requisitos**  | **User Interface** | **Testagem**    |
| ------------------- | ------------------ | ------------------- | --------------- | ------------------ | --------------- |
| Tatiana Almeida - G | Duarte Meneses - G | Miguel Barroso - G  | Sofia Neves - G | Luís Braga - G     | Patrícia Costa  |
| Alexy Almeida       | Gonçalo Lopes      | Alexy Almeida       | Eva Teixeira    | Ana Martinez       | Eva Teixeira    |
| Sofia Botelho       | Inês Marçal        | Duarte Meneses      | Gonçalo Pimenta | Sofia Santos       | Gonçalo Lopes   |
| Sofia Neves         | Patrícia Costa     | Edgar Duarte        | João Lourenço   | Joana Antunes      | Inês Marçal     |
|                     |                    | Francisco Carreira  | Ricardo Vieira  | Luís Vieira        | João Lourenço   |
|                     |                    | Gonçalo Pimenta     |                 |                    | Tatiana Almeida |
|                     |                    | João Catré          |                 |                    |                 |
|                     |                    | João Freire         |                 |                    |                 |
|                     |                    | Rodrigo Ferreira    |                 |                    |                 |
|                     |                    | Sofia Alves         |                 |                    |                 |

## **Papel dos cargos**

<br>

**Gestora de Projeto -** Coordena a equipa de gestão e comunica diretamente com os restantes gestores, de forma a saber o ponto de situação de cada uma das subunidades. Distribuiu tarefas semanais e garante a gestão dos processos;
<br>

**Gestora de Requisitos -** Responsável pela comunicação direta com o cliente e interpretação dos requisitos, de forma a serem mais facilmente compreendidos e desenvolvidos pela equipa. Organiza reuniões e distribui tarefas pelos elementos da equipa;
<br>

**Gestor da Qualidade -** Assegura que todas as alterações/adições feitas no projeto são adequadas e corretas, de forma a evitar erros. Responsável pelo rigor do projeto e pela distribuição de tarefas aos diferentes membros;
<br>

**Gestor de UI -** Responsável pela comunicação com as restantes subunidades, organização de reuniões entre UI e distribuição de tarefas a realizar;
<br>

**Gestor de desenvolvimento -** Responsável pela organização de reuniões entre os elementos da sua subunidade e definição de tarefas a realizar;
<br>

**Gestor de Testagem -** Responsável por organizar a subunidade e atribuir tarefas aos diferentes membros.
<br><br>

### **3.2. Estrutura do repositório do projeto**
De seguida está representado o esqueleto do nosso projeto. Cada ficheiro deve estar no respetivo campo abaixo assinalado.
É também possível adicionar ficheiros novos desde que estes sejam devidamente contemplados no README do respetivo campo.

**PM** <br>
- READme PM;
- Ficheiros relacionados com a gestão do projeto;
- Dashboards do projeto;
- Ficheiros pessoais;
- Todos os riscos são tratados neste campo.

**REQ**<br>
- READme REQ;
- Requisitos do cliente, produto e técnicos;
- Design visual da aplicação;
- Cada feature tem o seu próprio ficheiro;
- Cada requisito deve estar assiciado a um conjunto de testes.

**ARCH**<br>
- READme ARCH;
- Ficheiros relacionados com técnologias e arquiteturas.

**DES**<br>
- READme DES;
- Design do Software detalhado.

**DEV**<br>
- READme DEV;
- Código fonte;
- Incluir ficheiros com instruções de compilação;
- Ficheiros das equipa de testagem e da equipa de qualidade;

Neste campo o código é a referencia principal pelo que os ficheiros relacionados com qualidade devem ser mantidos nos ficheiros do repositório.

**PROD**<br>
- READme PROD;
- Lista das várias versões do produto.

**QA**<br>
- READme QA
- Ficheiros relacionados com qualidade;


**ENV**<br>
- READme ENV;
- Conflitos de IDES e do Pipeline.

**[PROC]**<br>
- READme PROC;
- Todos os ficheiros relacionados com processos e práticas abordadas ao longo do projeto.
- Vídeos e tutoriais.

O *[PROC]* tem apenas o propósito regulamentar. As restantes pastas é que vão conter a informação.

**(External)**<br>
- READme EXT;
- Ficheiro opcional para colocar informação relacionada com terceiros.
<br>


### **3.3. Processos**

#### **3.3.1. Issues**

1. [Criar uma issue](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#create-a-new-issue)
2. [Mover uma issue](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#moving-issues)
3. [Mudar o tipo de issue](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#change-the-issue-type)
4. [Fechar uma issue](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues)

<br>

Na criação de issues é **impreterível** que se insiram todas as informações corretamente, sendo estas as seguintes:

- **Title:** verbo no infinitivo. Ex: "fazer logotipo", "desenvolver função x do código", "publicar ficheiro na página pessoal";
- **Description:** completar o título com informação relevante à issue, usando [sintaxe do Markdown](https://www.markdownguide.org/basic-syntax/);
- **Checkbox to make the issue confidential:** deixar sempre unchecked;
- **Assignee:** atribuir issue a si próprio e/ou mais membros da equipa (evita repetição de tickets);
- **Weight**: [seguir as convenções presentes no tutorial](https://gitlab.com/MigDinny/dash-it/-/blob/dev/%5Bproc%5D/tutorials/tutorial_weights.md);
- **Epic**: deixar vazio;
- **Due date**: atribuir um prazo realista, concretizável e que seja cumprido;
- **Milestone**: selecionar o objetivo a concretizar;
- **Labels**: atribuir todas as labels corretas. Ex: ocorreu um bug no código, vou atribuir as labels "Development" e "Bug";

  - **Atribuição de labels**

    - Grupo de trabalho: dev / req / quality / ui **(obrigatório pelo menos 1)**;
    - Tipo: documentation / bug / discussion / enhancement / suggestion / critical **(não obrigatório)**;
    - Status **(obrigatória 1 e só 1)**:

      - no label: ainda não tem label, acabou de ser criada e ainda não se sabe o destino;
      - waiting-assignment: à espera que alguém dê self-assign ou assign a outra pessoa, respeitando WIP 3 (!!);
      - work-in-progress: a ser trabalhada pelo dev respetivo;
      - waiting-approval: trabalho feito, à espera de revisor;
      - ready-to-merge: vários revisores aprovaram, pronto a ser merged com a main branch. Algum project-owner irá dar merge e fechar a issue.

      <BR>

- **Esforço estimado:** Estimar o tempo despendido para realizar a tarefa. Para tal, usar os comandos [aqui](https://docs.gitlab.com/ee/user/project/time_tracking.html#how-to-enter-data) referidos. <br>

  > Tip: Usar a ferramenta [toggl](https://toggl.com/) para cronometrar o tempo despendido na realização da issue

<br>

#### **3.3.2. Contribuições**

1. Identificar a issue: trata-se de resolver um problema, corrigir um bug, executar uma tarefa,..?;
2. Criar a issue/ticket;
3. [Criar branch](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#create-a-branch);
4. Trabalhar na solução e testar;
5. Dar push;
6. Garantir que a branch vai atualizada, sem merge conflicts (ver abaixo caso existam merge conflicts);
7. Git push, criando MR no sentido "**bug-fix-123132 --> dev**";
   - Sintetizar o que foi feito na forma de texto;
   - Atualizar a label da issue associada;
   - Associar a issue à MR usando hashtag;
   - Exemplo 2: "este MR diz respeito à issue #6".
   1. Colocar as labels corretamente e esperar aprovação;
   2. Depois do merge, a branch local poderá (e deverá) ser apagada -- para isto existe uma opção na altura de criar MR, na forma de uma checkbox. Algo como "auto delete branch after merge". Branches antigas e sem open MR associada serão apagadas.<br><br>
      > **Nota:** A maior parte destas etapas pode ser realizada no próprio GitLab, sendo recomendado [ler a documentação oficial](https://docs.gitlab.com/ee/topics/use_gitlab.html)

**! Merge conflicts:** No código aparece onde existem conflitos, mostrando 2 versões diferentes: a vossa versão da branch e a versão da dev branch do projeto, de alguém que entretanto alterou recentemente. Este tipo de problemas acontece sobretudo em branches que são criadas num dia e só muito tempo depois é que entram em processo de merge.<br>

- **Solução 1:** escolher a versão mais adequada do código;
- **Solução 2:** juntar as duas versões. Aqui é necessária a inspeção minuciosa para que não se apague código de outras pessoas. É uma tarefa com um elevado perigo se não for feita como deve ser.
  <br><br>


#### **3.3.3. Atas**

**-- Antes da aula --**

1. (GESTÃO + QUALIDADE) Selecionar os membros encarregues de recolher informação + escrever + rever, no Discord;
2. (GESTÃO) Dar tag aos membros indicados no canal "atas_info".

**-- Durante a aula --**

1. (QUEM RECOLHE INFORMAÇÃO + ESCRITOR) Recolher informação;<br>
   > **IMPORTANTE:** Quem escrever ou recolher informação e chegar mais de 10 minutos depois do docente à aula leva um aviso. Ao segundo aviso deixa de poder participar nestas tarefas.<br>
2. (ESCRITOR) Começar a escrever a ata.<br>

**-- Após a aula --**

1. (ESCRITOR) Criar ticket;<br><br>
   **ATÉ ÀS 20H:**
2. (ESCRITOR) Escrever a ata (.md);<br>
3. (ESCRITOR) Criar nova branch e publicar a ata (.md) no GitLab, identificado os reviewers (ver no canal "atas_info"); **é feito um MR**;<br><br>
   **ATÉ ÀS 22H:**
4. (QUALIDADE) Rever a ata e mandar pedido de alterações (se se verificar); **aconselhamos a discutirem todas essas alterações nos comentários do MR**;
5. (ESCRITOR) Aplicar alterações, trabalhando sempre na mesma branch;
6. (QUALIDADE) Publicar a ata (.pdf, ver nota sobre conversor), no canal "atas_discussion", para ser feita a revisão e discussão acerca da mesma por todos os membros; <br><br>
   **ATÉ ÀS 11H, TERÇA-FEIRA:**
7. (MEMBROS) Discutir/propôr alterações à ata/possíveis erros que ainda estejam presentes; <br><br>
   **ENTRE AS 11H e 14H, TERÇA-FEIRA:**
8. (Alexy) Aceitar Merge Request e enviar email para o docente da pl com um link para a ata.<br>
   > **Nota:** Usar [este site](https://md2pdf.netlify.app/) para converter md para pdf.

**-- Após revisão do docente --**

1. (Alexy) Mandar alterações exigidas pelo docente ao escritor;
2. Repetir passos 2 a 8.

<br>

**Exemplos formatos de filename**

- Ata de aula
  - pl10_ata2_v2.md
  - pl10_ata10_v1.md

> Nota: o nº da ata é sempre cumulativo, isto é, uma ata de aula que seja a nº7, se a próxima ata será de ui/req então passará a ser a ata nº8.

<br>

**Formato do ticket**

- Título: "Redigir ata x" (onde x é o nº da ata);
- Descrição: escolher o template "Ata";
- Dar sempre assign a todos os envolventes da ata (quem recolhe informação, quem escreve, quem revê, quem envia ao docente);
- Deadline para o dia seguinte (24h);
- Labels: documentation, work-in-progress (alterar consoante o estado do ticket).

<br>

**Outras indicações**

- 2 pessoas a recolher informação na aula;

  > Nota: pode haver backup caso alguma das pessoas não esteja presente, mas é de evitar!<br>

- Quem escreve a ata também recolhe informação;<br>

- Quem escreve tem de tentar seguir o mais parecido possível a estrutura e formatação das atas anteriores, visto que ter consistência facilita o trabalho de quem vai escrever, rever e ler (podem alterar algumas coisas livremente desde que não seja nada excessivo);<br>

- Existe rotatividade para todos os interessados participarem nas tarefas das atas.


#### **3.3.4. Método de organização de algumas subunidades**

###### **Back-end** <br>
Foram criadas várias equipas de desenvolvimento para conseguir cobrir as diferentes vistas da dashboard a nível de back-end e front-end.

Para conseguir seguir o processo de contribuição das diferentes equipas, cada um dos responsáveis da equipa cria a sua milestone.

De seguida, cada um dos membros da equipa deve seguir os seguintes passos para contribuir para um dado requisito:

1. Pegar num requisito com prioridade mais elevada (MUST - elevada prioridade, SHOULD - prioridade média, COULD - prioridade baixa) associado à milestone da sua equipa
2. Se for necessário criar uma ou várias novas issues, colocar parâmetros _spent_ e _estimate_ associados à milestone correta.
3. Resolver a(s) issue(s)
4. Criar um ou vários MR's (merge-requests)
5. Esperar pela aprovação
6. Repetir o ciclo voltando à etapa 1. , quando terminarem todos os requisitos de prioridade elevada e média, mudar de vista ou equipa conforme necessário
   <br>

###### **Front-end** <br>
Para a organização da equipa de Front-end, o UI manager permitiu à sua equipa escolher em que vista queria trabalhar. 

O mesmo dividiu todas as vistas, escolhendo 6 delas como prioritárias e pediu a cada membro da sua equipa que escolhesse 3 vistas, sendo uma delas prioritária que seria a primeira a ter de ser implementada. 

Cada um dos membros criou os seus próprios issues e fechou os mesmos quando concluiu o seu trabalho. 

###### **Test Team** <br>
A equipa de testagem, constituida por 6 membros, foi dividida em 3 subequipas de 2 elementos. 
O gestor da equipa dividiu as diferentes vistas pelas subequipas para que podessem ser criados os planos de teste. 
Foi o gestor de criou o issue onde cada uma das equipas, depois de terminar a tarefa, assinalou que a mesma tinha sido feita. 
Quando chegou a altura de testar cada equipa testou os requisitos que se encontravam associados às vistas às quais tinham feito o plano de testes, mas houve margem para mudar as vistas, para que nao houvesse ninguem sem trabalho e alguem sobre-carregado. No entanto, no caso da nossa equipa, isso não aconteceu e a distribuição inicial manteve-se inalterada até ao fim do processo.

#### **3.3.5. Fluxo de trabalho de um requisito**<br>

- [Workflow](https://gitlab.com/MigDinny/dash-it/-/blob/criar_manual_qualidade/qa/Imagens/Fluxo_de_Trabalho_de_um_requisito.png).

<br>

### **3.4. Tarefas e esforço envolvido**

- **Qualidade:** A subunidade de qualidade esteve encarregue de analisar e validar todos os processos e todas as tarefas realizadas. Com isto, destacam-se a revisão de atas, a revisão de tutoriais (dos processos) e a revisão de todos os processos (se estes estão a ser bem executados). Em suma, esta subunidade assegurou o rigor do projeto e foi a principal responsável pela elaboração do manual de qualidade;

- **Desenvolvimento:** A subunidade de desenvolvimento esteve encarregue de fazer toda a arquitetura do back-end, base de dados e crawler da API. Programou ainda o back-end e todas as suas complexas features associadas. Foi responsável pela integração do código vindo da UI e a respetiva revisão técnica. Em síntese, esta subunidade tratou de toda a parte técnica e de desenvolvimento de requisitos;

- **Gestão de projetos:** A gestora de projeto coordenou a equipa de gestão e comunicou diretamente com os restantes gestores, de forma a saber o ponto de situação de cada uma das restantes equipas. Foi também responsável pela distribuição de algumas das tarefas semanais. Os restantes membros da gestão de projeto foram responsáveis por dar Merge a tudo o que era documentação, pelo envio e organização das atas (escolhendo as pessoas envolvidas na sua escrita), por comunicar o não cumprimento de prazos de issues ou faltas destes, organizar os canais de comunicação entre as equipas no Discord e avisar os elementos cujos issues não foram realizados no prazo estipulado;

- **Design visual - UI:** A subunidade de UI ficou encarregue de receber os requisitos criados pela equipa de Requisitos e de criar, primeiro, layouts do user path que resolvessem os requisitos. Em seguida, criou os mockups para servir de base visual do aspeto das vistas e também foi responsável pela criação da estrutura e da estilização das vistas;
- **Requisitos:** A subunidade de requisitos ficou encarregue de compreender e interpretar os requisitos pedidos pelo cliente e de falar com o mesmo. De seguida, foi feita uma distribuição dos requisitos pelos vários membros das equipas poderem criar a respetiva UST. Também esteve encarregue de dividir os requisitos por vista na dashboard, criar os requisitos no GitLab e definir as prioridades dos mesmos. Para além disso, manteve uma relação próxima com a subunidade de UI no que toca a explicar o que é necessário em cada vista para a dashboard e respetiva adição dos requisitos nas mesmas. Por último, também ficou encarregue de criar um mapa de navegação e de criar o manual de requisitos;

- **Testagem:** A subunidade de testagem ficou encarregue, inicialmente, de criar um plano de testes para todos os requisitos. De seguida, após estes terem sido implementados pelo departamento de desenvolvimento, a subunidade de testes procedeu à testagem e documentação dos resultados. No caso de haverem falhas no código, os resultados eram enviados para o departamento de desenvolvimento.

##### [_Mapa de esforço_](https://gitlab.com/MigDinny/dash-it/-/blob/criar_manual_qualidade/qa/Imagens/ContagemTempos.png)

<br>

### **3.5. Como é realizada a gestão de projeto?**

Todos os domingos, a subunidade de Gestão de Projeto reunia-se para discutir o progresso do projeto na semana que tinha passado: discutia se algo havia de mudar e delineavam novas metas para a semana que se avizinhava. <br>
Todas as segundas-feiras, após a aula prática, a Gestora de Projeto reunia as tarefas a distribuir para a semana, apresentava-as à equipa e dava a escolher.

<br>

---

## **4. Produto**

### **4.1. Arquitetura e design** <BR>

- [Arquitetura Software](https://gitlab.com/MigDinny/dash-it/-/blob/dev/arch/diagramas/arquitetura%20software.png);
- [Arquitetura Python Django](https://gitlab.com/MigDinny/dash-it/-/blob/dev/arch/diagramas/arquitetura%20python%20django.png);
- [Design](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/Vistas.md).


### **4.2. Modelo ER da base de dados**

- [Modelo ER da base de dados](https://gitlab.com/MigDinny/dash-it/-/blob/dev/arch/diagramas/model_ER_ES_final_4.png).

<br>

---

## **5. Qualidade**

<br>

### **5.1. Como é garantida a qualidade de processos?**

Na maioria dos processos (redação de atas, criação de issues,...) foi desenvolvido um tutorial. Nestes casos a subunidade de Qualidade ficou encarregue de o validar. Para isso, era posto em prática o tutorial por membros da Qualidade e, caso todos conseguissem realizar a tarefa correspondente ao processo sem erros e sem dúvidas, o tutorial era validado. Caso contrário, eram apontadas as falhas ao tutorial e este seria novamente desenvolvido pela subunidade que já o tinha feito. Após essa nova versão do tutorial, todos os passos que a Qualidade desempenhou até então, eram repetidos.<br>
Mesmo nestes casos em que existiam tutoriais, a subunidade da Qualidade verificou, na maioria das vezes, o resultado das tarefas desenvolvidas pela equipa com o intuito de verificar se o tutorial estava a ser seguido corretamente.<br>
Já no caso dos processos não terem um tutorial onde os membros da equipa se pudessem guiar, todos os resultados de determinada tarefa foram verificados por pelo menos um membro da subunidade da Qualidade. Com isto, esta subequipa conseguia aferir se um processo estava a ser mal implementado (gerando resultados diferentes dos expectáveis).

<br>

### **5.2. Como é garantida a qualidade do produto?**

Qualquer tarefa realizada e que tenha resultados práticos relevantes para o produto é validada pela subunidade de Qualidade. Nestas tarefas destacam-se as que têm como resultado a implementação de requisitos (a serem validados pela subunidade de testes), a criação de mockups, a junção de mockups com os requisitos e o detalhar dos requisitos.<br>
Após a integração do trabalho do backend com o frontend, o produto é novamente testado pela equipa de testes.

<br>

### **5.3. Como é garantido que cada membro da equipa está apto a realizar as tarefas que lhe foram propostas?**

Ao início, cada membro da equipa pôde escolher a subunidade a integrar. Se estas fossem mais técnicas como a de desenvolvimento ou a de design visual (UI), todos os seus membros deviam visualizar tutoriais. No caso de desenvolvimento, foi necessário ainda realizar um pequeno teste para aferir se todos estavam aptos às tarefas que lhes iriam ser propostas. Caso não passassem no teste, os membros deveriam voltar a tentar até conseguir. <br>
Com o decorrer do projeto, foi-se analisando se as tarefas estavam a ser bem desempenhadas por todos os membros. Caso fosse detetada alguma falha, era comunicada ao membro em questão e era sugerido trocar de função (sobretudo se fosse gestor).

<br>

### **5.4. Trabalho futuro**

Chegando à fase final do projeto, ficaram alguns requisitos por implementar. Deste modo, entendemos que este seria o próximo passo caso o projeto continuasse. A parte visual seria ainda um aspeto que gostavamos de rever.

<br>

---

## **6. Anexos**

### **6.1. Equipa**

- Alexy Almeida         
   - 2019192123
   - adenis@student.dei.uc.pt
- Ana Martinez          
   - 2019210217
   - beatriz.martinez209@gmail.com
- Duarte Meneses        
   - 2019216949
   - duartemeneses@student.dei.uc.pt
- Edgar Duarte          
   - 2019216077
   - edgartip2003@gmail.com
- Eva Teixeira          
   - 2019215155
   - evateixeira@student.dei.uc.pt
- Filipe Viana          
   - 2019216279
   - filipeviana@student.dei.uc.pt
- Francisco Carreira    
   - 2019222462
   - fmbcarreira@gmail.com
- Gonçalo Pimenta       
   - 2019218212
   - 087f.goncalopimenta@gmail.com
- Gonçalo Lopes         
   - 2019218223
   - goncalolopes@student.dei.uc.pt
- Inês Marçal           
   - 2019215917
   - inesmarcal@student.dei.uc.pt
- Joana Antunes         
   - 2019219158
   - joanaantunes@student.dei.uc.pt
- João Lourenço         
   - 2019217948
   - joao.antonio.s.lourenco@gmail.com
- João Catré            
   - 2019218953
   - joaomcatre@hotmail.com
- João Freire           
   - 2019195830
   - joaofreire.0710@gmail.com
- Luís Braga            
   - 2019198731
   - lbraga@student.dei.uc.pt
- Luís Vieira           
   - 2019217980
   - lpedro.vieira18@gmail.com
- Miguel Barroso        
   - 2019219010
   - miguelbarroso@student.dei.uc.pt
- Patrícia Costa        
   - 2019213995
   - patriciacosta@student.dei.uc.pt
- Ricardo Vieira        
   - 2014200973
   - ricardojrsmvieira@gmail.com
- Rodrigo Ferreira      
   - 2019220060
   - rodrigoff988@gmail.com
- Sofia Alves           
   - 2019227240
   - sbalves@student.dei.uc.pt
- Sofia Santos          
   - 2018279108
   - soficgs@gmail.com
- Sofia Neves           
   - 2019220082
   - sofianeves@student.dei.uc.pt
- Tatiana Almeida       
   - 2019219581
   - tsalmeida@student.dei.uc.pt

### **6.2. Plataformas utilizadas**

- **Discord:** comunicação informal entre os membros da equipa;
- **GitLab:** repositório do projeto;
- **Slack:** comunicação formal com o docente e com o cliente.

<br>
