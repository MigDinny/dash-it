# **Validação Independente - PL9** <br>

---

## [Link](//10.17.0.180)

#### **[REQ-56] Landing page** <br>
- **Categoria**: Landing Page

- **UST**: Como utilizador, gostava de ver uma landing page para conhecer o estado do projeto.

- **Plano de testes:** 
    - Verificar a existência de alguma informação relativa ao estado do projeto.
<br> 

- **Resultado do plano de testes:**
    - ❌ Não existe nenhuma informação relativa ao estado do projeto.

<br>

---

#### **[REQ-44] Deve ser apresentada a árvore de ficheiros do repositório**
- **Categoria:** Ficheiros

- **UST:** Como utilizador, quero ver a árvore de ficheiros do mesmo, isto é, verificar um diagrama em árvore com as diretorias e respetivos ficheiros para ter uma visualização de todo o repositório e porque se torna mais simples encontrar ficheiros e as suas diretorias. Também quero poder diferenciar as diretorias dos ficheiros para identificação mais fácil.

- **Plano de testes:**
    - Verificar se existe um diagrama em árvore; 
    - Verificar se é possivel abrir todas as pastas do repositório e ver todos os ficheiros que as mesmas contêm;
    - Verificar se existe uma diferenciação de diretorias.  
<br> 

- **Resultado da testagem**
    - ❌ Não existe um diagrama em árvore;
    - ✅ É possível abrir todas as pastas do repositório;
    - ✅ Consigo distinguir diretorias por nome.

<br>

---

#### **[REQ-106] Deverá existir uma página com a lista de membros ativos do repositório** 

- **Categoria:** Membros

- **UST:** Como utilizador quero que haja uma página com a lista de membros ativos no repositório, de modo que seja possível ver os membros que tenham feito pelo menos um commit. 

- **Plano de testes:**
    - Selecionar a página com a lista de membros ativos;
    - Garantir que todos os membros presentes na lista são ativos.
<br>

- **Resultado da testagem:**
    - ✅ É possível selecionar a págna com a a lista de membros ativos;
    - ❌ Nem todos os membros são ativos (ex. GitNub Dummy), ou seja, nem todos os membros apresentados têm pelo menos um commit.

<br>

---

#### **[REQ-51] Deverá ser possivel observar a lista de commits de cada membro activo**

- **Categoria:** Membros

- **UST:** Como utilizador, pretendo observar a lista de commits de cada um dos membros ativos, podendo organizar cada utilizador pelo seu histórico de commits.

- **Plano de testes:** 
    - Garantir que existe pelo menos 1 commit;
    - Garantir que na lista estão todos os commits do membro;
    - Garantir que todos os commits pertencem ao respetivo membro. 
<br>

- **Resultado da testagem:**
    - ❌ Nem todos os membros tinham pelo menos um commit logo, não aparecem apenas os membros ativos;
    - ✅ Foi possivel visualizar todos os commits de um membro;
    - ✅ Todos os commits correspondem ao membro em questão.
<br>

---

#### **[REQ-46] Possibilidade de visualizar a timeline de issues ao longo do tempo: abertos, activos e total**
- **Categoria:** Issues

- **UST:** Como utilizador, deve ser possível visualizar a timeline de todos os issues pois é mais prático e torna a compreensão mais fácil.

- **Plano de testes:** 
    - Deverá ser possível visualizar uma timeline;
    - Deverá ser possível visualizar todos os issues com um estado associado: aberto, ativo, total;
    - Deverá ser possível ver os issues abertos;
	- Deverá ser possível ver os issues ativos;
	- Deverá ser possível ver os issues totais;
    - Deverá ser possível compreender facilmente.
<br>

- **Resultado da testagem:**
    - ❌ Não é possivel a visualização da timeline;
    - ❌ Não existe o estado ''ativo'' mas sim ''closed'';
    - ✅ É possível ver os issues abertos;
    - ❌ Não é possível ver os issues ativos, mas sim ''closed'';
    - ✅ É possível ver os issues totais;
    - ❌ Sendo que não existe mais nenhuma informação do issue, sem ser o titulo (que pode ser repetido), a compreensão não é fácil nesses casos. 
<br>

---

#### **[REQ-36] Funcionalidade "back"**
- **Categoria:** Exibição de dados solicitados

- **UST:** Como utilizador, quero poder regressar à vista anteriormente apresentada sem ter de reinserir as opções que levaram à sua visualização, de modo a poupar tempo e aumentar produtividade.

- **Plano de testes:** 
    - Existência de uma opção "back" em todas as vistas;
    - Através dessa opção deve ser possível regressar à vista anterior;
    - Ao regressar à vista anterior não deve ser necessário reinserir as opções que levaram à sua visualização.
<br>

- **Resultado da testagem:**
    - ❌ Não existe a opção "back" em vistas diferentes de "branches" e "files";
    - ❌ Esta opção, nestas duas vistas, não se encontra funcional, logo nao é possível regressar à vista anterior;
    - ❌ Não foi possível averiguar a necessidade de reinserção de informação uma vez que a opcção ''back'' não se encontra funcional.
<br><br><br>

###### **Realizado por:** Dash'it Team


