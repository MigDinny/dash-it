# **Verificação da estrutura dos requisitos**
**Resultado da verificação dos requisitos que cada elemento do grupo criou:**

- Não efetuaram o requisito:
    1. Ana Martinez;
    2. Sofia Santos.
<br><br>
- Efetuaram mal o requisito:
    1. Duarte Meneses;
    2. Francisco Carreira;
    3. Gonçalo Pimenta;
    4. Gonçalo Lopes;
    5. João Lourenço;
    6. João Freire;
    7. Luís Braga;
    8. Ricardo Vieira.
<br><br>
- *Principais erros na realização do requisito:*
    - Ao realizar o requirement não colocou o título ou a descrição correta que eram apresentados no ficheiro de apoio;
    - Formato errado do número do requirement;
    - Não foi cumprido o formato da descrição do issue. Ao nível deste aspeto, o erro cometido mais frequentemente foi não eliminar o que se encontrava abaixo das etapas, tal como era pedido.
<br><br>
- Posteriormente corrigiu o requisito:
    1. Duarte Meneses;
    2. Francisco Carreira;
    3. Gonçalo Pimenta;
    4. Gonçalo Lopes;
    5. Luís Braga.
<br><br>
- Foi notificado do erro mas não efetuou a correção:
    1. Ana Martinez;
    2. João Lourenço;
    3. João Freire;
    4. Ricardo Vieira;
    5. Sofia Santos.

