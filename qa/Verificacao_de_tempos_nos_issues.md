# **Verificação de tempos colocados nos issues**
**Resultado dos tempos colocados nos issues por cada elemento:**

>Esta verificação tem em conta os issue criados até ao momento. (Até ao issue #195)

### **Requisitos  verificados por Patricia Costa**
- 7 pessoas foram notificadas e, posteriormente, colocaram os tempos; <br><br>
- 7 pessoas foram notificadas e não colocaram os tempos; <br><br>
- Ao todo foram notificadas 14 pessoas.

<br>

***Principais razões/explicações para os tempos não terem sido colocados:***
- Tarefa esquecida;
- Várias pessoas colocaram os tempos apenas nos issues em que se encontravam como autor;
- Tempos não eram colocados em issues sobre tutoriais.

<br><br>
### **Requisitos  verificados por Inês Marçal**
- 12 pessoas foram notificadas e, posteriormente, colocaram os tempos; <br><br>
- 5 pessoas foram notificadas e não colocaram os tempos; <br><br>
- Ao todo foram notificadas 17 pessoas.

<br>

***Principais razões/explicações para os tempos não terem sido colocados:***
- Tarefa esquecida;
- Várias pessoas colocaram os tempos apenas nos issues em que se encontravam como autor;
- Tempos não eram colocados em issues sobre tutoriais;
- Alguns issues iam ser eliminados.
