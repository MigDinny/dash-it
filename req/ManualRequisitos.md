# **Manual de Requisitos** | Dash'it

## Engenharia de Software - PL10

### Licenciatura em Engenharia Informática e Licenciatura em Design e Multimédia

###

#### Faculdade de Ciências e Tecnologia da Universidade de Coimbra

#### Ano letivo 2021/2022<br><br>

<div align="justify">

**Versão: (1.0)\*\***Data: (12/12/2021)\*\*

**Índice**

**1. [Introdução](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#1-introdu%C3%A7%C3%A3o)**

- 1.1. [Objetivo](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#11-objetivo)

- 1.2. [Scope](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#12-scope)

- 1.3. [Visão geral](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#13-vis%C3%A3o-geral)

**2. [Descrição geral](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#2-descri%C3%A7%C3%A3o-geral)**

- 2.1. [Perspetiva de produto](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#21-perspetiva-de-produtodev)

  - 2.1.1 [Interfaces do sistema](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#211-interfaces-do-sistema)

  - 2.1.2 [Interfaces](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#212-interfaces)

  - 2.1.3 [Interfaces de Hardware](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#213-interfaces-de-hardware)

  - 2.1.4 [Interfaces de Software](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#214-interfaces-de-software)

  - 2.1.5 [Interfaces de comunicação](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#215-interfaces-de-comunica%C3%A7%C3%A3o)

  - 2.1.6 [Limites de memória](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#216-limites-de-mem%C3%B3ria)

- 2.2 [Funcionalides do produto](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#22-funcionalidades-do-produto)

- 2.3 [Características do utilizador](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#23-caracter%C3%ADsticas-do-utilizador)

- 2.4 [Limitações](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#24-limita%C3%A7%C3%B5es)

- 2.5 [Distribuição de requisitos](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#25-distribui%C3%A7%C3%A3o-de-requisitos)

**3. [Requisitos](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#3-requisitos)**

- 3.1 [Tabela dos requisitos](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#31-tabela-dos-requisitos)

- 3.2 [Requisitos](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#32-requisitos)

- 3.3 [Mapa de Navegação](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#33-mapa-de-navega%C3%A7%C3%A3o)

- 3.4 [Mockups](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#34-mockups)

- 3.5 [Requisitos lógicos da base de dados](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#35-requisitos-l%C3%B3gicos-da-base-de-dados)

- 3.6 [Atributos do sistema de software - requisitos não funcionais](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#36-atributos-do-sistema-de-software-requisitos-n%C3%A3o-funcionais)

**4. [Mudanças no processo de gestão](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#4-mudan%C3%A7as-no-processo-de-gest%C3%A3o)**

**5. [Aprovação do documento](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#5-aprova%C3%A7%C3%A3o-do-documento)**

## **1. Introdução**

Este documento tem como objetivo especificar os requisitos que permitem o desenvolvimento do projeto _Dash’it_, desenvolvido no âmbito da cadeira de Engenharia de Software dos cursos de Licenciatura em Engenharia Informática e Licenciatura em Design e Multimédia da Faculdade de Ciências e Tecnologia da Universidade de Coimbra.

Estes requisitos serão utilizados pelas equipas de desenvolvedores e de design, no decorrer do processo de desenvolvimento do projeto.

### **1.1. Objetivo**

Este é um documento de Especificação de Requisitos de Software (SRS) para o nosso projeto, uma _dashboard_ acedida via web.

O seu propósito é fornecer uma visão geral do nosso produto de software, dos seus parâmetros e objetivos. Este documento descreve o público-alvo do projeto, a sua interface de utilizador, requisitos funcionais e não funcionais. Ele define como o nosso cliente, a equipa e o público veem o produto e as suas funcionalidades.

Para além disso, auxilia a equipa de design e de desenvolvedores no processo de implementação do software.
Este documento é destinado a utilizadores do software e também a potenciais desenvolvedores.

### **1.2. Scope**

O projeto Dash’it visa a construção de uma _dashboard_ de projeto apresentando diversas vistas de um repositório de projeto alojado no gitlab1. Esta _dashboard_ é acedida via web.

Algumas das funcionalidades que possui são:

\- criar uma lista de ficheiros do repositório

\- criar uma _timeline_ da evolução da lista de ficheiros do repositório

\- criar um perfil para cada utilizador com toda a sua atividade

\- todos os ficheiros têm um histórico de alterações feitas

Estas funcionalidades permitem um monitoramento da atividade de cada utilizador e a visualização de todas as alterações em ficheiros, bem como o seu autor.

### **1.3. Visão geral**

As restantes secções deste documento fornecem uma descrição geral do projeto, incluindo as características dos utilizadores deste projeto, os requisitos funcionais e não funcionais e requisitos de dados do produto.

A descrição geral do projeto é discutida na secção 2 deste documento.

A secção 3 fornece os requisitos de dados e a organização dos requisitos específicos do produto. Para além disso, também discute os requisitos de interface externa e fornece uma descrição detalhada dos requisitos funcionais.

Na secção 4 é discutido o processo de gestão de mudanças, na secção 5 a aprovação de documentos e na secção 6 as informações de apoio.

# 2. Descrição Geral

Existem fatores que permitem determinar a qualidade de um produto de software e garantir uma melhor gestão de recursos.

Alguns desses fatores que podem influenciar a qualidade final desse produto e dos seus requisitos são:

\- **Fatores tecnológicos**, como o uso de uma base de dados específica, uma linguagem de programação e um método de desenvolvimento adequado, podem afetar diretamente a qualidade de um produto de software. Entende-se que a inclusão de um processo por meio de um método de desenvolvimento adequado pode facilitar a manutenção do produto de software e reduzir a probabilidade de erros. O uso de uma ferramenta de desenvolvimento adequada e a apresentação de recursos para a construção de interfaces gráficas do utilizador, pode resultar em produtos com interfaces ricas em recursos, mais fáceis de usar, e muito mais amigáveis.

\- **Fatores individuais**, como a experiência do utilizador, competência e envolvimento podem afetar a qualidade de um produto de software. Os utilizadores mais envolvidos com o projeto podem ser mais colaborativos e contribuir mais para os requisitos do projeto, assim como utilizadores competentes e bem treinados podem ter um melhor entendimento das entradas e saídas do sistema e, consequentemente, podem colaborar no levantamento de processos. Os utilizadores mais resistentes tendem a posicionar-se negativamente em relação à mudança e, portanto, tornam-se um problema. Esses utilizadores tendem a solicitar cada vez mais mudanças para o sistema, aumentando as chances da ferramenta se tornar não confiável.

\- **Fatores organizacionais**, como suporte da alta administração em relação aos projetos apropriados e disponibilidade de orçamento, podem levar ao desenvolvimento de melhores produtos. Quanto mais alta a posição de gestão, maior a compreensão das necessidades organizacionais e, portanto, maior a probabilidade de informações mais relevantes e pertinentes serem adicionadas aos produtos.

\- Priorização dos requisitos

\- Excelência tecnológica, maturidade do processo e produtividade da equipa

## 2.1 Perspetiva de Produto(DEV)

Este produto tem como dependência a existência de um repositório do GitLab. Após aceder ao mesmo, o produto irá mostrar as devidas vistas correspondentes a cada parte do repositório, conforme os requisitos definidos e implementados.

_As subsecções seguintes descrevem como é que o software funciona com as respetivas limitações associadas_.

### 2.1.1 Interfaces do Sistema

O sistema usa a api python-gitlab para ir buscar os dados ao GitLab e usa a rede do DEI.

###

### 2.1.2 Interfaces

1. O produto interage com o utilizador através de uma _GUI_ na _web_. Não requer qualquer tipo de conhecimento específico para interagir com o produto.
2. É feita uma cópia dos dados da api para a base de dados para rápido acesso e fluidez no site, após o _fetch_ dos dados.

### 2.1.3 Interfaces de Hardware

O produto não requer nada, para além de acesso à internet, ao utilizador, isto é, não há propriamente limitações de memória nem de armazenamento (ainda que, quanto mais tiver a máquina onde o produto corre, mais fluído é usá-lo). Todos os dispositivos com acesso à internet e a um browser comum suportam o produto.

###

### 2.1.4 Interfaces de Software

O sistema utiliza gitlab-api e a biblioteca python-gitlab para comunicar com ela.

O sistema utiliza SQL (sqlite3), que está integrado no django. 

### 2.1.5 Interfaces de Comunicação

Não são usados protocolos de comunicação para além dos _standard_ necessários para efetuar a comunicação com os serviços web.

### 2.1.6 Limites de memória

O sistema tem disponíveis 4Gb de RAM e 80Gb de disco.

## 2.2 Funcionalidades do Produto

O produto serve para vizualizar _dashboards_ do GitLab. Conseguimos ver os _commits_ feitos pelos utilizadores, bem como quais o utilizadores ativos (ou seja, que têm pelo menos um _commit_). Podemos, também, ver a ficha de cada participante do repositório, bem como as suas horas gastas no mesmo, número de _commits_,... É apresentada uma lista com os ficheiros existentes no repositório e uma lista das _issues_ existentes, bem como os seus detalhes.

## 2.3 Características do utilizador

O produto em questão pedido pelo cliente é direcionado a um público geral. Isto significa que não será necessário ter conhecimento da idade do utilizador final de modo ao mesmo poder utilizar o produto.

A interface do UI teve em conta o nível de conforto e experiência do cliente final. O produto é direcionado a utilizadores com experiência em computadores, dashboards e, a pessoas com experiência e familiarização em GitLab uma vez que a dashboard analisa o mesmo.

## 2.4 Limitações

_Limitações de hardware_ - vCPUs=4; RAM=4Gb; Disk=80Gb

_Interface para outros aplicativos_ - Em dispositivos móveis (nomeadamente, _smartphones_) o produto pode não funcionar como desejado, devido à escala reduzida (do ecrã) dos mesmos. Assim, recomenda-se que use um computador portátil ou um computador fixo.

# 2.5. Distribuição de requisitos

O papel da priorização de requisitos é imperativo para um produto eficiente e orientado para o desenvolvimento de resultados. A priorização de requisitos marca os requisitos mais importantes a serem priorizados na implementação.

Normalmente, as expectativas das partes interessadas são altas, mas a escassez de tempo e recursos limitados dificultam a implementação de todos requisitos que foram idealizados para o sistema.

Muitas vezes, após uma avaliação do plano do nosso projeto e as horas disponíveis, apercebermo-nos de que nem tudo poderá ser concluído dentro do tempo estimado.
Então, em conjunto com o cliente, deve-se proceder a uma priorização dos requisitos de maneira a categorizar os requisitos de acordo com o seu grau de prioridade (Must, Should, Could). Assim é possível identificar os requisitos a ser implementados com maior prioridade (Must) e aqueles que podem ser atrasados até versões futuras do sistema (Could).

# 3. Requisitos

---

### **Convenções, Termos e Abreviações**

---

**RF - Requisito Funcional:** declarações de serviços que o sistema deve oferecer, como o sistema deve reagir a determinados inputs ou comportar-se perante determinadas situações.<br>
**RNF - Requisito Não Funcional:** restrições nos serviços ou funções oferecidas pelo sistema como prazos, standards e processo de desenvolvimento.<br>
**Must:** É o requisito sem o qual o sistema não entra em funcionamento. Requisitos Must são requisitos imprescindíveis.<br>
**Should -** É o requisito sem o qual o sistema entra em funcionamento, mas de forma não satisfatória. Requisitos importantes devem ser implementados, mas, se não forem, o sistema pode ser usado mesmo assim.<br>
**Could:** É o requisito que não compromete as funcionalidades básicas do sistema, isto é, o sistema pode funcionar de forma satisfatória sem ele.<br>
**Utilizador:** Quem utiliza a plataforma.<br>
**Objetivo:** Descrição detalhada da funcionalidade.<br>
**Condições:** Condições a serem cumpridas para o requisito ser executável.<br>
**Passos:** Descrição dos passos necessários ao concluir o requisito em questão.<br>
**Prioridade:** Identificação da prioridade do requisito, isto é, Must (M), Should (S), Could(C).<br>
**Caso de Sucesso:** Descrição do processo concretizada pelo utilizador, no sentido de chegar ao estado final.<br>
**Caso de Exceção:** Descrição do processo não concretizado pelo utilizador,impedido o mesmo de chegar ao estado final.<br><br>

---

### **Identificação dos Requisitos**

---

Para facilitar a leitura deste documento, optámos por criar uma referência que identificasse os tipos de requisitos registados. Essa referência é feita através do
do número e nome do requisito, de acordo com: [REQ- «número do requisito»] «nome»
Por exemplo, o requisito “Perfil de cada membro” é identificado pelo número 11. Assim, podemos identificar este requisito por [REQ-11] Perfil de cada membro.
<br><br>

---

### **Prioridade dos Requisitos**

---

De maneira a estabelecer prioridade entre os diferentes requisitos, decidimos atribuir as letras M, S e C de acordo com as prioridades Must, Should, Could.
A prioridade dos requisitos foi feita pelo Cliente de maneira a podermos dar prioridade ao que este desejar. Em frente a cada requisito haverá três checkboxes que indicam a prioridade do requisito semelhante a: <br>
Prioridade: M☐ S☐ C☐
<br><br>

# 3.1 Tabela dos requisitos

| Tipo              |            Vista             |    ID    | Requisito                                                                                                                                                                          | Prioridade | Implementado | Testado |
| :---------------- | :--------------------------: | :------: | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :--------: | :----------: | :-----: |
| **Funcional**     |          **Todas**           | [F10](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-10-escolha-do-reposit%C3%B3rio-a-analisar)  | Escolher o repositório GitLab a analisar                                                                                                                                           |    MUST    |      ✅      |   ✅    |
| ''                |              ''              | [F40](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-40-regressar-%C3%A0-vista-anterior-back)  | Regressar à vista anteriormente apresentada sem ter de reinserir as opções que levaram à sua visualização                                                                          |    MUST    |      ❌      |   ❌    |
| ''                |              ''              | [F04](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-4-recolha-de-dados-autom%C3%A1tica)  | Cada novo acesso à _dashboard_ deve envolver a recolha dos dados de atividade mais recente no repositório                                                                          |    MUST    |      ✅      |   ✅    |
| ''                |              ''              | [F20](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-20-modo-de-visualiza%C3%A7%C3%A3o-dos-ficheiros-nome-e-extens%C3%A3o)  | Sempre que houver um ficheiro, o mesmo deve conter a categoria, o nome e a extensão do próprio                                                                                     |    MUST    |      ✅      |   ✅    |
| ''                |              ''              | [F47](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-47-navega%C3%A7%C3%A3o-entre-vistas-principais)  | Deverá existir uma forma de navegar entre as diferentes vistas principais (Lista de ficheiros, Departamento, commits, issues)                                                      |    MUST    |      ✅      |   ✅    |
| ''                |              ''              | [F35](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-35-cada-p%C3%A1gina-deve-ter-uma-timeline-do-reposit%C3%B3rio)  | Cada página de visualização deverá ter uma ‘timeline’ que permita ver o estado do repositório em cada instante, lista de ficheiros e commits                                       |   COULD    |      ❌      |   ❌    |
| ''                |       **Departamento**       | [F15](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-15-lista-de-membros-ativos-do-reposit%C3%B3rio)  | Deverá existir uma página com a lista de membros ativos do repositório. Um membro ativo é aquele que fez pelo menos um commit                                                      |    MUST    |      ✅      |   ✅    |
| ''                |              ''              | [F19](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-19-esfor%C3%A7o-total-aplicado-no-projeto-por-pessoa)  | Deverá ser possível visualizar o esforço total aplicado no projeto, por pessoa                                                                                                     |    MUST    |      ✅      |   ✅    |
| ''                |          **Perfil**          | [F11](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-11-perfil-de-cada-membro)  | Deverá ser possível observar a página de perfil de cada membro ativo                                                                                                               |    MUST    |      ✅      |   ✅    |
| ''                |              ''              | [F50](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-50-perfil-lista-de-commits)  | Deverá ser possível ver a lista de commits de um membro ativo                                                                                                                      |    MUST    |      ✅      |   ✅    |
| ''                |              ''              | [F21](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-21-vista-por-autor-ficheiros-alterados-por-esse-autor)  | Para cada um dos perfis vai existir uma lista de ficheiros que foram alterados pela pessoa à qual o perfil corresponde                                                             |    MUST    |      ✅      |   ✅    |
| ''                |              ''              | [F48](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-48-perfil-lista-de-mrs)  | Deverá ser possível visualizar a lista de MR's de cada membro ativo                                                                                                                |    MUST    |      ❌      |   ❌    |
| ''                |              ''              | [F49](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-49-perfil-lista-de-issues)  | Deverá ser possível ver a lista de issues - todas - de um dado membro ativo                                                                                                        |    MUST    |      ❌      |   ❌    |
| ''                |              ''              | [F44](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-44-visualiza%C3%A7%C3%A3o-do-papel-de-cada-membro-a-partir-das-contribui%C3%A7%C3%B5es)  | Deverá ser possível visualizar o papel de cada membro ativo a partir da intensidade das suas contribuições por categoria                                                           |   SHOULD   |      ❌      |   ❌    |
| ''                |              ''              | [F28](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-28-contribui%C3%A7%C3%B5es-de-cada-membro-por-categoria)  | Deverá ser possível ver a lista de contribuições por categoria de cada membro ativo (contibuições = MR's, issues, commits e ficheiros alterados)                                   |   SHOULD   |      ❌      |   ❌    |
| ''                |              ''              | [F07](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-7-vista-por-autor-hist%C3%B3ria-desse-ficheiro)  | Clicando no ficheiro navegamos para a vista da história desse ficheiro                                                                                                             |   COULD    |      ❌      |   ❌    |
| ''                |              ''              | [F34](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-34-timeline-de-commits-de-cada-membro)  | Deverá ser possível visualizar a timeline de commits de cada membro ativo                                                                                                          |   COULD    |      ❌      |   ❌    |
| ''                |    **Lista de ficheiros**    | [F09](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-9-cria%C3%A7%C3%A3o-lista-de-ficheiros)  | Deverá ser apresentada a lista de ficheiros do repositório                                                                                                                         |    MUST    |      ✅      |   ✅    |
| ''                |              ''              | [F06](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-6-lista-ficheiros-agrupar-ficheiros-por-autor)  | Deverá aparecer a lista de todos os ficheiros agrupados por autor                                                                                                                  |   SHOULD   |      ❌      |   ❌    |
| ''                |              ''              | [F03](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-3-lista-ficheiros-agrupar-ficheiros-por-categoria)  | Deverá aparecer a lista de todos os ficheiros agrupados por categoria                                                                                                              |   SHOULD   |      ❌      |   ❌    |
| ''                |              ''              | [F16](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-16-visualiza%C3%A7%C3%A3o-de-ficheiros-por-categoria)  | Deverá aparecer a lista de todos os ficheiros da categoria selecionada                                                                                                             |   SHOULD   |      ❌      |   ❌    |
| ''                |              ''              | [F36](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-36-lista-ficheiros-agrupar-por-intensidade-de-altera%C3%A7%C3%B5es)  | Deverá ser apresentada a lista (tabela) de todos os ficheiros do repositório agrupados por intensidade de alterações                                                               |   COULD    |      ❌      |   ❌    |
| ''                |              ''              | [F46](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-46-eventos-organizados-por-commit-instants)  | Os eventos na linha temporal devem estar organizados por commit instants                                                                                                           |   COULD    |      ❌      |   ❌    |
| ''                |              ''              | [F45](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-45-visualiza%C3%A7%C3%A3o-temporal-uniforme)  | Deverá apresentar a visualização temporal (linha de tempo) que deve ser uniforme                                                                                                   |   COULD    |      ❌      |   ❌    |
| ''                |              ''              | [F27](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-27-timeline-da-evolu%C3%A7%C3%A3o-da-lista-de-ficheiros)  | Deverá ser possível visualizar a evolução da lista de ficheiros ao longo do tempo                                                                                                  |   COULD    |      ❌      |   ❌    |
| ''                |   **Vista de um ficheiro**   | [F12](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-12-visualiza%C3%A7%C3%A3o-do-tempo-desde-a-%C3%BAltima-atualiza%C3%A7%C3%A3o)  | O momento da última atualização dos dados deverá estar sempre presente no ecrã                                                                                                     |    MUST    |      ❌      |   ❌    |
| ''                |              ''              | [F14](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-14-lista-de-ficheiros-visualizar-conte%C3%BAdo-do-ficheiro)  | Deverá ser possível visualizar o conteúdo de um ficheiro num commit                                                                                                                |    MUST    |      ✅      |   ✅    |
| ''                |              ''              | [F08](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-8-hist%C3%B3ria-de-commits-do-ficheiro)  | Deverá apresentar uma lista de commits de um ficheiro (botão histórico) ordenada por data/hora. Deverá conter também o nome da pessoa que efetuou cada commit nesse mesmo ficheiro |   SHOULD   |      ❌      |   ❌    |
| ''                |              ''              | [F02](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-2-issues-associados-a-cada-ficheiro)  | Deverá ser possível saber, por ficheiro, quais os issues que lhe estão associados                                                                                                  |   SHOULD   |      ❌      |   ❌    |
| ''                |              ''              | [F05](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-5-visualiza%C3%A7%C3%A3o-de-ficheiros-por-churn)  | Deverá existir uma bola, associada a cada ficheiro, com a cor correspondente ao nível de alterações nesse ficheiro                                                                 |   COULD    |      ❌      |   ❌    |
| ''                |              ''              | [F01](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-1-informa%C3%A7%C3%A3o-est%C3%A1tica-ficheiro-tipo-dimens%C3%A3o-e-complexidade)  | Adicionar informação estática sobre o ficheiro: Tipo, dimensão e complexidade                                                                                                      |   COULD    |     ✅❌     |  ✅❌   |
| ''                |              ''              | [F13](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-13-informa%C3%A7%C3%A3o-est%C3%A1tica-ficheiro-m%C3%A9tricas-de-estabilidade)  | Apresentar as métricas de estabilidade                                                                                                                                             |   COULD    |      ❌      |   ❌    |
| ''                |              ''              | [F18](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-18-hist%C3%B3ria-de-defeitos-pendentes-de-um-ficheiro)  | Deverá ser possível visualizar a existência de defeitos pendentes nesse mesmo ficheiro através de um ticket/issue                                                                  |   COULD    |      ❌      |   ❌    |
| ''                |         **Commits**          | [F30](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-30-visualizar-lista-de-commits)  | Deverá ser possível visualizar uma lista de commits                                                                                                                                |    MUST    |      ✅      |   ✅    |
| ''                |              ''              | [F31](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-31-lista-de-commits-de-cada-membro)  | Deverá aparecer a lista de todos os commits agrupados por autor(ordem alfabética)                                                                                                  |    MUST    |      ✅      |   ✅    |
| ''                |              ''              | [F29](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-29-por-commit-ver-ficheiros-alterados-e-autor)  | Deverá ser possível ver, por cada commit, quais os ficheiros que foram modificados nesse commit, bem como o respectivo autor                                                       |    MUST    |      ❌      |   ❌    |
| ''                |              ''              | [F43](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-43-contribui%C3%A7%C3%B5es-de-todos-os-membros-por-categoria)  | Deverá ser possível visualizar os commits agrupados por categoria (ordem alfabética)                                                                                               |   SHOULD   |      ❌      |   ❌    |
| ''                |          **Issues**          | [F51](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-51-lista-de-issues-abertos-fechados-e-total)  | Deverá ser possível ver a lista de issues abertos, fechados e total                                                                                                                |    MUST    |      ✅      |   ✅    |
| ''                |              ''              | [F26](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-26-timeline-de-issues-abertos-ativos-e-total)  | Deverá ser possível visualizar a timeline de issues ao longo do tempo: abertos, fechados e total                                                                                   |   COULD    |      ❌      |   ❌    |
| ''                |              ''              | [F32](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-32-clicando-em-cada-instante-ver-lista-de-issues)  | Clicando em cada instante deveremos poder ver a lista de issues abertos e ativos desse momento                                                                                     |   COULD    |      ❌      |   ❌    |
| ''                |    **Vista de uma issue**    | [F52](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-52-issue-t%C3%ADtulo)  | Deverá ser possível ver o título de uma issue                                                                                                                                      |    MUST    |      ✅      |   ✅    |
| ''                |              ''              | [F53](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-53-issue-descri%C3%A7%C3%A3o)  | Deverá ser possível ver a descrição de uma issue                                                                                                                                   |    MUST    |      ✅      |   ✅    |
| ''                |              ''              | [F54](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-54-issue-nome-da-pessoa-que-a-criou)  | Deverá ser possível ver o nome da pessoa que criou a issue                                                                                                                         |    MUST    |      ✅      |   ✅    |
| ''                |              ''              | [F55](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-55-issue-estado-da-issue)  | Deverá ser possível ver o estado da issue: aberto ou fechado                                                                                                                       |   COULD    |      ✅      |   ✅    |
| ''                | **Histórico de um ficheiro** | [F22](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-22-hist%C3%B3ria-de-altera%C3%A7%C3%B5es-no-ficheiro)  | Deverá ser possível visualizar a dimensão das alterações (nr de linhas alteradas/acrescentadas/removidas) em cada commit                                                           |    MUST    |      ❌      |   ❌    |
| ''                |          **Login**           | [F39](https://gitlab.com/MigDinny/dash-it/-/blob/dev/req/ManualRequisitos.md#req-39-login)  | Deverá ser possível efetuar um login na dashboard sempre que o projeto a analisar seja privado                                                                                     |   COULD    |      ❌      |   ❌    |
| **Não-Funcional** |              ''              | NF24 | A tecnologia a adoptar deverá ser baseada em Python                                                                                                                                |    MUST    |      ✅      |   ✅    |
| ''                |              ''              | NF38 | A solução deve ser web-based                                                                                                                                                       |    MUST    |      ✅      |   ✅    |
| ''                |              ''              | NF25 | Apenas utilizadores autenticados poderão aceder a informação interna do projecto                                                                                                   |   COULD    |      ❌      |   ❌    |
| ''                |              ''              | NF37 | O sistema deverá ter uma boa usabilidade                                                                                                                                           |   COULD    |      ❌      |   ❌    |
| ''                |              ''              | NF41 | A dashboard deverá suportar multi-idioma                                                                                                                                           |   COULD    |      ❌      |   ❌    |

<br><br>

# 3.2 Requisitos

## **Todas as vistas**

---

#### **[REQ-10] Escolha do repositório a analisar**

---

**Objetivo:** Escolher o repositório GitLab a analisar<br>
**User Story:** O utilizador quer poder escolher o repositório GitLab a analisar para efetuar a avaliação dos seus membros e examinar todos os progressos<br>
**Design:** Existirá uma searchbar em cima em todas as vistas para permitir procurar o repositório a analisar e posteriormente selecioná-lo. Levará até à 'Lista de ficheiros' desse novo repositório<br>
**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- Escolher um repositório privado e não deverá ser possível analisá-lo;
- Escolher um repositório público de elevada dimensão e deverá ser possível analisá-lo;
- Tentar escolher um repositório que não existe e não o deverá encontrar;
- Escolher um repositório vazio.
  <br>

---

#### **[REQ-40] Regressar à vista anterior – ‘Back’**

---

**Objetivo:** Regressar à vista anteriormente apresentada sem ter de reinserir as opções que levaram à sua visualização<br>
**User Story:** O utilizador quer a opção de recuar na página sem ter que repetir os mesmos passos para atingir uma dada página porque facilita e evita uma utilização repetitiva do programa<br>
**Design:** Encontrar-se-á na barra lateral esquerda<br>
**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- Verificar se existe a opção de 'back' em todas as vistas;
- Verificar se realmente me leva à vista de onde vim.
  <br>

---

#### **[REQ-4] Recolha de dados automática**

---

**Objetivo:** Cada novo acesso à _dashboard_ deve envolver a recolha dos dados de atividade mais recente no repositório.<br>
**User Story:** O utilizador quer que a cada novo acesso à dashboard exista recolha dos dados da atividade mais recente no repositório porque quer visualizar sempre a informação mais atualizada do repositório<br>
**Design:** Não tem<br>
**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- Verificar se a recolha de dados é feita de forma automática fazendo um commit, MR, criar um issue e verificar que os mesmos se encontram na dashboard sem qualquer tipo de atualização.
  <br>

---

#### **[REQ-47] Navegação entre vistas principais**

---

**Objetivo:** Deverá existir uma forma de navegar entre as diferentes vistas principais (Lista de ficheiros, Departamento, commits, issues)<br>
**User Story:** O utilizador quer ter uma forma de navegar entre as diferentes vistas porque facilita o acesso à informação.<br>
**Design:** Existirá uma barra lateral esquerda que permite mudar entre as diferentes vistas principais: Lista de ficheiros, Departamento, Commits e Issues<br>
**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- Verificar se consigo aceder a vistas secundárias através das diferentes vistas principais ou até outras secundárias;
- Verificar se consigo aceder a todas as vistas principais.
  <br>

---

#### **[REQ-35] Cada página deve ter uma timeline do repositório**

---

**Objetivo:** Cada página de visualização deverá ter uma ‘timeline’ que permita ver o estado do repositório em cada instante, lista de ficheiros e commits.<br>
**User Story:** O utilizador quer ter acesso a uma timeline detalhada com todos os commits feitos ao longo da implementação do repositório porque lhe permite ver a evolução do projeto ao longo do tempo<br>
**Design:** Existirá uma 'timeline' no topo de cada vista de modo a permitir ver em cada instante a lista de ficheiros e commits<br>
**Prioridade:** M☐ S☐ C☒<br>
**Plano de Testes:**

- Verificar se consigo visualizar a timeline do repositório em todas as vistas;

<br>

## **Vista Login**

---

#### **[REQ-39] Login**

---

**Objetivo:** Deverá ser possível efetuar um login na dashboard sempre que o projeto a analisar seja privado<br>
**User Story:** O utilizador quer que seja necessário efetuar login de modo a poder aceder ao repositório privado a analisar<br>
**Design:** O login estará localizado no canto superior direito sob a forma de um círculo. Caso a pessoa ainda não esteja logged in, deve abrir a vista "Login". Caso contrário, se a pessoa já estiver autenticada, deverá apresentar a imagem da pessoa que está no GitLab<br>
**Prioridade:** M☐ S☐ C☒<br>
**Plano de Testes:**

- Verificar se consigo fazer login na dashboard;
- Tentar entrar com os campos (user e password) em branco;
- Tentar colocar uma password inválida;
- Tentar colocar um username não registado.
  <br>

## **Vista Departamento**

---

#### **[REQ-15] Lista de membros ativos do repositório**

---

**Objetivo:** Deverá existir uma página com a lista de membros ativos do repositório. Um membro ativo é aquele que fez pelo menos um commit.<br>
**User Story:** O utilizador quer poder visualizar os membros ativos do repositório para poder saber quem é que faz parte do repositório. O utilizador quer não ter "poluição" na dashboard com membros inativos porque tem maior legibilidade<br>
**Design:** Deverão estar organizados por ordem alfabética. Aparecerá sob a forma de lista.<br>
**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- Será possível ver listado por ordem alfabética todos os membros ativos da equipa;
- Os membros ativos deverão ter pelo menos um commit realizado.
  <br>

---

#### **[REQ-19] Esforço total aplicado no projeto por pessoa**

---

**Objetivo:** Deverá ser possível visualizar o esforço total aplicado no projeto, por pessoa<br>
**User Story:** O utilizador quer que seja possível visualizar o esforço total por pessoa porque permite saber o esforço total de cada um para posterior análise e estatística do trabalho investido<br>
**Design:** Aparecerá sob a forma de um gráfico de barras o esforço total de cada membro<br>
**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- Deverá ser possível ver um gráfico correspondente ao esforço aplicado no projeto por pessoa;
- O gráfico deverá ter várias cores, uma por pessoa;
- Verificar a veracidade do gráfico.

<br>

## **Vista Perfil de cada membro**

---

#### **[REQ-11] Perfil de cada membro**

---

**Objetivo:** Deverá ser possível observar a página de perfil de cada membro ativo<br>
**User Story:** O utilizador quer visualizar o perfil de cada membro ativo para poder saber as suas informações e as tarefas que tem cumprido ou que está assigned para fazer<br>
**Design:** Depois de carregar no nome do membro na lista de membros ativos na vista 'Departamento' aparece a vista do perfil desse mesmo membro<br>
**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- Deverá ser possível ver a página do perfil de cada membro ativo;
- Os membros deverão estar por ordem alfabética;
- Verificar se todos os membros são ativos.
  <br>

---

#### **[REQ-21] Vista por autor: Ficheiros alterados por esse autor**

---

**Objetivo:** Para cada um dos perfis vai existir uma lista de ficheiros que foram alterados pela pessoa à qual o perfil corresponde.<br>
**User Story:** O utilizador quer que seja possível visualizar os ficheiros de cada autor porque, assim, pode monitorar o trabalho de cada membro <br>
**Design:** Terá que se carregar num botão 'Ficheiros' de modo a aparecer sob a forma de lista os ficheiros alterados<br>
**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- No perfil de um membro, verificar se consigo ver a lista de ficheiros alterados por esse membro;
- Verificar se a lista de encontra organizada (primeiro o mais recente).
  <br>

---

#### **[REQ-7] Vista por autor: História desse ficheiro**

---

**Objetivo:** Clicando no ficheiro (na vista por autor) navegamos para a vista da história desse ficheiro<br>
**User Story:** O utilizador quer visualizar o histórico do ficheiro porque, assim, pode ver todas as alterações efetuadas naquele ficheiro<br>
**Design:** Haverá um link no nome do ficheiro que redirecionará para a vista da história desse ficheiro<br>
**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- Deverá haver uma tabela com a lista de ficheiros alterados pelo membro;
- Deverá ser possível carregar em cima de cada ficheiro e ser direcionado para a vista do histórico desse ficheiro;
- Os ficheiros deverão estar por ordem;
- Não devem haver ficheiros repetidos;

<br>

---

#### **[REQ-48] Perfil: Lista de MR's**

---

**Objetivo:** Deverá ser possível visualizar a lista de MR's de cada membro ativo.<br>
**User Story:** O utilizador quer visualizar a lista de MR's de cada membro ativo porque lhe permite saber quantos merge requests fez e quais.<br>
**Design:** Terá que se carregar num botão 'Merge Requests' de modo a aparecer sob a forma de lista os MR's (merge requests) desse membro<br>
**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- No perfil de um membro, verificar se consigo ver a lista de MR's desse mesmo membro;
- Verificar se a lista de encontra organizada (primeiro o mais recente).
  <br>

---

#### **[REQ-49] Perfil: Lista de Issues**

---

**Objetivo:** Deverá ser possível ver a lista de issues - todas - de um dado membro ativo<br>
**User Story:** O utilizador quer visualizar a lista de issues de um dado membro porque lhe permite saber o que o utilizador andou a fazer<br>
**Design:** Terá que se carregar num botão 'Issues' de modo a aparecer sob a forma de lista as issues desse membro<br>
**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- No perfil de um membro, verificar se consigo ver a lista de issues desse mesmo membro;
- Verificar se a lista de encontra organizada (primeiro o mais recente).
  <br>

---

#### **[REQ-50] Perfil: Lista de Commits**

---

**Objetivo:** Deverá ser possível ver a lista de commits de um membro ativo<br>
**User Story:** O utilizador quer visualizar a lista de commits de um dado membro porque lhe permite saber o trabalho investido por cada membro<br>
**Design:** Terá que se carregar num botão 'Commits' de modo a aparecer sob a forma de lista as commits desse membro<br>
**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- No perfil de um membro, verificar se consigo ver a lista de commits desse mesmo membro;
- Verificar se a lista de encontra organizada (primeiro o mais recente).
  <br>

---

#### **[REQ-28] Contribuições de cada membro por categoria**

---

**Objetivo:** Deverá ser possível ver a lista de contribuições (Mr's, isues, commits e ficheiros alterados) por categoria de cada membro ativo<br>
**User Story:** O utilizador quer poder ver a lista de contribuições por categoria porque lhe permite ter uma noção clara e visual do trabalho efetuado em cada área do projeto por cada membro do projeto<br>
**Design:** Há um gráfico circular que estará separado por categorias, cada categoria tem uma cor diferente<br>
**Prioridade:** M☐ S☒ C☐<br>
**Plano de Testes:**

- Deverá existir um gráfico a cores com as contribuições do membro por categoria;
- As linhas do gráfico deverão ser de cores diferentes e representar uma categoria diferente;
- Verificar o número de contribuições de cada categoria está correto.
  <br>

---

#### **[REQ-34] Timeline de commits de cada membro**

---

**Objetivo:** Deverá ser possível visualizar a timeline de commits de cada membro ativo<br>
**User Story:** O utilizador quer poder visualizar a timeline de commits porque assim conseguirá ter uma ideia de como o trabalho foi desenvolvido ao longo do tempo<br>
**Design:** Existirá uma grelha anual em que cada quadrado representará um dado dia do ano. Caso não haja commits num dado dia aparecerá a branco, caso haja então aparecerá a azul esse mesmo quadrado.<br>
**Prioridade:** M☐ S☐ C☒<br>
**Plano de Testes:**

- Será possível ver uma grelha com a timeline de commits do membro em causa;
- Na grelha da timeline deverá estar explícito a sub-equipa para a qual foi realizada a contribuição.
  <br>

---

#### **[REQ-44] Visualização do papel de cada membro a partir das contribuições**

---

**Objetivo:** Deverá ser possível visualizar o papel de cada membro ativo a partir da intensidade das suas contribuições por categoria<br>
**User Story:** O utilizador quer ver o papel de cada membro a partir das contribuições porque, assim, fica a saber a quantidade de trabalho efetuada por esse membro<br>
**Design:** Por baixo do nome no centro da vista deverá aparecer o papel do membro, ex: Quality Asssurance. Caso haja empate no número de contribuições, deverá aparecer os dois papéis desse membro separados por '/', ex: Quality Assurance/Requirement<br>
**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- Deverá ser possível ver a equipa que o membro integra;
- Confirmar se a sub-equipa mostrada se encontra correta a partir do número de contribuições.
  <br>

## **Lista de ficheiros**

---

#### **[REQ-9] Criação lista de ficheiros**

---

**Objetivo:** Deverá ser apresentada a lista de ficheiros do repositório<br>
**User Story:** O utilizador quer visualizar a lista de ficheiros do repositório porque lhe permite saber quais os ficheiros existentes no repositório<br>
**Design:** Na barra lateral, quando se carregar na vista da 'Lista de ficheiros', deverá aparecer a lista de todos os ficheiros organizados por ordem alfabética<br>
**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- Verificar se a estrutura apresentada na dashboard segue a do repositório em causa;
- Verificar se a visualização dos ficheiros da lista é feita por nome e extensão;
- Verificar se os ficheiros ficam ordenados por ordem alfabética;
  <br>

---

#### **[REQ-6] Lista ficheiros: Agrupar ficheiros por autor**

---

**Objetivo:** Deverá aparecer a lista de todos os ficheiros agrupados por autor<br>
**User Story:** O utilizador quer ver todos os ficheiros do repositório agrupados por autor porque permite uma análise desses dados mais fácil<br>
**Design:** Deverá aparecer um dropdown bar à direita na vista 'Lista de ficheiros' que me permita escolher um membro ativo. Quando carregando no mesmo, devo poder visualizar os ficheiros alterados por esse membro<br>
**Prioridade:** M☐ S☒ C☐<br>
**Plano de Testes:**

- Verificar se consigo visualizar uma lista de ficheiros organizada por autor.
  <br>

---

#### **[REQ-3] Lista ficheiros: Agrupar ficheiros por categoria**

---

**Objetivo:** Deverá aparecer a lista de todos os ficheiros agrupados por categoria<br>
**User Story:** O utilizador quer visualizar a lista de todos os ficheiros do repositório agrupados por categoria porque necessita de visualizar o trabalho organizado por tópicos<br>
**Design:** Deverá aparecer um dropdown bar à direita na vista 'Lista de ficheiros' que me permita escolher 'Categoria'. Quando carregando na mesma, devo poder visualizar os ficheiros todos separados por categorias<br>
**Prioridade:** M☐ S☒ C☐<br>
**Plano de Testes:**

- Verificar se todos os ficheiros se encontram listados;
- Verificar que os ficheiros se encontram associados à categoria correta;
  <br>

---

#### **[REQ-16] Visualização de ficheiros por categoria**

---

**Objetivo:** Deverá aparecer a lista de todos os ficheiros da categoria selecionada<br>
**User Story:** O utilizador quer visualizar a lista de todos os ficheiros por categoria porque, assim, a procura de ficheiros fica mais fácil<br>
**Design:** Deverá aparecer um dropdown bar à direita na vista 'Lista de ficheiros' que me permita escolher uma categoria, ex: 'DEV'. Quando carregando na mesma, devo poder visualizar apenas os ficheiros dessa mesma categoria<br>
**Prioridade:** M☐ S☒ C☐<br>
**Plano de Testes:**

- Verificar se todos os ficheiros se encontram listados;
- Verificar que os ficheiros se encontram associados à categoria correta;
  <br>

---

#### **[REQ-36] Lista ficheiros: Agrupar por intensidade de alterações**

---

**Objetivo:** Deverá ser apresentada a lista (tabela) de todos os ficheiros do repositório agrupados por intensidade de alterações<br>
**User Story:** O cliente quer poder visualizar uma lista de todos os ficheiros do repositório ordenada por intensidade de alterações porque, assim, fica informado dos ficheiros que sofreram mais e menos alterações<br>
**Design:** Deverá aparecer um dropdown bar à direita na vista 'Lista de ficheiros' que me permita escolher 'Intensidade'. Quando carregando na mesma, devo poder visualizar os ficheiros separados por intensidade.<br>
**Prioridade:** M☐ S☐ C☒<br>
**Plano de Testes:**

- Verificar se apresenta a lista de todos os ficheiros do repositório agrupados por intensidade de alterações
- Testar se as cores estão de acordo com o número de alterações

<br>

---

#### **[REQ-46] Eventos organizados por commit instants**

---

**Objetivo:** Os eventos na linha temporal devem estar organizados por commit instants.<br>
**User Story:** O utilizador quer que os eventos estejam organizados por commit instants porque, assim, pode ter uma melhor visualização da timeline <br>
**Design:** <br>
**Prioridade:** M☐ S☐ C☒<br>
**Plano de Testes:**

- Verificar que quando se carrega num instante, os commits apresentados se encontram organizados pelo que aconteceu primeiro.
  <br>

---

#### **[REQ-45] Visualização temporal uniforme**

---

**Objetivo:** Deverá apresentar a visualização temporal (linha de tempo) que deve ser uniforme<br>
**User Story:** O utilizador quer que seja apresentada uma visualização temporal uniforme para ver a evolução do projeto ao longo do tempo<br>
**Design:** <br>
**Prioridade:** M☐ S☐ C☒<br>
**Plano de Testes:**

- Veriricar se a linha de tempo se encontra visível de uma forma uniforme.
  <br>

---

#### **[REQ-27] Timeline da evolução da lista de ficheiros**

---

**Objetivo:** Deverá ser possível visualizar a evolução da lista de ficheiros ao longo do tempo<br>
**User Story:** O utilizador quer visualizar a evolução da lista ao longo do tempo porque assim será possível visualizar a evolução do projeto ao longo do tempo<br>
**Design:** <br>
**Prioridade:** M☐ S☐ C☒<br>
**Plano de Testes:**

- Deverá ser possível carregar num instante e visualizar a lista de ficheiros desse instante;
- Confirmar se os ficheiros apresentados eram realmente os que estavam disponíveis naquele instante.
  <br>

## **Vista de um ficheiro**

---

#### **[REQ-20] Modo de visualização dos ficheiros: Nome e extensão**

---

**Objetivo:** Sempre que houver um ficheiro, o mesmo deve conter a categoria, o nome e a extensão do próprio.<br>
**User Story:** O utilizador quer poder ver o nome, extensão e categoria dos ficheiros porque permite perceber a finalidade dos mesmos, o seu nome e o tipo de ficheiro que é<br>
**Design:** Quando visualizamos um ficheiro terá de conter o nome, a extensão e à frente do mesmo o nome da categoria onde se encontra inserido, ex:
| nome | path | tamanho |
| :---------------- | :--------------------------: | :------: |
| manual_req.md | req | 32 bytes |
| perfil_maria | pm/profiles | 54 bytes |

**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- Em todas as vistas que possa conter um ficheiro verificar se os ficheiros estão representados com os 3 parâmetros;
- Verificar se a categoria do ficheiro está correta em relação à pasta onde ele se encontra.
  <br>

---

#### **[REQ-12] Visualização do tempo desde a última atualização**

---

**Objetivo:** O momento da última atualização dos dados deverá estar sempre presente no ecrã<br>
**User Story:** O utilizador quer visualizar o tempo desde a última atualização dos dados porque lhe permite saber o quão atualizada está a informação.<br>
**Design:** Quando visualizamos um ficheiro terá de conter o momento da última atualização à frente do mesmo, ex:
| nome | Última atualização |
| :---------------- | :------: |
| manual_req.md | 2 dias |
| perfil_maria | 1h |

**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- Verificar se é visível no ecrã o tempo desde a última atualização;
- Verificar se o tempo se encontra correto.
  <br>

---

#### **[REQ-5] Visualização de ficheiros por 'churn'**

---

**Objetivo:** Deverá existir uma bola, associada a cada ficheiro, com a cor correspondente ao nível de alterações nesse ficheiro<br>
**User Story:** O utilizador quer outra vista dos ficheiros no repositório porque pretende visualizar quais são os ficheiros mais/menos alterados.<br>
**Design:**

- Deverá existir uma bola, associada a cada ficheiro, com a cor correspondente ao nível de alterações nesse ficheiro - a grading scale deve ser relativa e não absoluta (10 alterações num ficheiro em que o máximo no repositório foram 1000 alterações é diferente de 10 alterações num máximo de 20. Considerar a possibilidade de usar sliding windows temporais para calcular a intensidade de alterações)<br>
- A cor deverá ser definida por intensidade das alterações, desde por ex: red hot (muito alterado) a light grey (muito pouco alterados). Deve ser usada uma palette adequada a pessoas daltónicas<br>

**Prioridade:** M☐ S☐ C☒<br>
**Plano de Testes:**

- Verificar se todos os ficheiros têm o seu churn associado;
- Verificar se o churn realmente é adequado ao ficheiro.
  <br>

---

#### **[REQ-14] Lista de ficheiros: Visualizar conteúdo do ficheiro**

---

**Objetivo:** Deverá ser possível visualizar o conteúdo de um ficheiro num commit<br>
**User Story:** O utilizador quer ao clicar num ficheiro visualizar o conteúdo naquele commit para poder verificar o seu contéudo<br>
**Design:** Deve haver um link no nome do ficheiro de modo a levar à visualização do contéudo de um ficheiro num commit quando o mesmo é clicado<br>
**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- Confirmar se a versão do ficheiro representada é aquela que corresponde ao último commit
- Verificar se o nome apresentado corresponde à pessoa que realizou o último commit

<br>

---

#### **[REQ-8] História de commits do ficheiro**

---

**Objetivo:** Deverá apresentar uma lista de commits de um ficheiro ordenada por data/hora. Deverá conter também o nome da pessoa que efetuou cada commit nesse mesmo ficheiro<br>
**User Story:** O utilizador pretende ver a lista dos commits e quem efetuou cada commit porque, assim, pode ver quem fez alterações a um dado ficheiro<br>
**Design:** No canto superior direito, existirá um botão que leva para o histórico de um ficheiro, ou seja, lista de commits desse mesmo ficheiro ordenado por data/hora. Deverá conter também o nome da pessoa que efetuou cada commit nesse mesmo ficheiro<br>
**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- Deve aparecer o nome do autor do commit;
- Serão apresentadas as imagens de perfil de cada membro que efetuou o commit;
- Certificar que a imagem correponde ao membro correto;
  <br>

---

#### **[REQ-1] Informação estática ficheiro: Tipo, dimensão e complexidade**

---

**Objetivo:** Adicionar informação estática sobre o ficheiro: Tipo, dimensão e complexidade<br>
**User Story:** O utilizador quer poder visualizar a informação estática referida acima porque são dados relevantes para análise<br>
**Design:** <br>
**Prioridade:** M☐ S☒ C☒<br>
**Plano de Testes:**

- Verificar que toda a informação é mostrada;
- Verificar a veracidade da informação.
  <br>

---

#### **[REQ-13] Informação estática ficheiro: métricas de estabilidade**

---

**Objetivo:** Apresentar as métricas de estabilidade.<br>
**User Story:** O utilizador quer visualizar as métricas da informação estática dos ficheiros porque isto permite uma melhor análise dos dados<br>
**Design:** Deverá ser ap<br>
**Prioridade:** M☐ S☐ C☒<br>
**Plano de Testes:**

- Verificar se as métricas de estabilidade são apresentadas;
- Verificar a veracidade das métricas de estabilidade.
  <br>

---

#### **[REQ-2] Issues associados a cada ficheiro**

---

**Objetivo:** Deverá ser possível saber, por ficheiro, quais os issues que lhe estão associados<br>
**User Story:** O utilizador quer saber, por ficheiro, quais os issues que lhe estão associados porque é mais fácil de visualizar o trabalho associado a esse mesmo ficheiro<br>
**Design:** Aparecerá sob a forma de texto os issues que lhe estão associados com link para o dado issue<br>
**Prioridade:** M☐ S☒ C☐<br>
**Plano de Testes:**

- Verificar se, nos ficheiros que contêm issue associado, o mesmo é realmente daquele ficheiro.
  <br>

---

#### **[REQ-18] História de defeitos pendentes de um ficheiro**

---

**Objetivo:** Deverá ser possível visualizar a existência de defeitos pendentes nesse mesmo ficheiro através de um ticket/issue<br>
**User Story:** O utilizador quer poder selecionar um ficheiro e ver quais tickets/issues/defeitos estão associados ao mesmo, permitindo uma procura mais eficiente.<br>
**Design:** Aparecerá sob a forma de texto os defeitos que lhe estão associados com link para o dado issue com os defeitos<br>
**Prioridade:** M☐ S☐ C☒<br>
**Plano de Testes:**

- Verificar se conseguimos aceder aos issues de defeitos pendentes de um ficheiro;
- Verificar se aquele issue está mesmo associado ao ficheiro.
  <br>

## **Vista de Commits**

---

#### **[REQ-30] Visualizar lista de commits**

---

**Objetivo:** Deverá ser possível visualizar uma lista de commits<br>
**User Story:** O utilizador quer visualizar uma lista de commits porque são dados relevantes para análise.<br>
**Design:** Deverá aparecer uma lista de commits quando se clica na barra lateral esquerda em 'Commits' de todos os autores<br>
**Prioridade:** M☒ S☐ C☐<br><
**Plano de testes:**

- Na barra lateral, deve ser possível carregar na vista de commits;
- Os ficheiros deverão aparecer oganizados por data e hora;
- Garantir que todos os ficheiros se encontram listados;
- Verificar que a cada autor/membro corresponde os respetivos ficheiros;
- Garantir que é possível ver todos os ficheiros do respetivo autor/membro;
- Deverá existir um dropdwn bar que permita escolher uma categoria.

## <br>

#### **[REQ-31] Lista de commits de cada membro**

---

**Objetivo:** Deverá aparecer a lista de todos os commits agrupados por autor(ordem alfabética)<br>
**User Story:** O utilizador quer poder observar a lista de commits de cada membro porque, assim, pode monitorar a atividade dos membros<br>
**Design:** Deverá aparecer um dropdown bar que me permita escolher um membro ativo, ex: 'Autor A'. Quando carregando no mesmo, devo poder visualizar os commits desse mesmo membro<br>
**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- Deverá existir uma dropdown bar para filtrar por autor;
- Deverá ser possível ser possível ver os commits de determinado autor.
  <br>

---

#### **[REQ-43] Contribuições de todos os membros por categoria**

---

**Objetivo:** Deverá ser possível visualizar os commits agrupados por categoria (ordem alfabética)<br>
**User Story:** O utilizador quer ver as contribuições de todos os membros por categoria porque, assim, pode ver quais são as categorias com maior e menor número de commits<br>
**Design:** Deverá aparecer um dropdown bar que me permita escolher uma categoria, ex: 'Pasta de REQ'. Quando carregando na mesma, devo poder visualizar os commits dessa mesma categoria.
<br>
**Prioridade:** M☐ S☒ C☐<br>
**Plano de Testes:**

- Deverá ser possível filtrar por categoria;
- Confirmar se existem todas as categorias;
- Garantir que a categoria apresentada é a pretendida;
- Confirmar que os commits apresentados pertencem à respetiva categoria.

<br>

---

#### **[REQ-29] Por commit ver ficheiros alterados e autor**

---

**Objetivo:** Deverá ser possível ver, por cada commit, quais os ficheiros que foram modificados nesse commit, bem como o respectivo autor<br>
**User Story:** O utilizador quer visualizar o conteúdo de um commit porque são dados importantes para análise.<br>
**Design:** Devem aparecer os vários ficheiros alterados sob a forma de dropdown bar. O nome aparecerá sob a forma de texto, ex: "Commit realizado por Maria"<br>
**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- Verificar se conseguimos identificar todos os ficheiros pertencentes a um commit;
- Veirificar se o autor apresentado corresponde ao autor do commit.

<br>

## **Histórico de um ficheiro**

#### **[REQ-22] História de alterações no ficheiro**

---

**Objetivo:** Deverá ser possível visualizar a dimensão das alterações (nr de linhas alteradas/acrescentadas/removidas) em cada commit<br>
**User Story:** O utilizador quer poder ver o número e tipo de alterações realizadas em cada commit para possibilitar comparação e análise das mesmas<br>
**Design:** Deve aparecer cada ficehiro sobre a forma de dropdown bar. Cada dropdown conterá um texto com o número de linhas alteradas/acrescentadas/removidas. O contéudo do dropdown são as linhas que foram alteradas e o que foi alterado.<br>
**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- Os commits deverão aparecer ordenados pela hora do dia em que foram efetuados, aparecendo o mais recente no topo;
- Verificar que o dia e hora estão corretos;
- Confirmar se os commits esão ordenados pelo dia;
- Garantir que dentro de cada dia, os commits estão bem ordenados de acordo com as horas;
- Verificar as alterações efetuadas pelo autor;

<br>

## **Issues**

---

#### **[REQ-51] Lista de issues: abertos, fechados e total**

---

**Objetivo:** Deverá ser possível ver a lista de issues abertos, fechados e total<br>
**User Story:** O utilizador quer visualizar a lista de issues abertos, fechados e total porque lhe permite ter as coisas organizadas e é de navegação mais fácil entre as diferentes issues: abertos, fechados e total.<br>
**Design:**

- Terá que se carregar num botão 'Abertos' de modo a aparecer sob a forma de lista as issues abertas
- Terá que se carregar num botão 'Fechados' de modo a aparecer sob a forma de lista as issues fechadas
- Terá que se carregar num botão 'Todos' de modo a aparecer sob a forma de lista todas as issues

**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- Verificar se é possíver ver uma lista de issues abertos, fechados e total.
  <br>

---

#### **[REQ-26] Timeline de issues: abertos, ativos e total**

---

**Objetivo:** Deverá ser possível visualizar a timeline de issues ao longo do tempo: abertos, fechados e total<br>
**User Story:** O utilizador quer visualizar a timeline das issues porque assim pode saber em que estado cada uma delas se encontra.<br>
**Design:** Existirá uma 'timeline' no topo da vista de modo a permitir clicar num dado instante<br>
**Prioridade:** M☐ S☐ C☒<br>
**Plano de Testes:**

- Deverá ser possível visualizar uma timeline.
  <br>

---

#### **[REQ-32] Clicando em cada instante ver lista de issues**

---

**Objetivo:** Clicando em cada instante deveremos poder ver a lista de issues abertos e ativos desse momento <br>
**User Story:** O utilizador quer poder ver a lista de issues abertos e activos num dado momento de forma a perceber o trabalho que está a ser desenvolvido.<br>
**Design:** Existirá uma 'timeline' no topo da vista de modo a permitir ver quando clicando num instante todos os issues: abertos, ativos e total<br>
**Prioridade:** M☐ S☐ C☒<br>
**Plano de Testes:**

- Deverá ser possível carregar num instante e visualizar apenas os issues criados até esse instante;
- Por baixo da Timeline deverão existir 3 listas com issues: lista de issues abertos, fechados e o total;
- Nas listas, os issues deverão estar por ordem de criação (mais recentes primeiro);
- Um issue não pode estar ao mesmo tempo na lista de aberto e fechado.
  <br>

## **Vista de uma issue**

---

#### **[REQ-52] Issue: título**

---

**Objetivo:** Deverá ser possível ver o título de uma issue<br>
**User Story:** O utilizador quer visualizar o título de uma issue porque lhe permite saber brevemente de que se trata a issue<br>
**Design:** Deverá aparecer sob a forma de texto o título da issue<br>
**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- Verificar se é possível ver o título da issue;
- Verificar se o título corresponde verdadeiramente àquela issue.
  <br>

---

#### **[REQ-53] Issue: descrição**

---

**Objetivo:** Deverá ser possível ver a descrição de uma issue<br>
**User Story:** O utilizador quer visualizar a descrição de uma issue porque quer saber qual é o problema que a issue resolve uma vez que o título pode não esclarecer essa questão<br>
**Design:** Deverá aparecer sob a forma de texto e, por baixo do título da issue, a descrição da mesma<br>
**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- Verificar se é possível ver a descrição da issue;
- Verificar se a descrição corresponde verdadeiramente àquela issue.
  <br>

---

#### **[REQ-54] Issue: Nome da pessoa que a criou**

---

**Objetivo:** Deverá ser possível ver o nome da pessoa que criou a issue<br>
**User Story:** O utilizador quer visualizar o nome da pessoa que criou a issue porque quer ter a perceção da pessoa responsável pela sua criação<br>
**Design:** Por cima do título da issue deverá aparecer sob a forma de texto o autor da issue<br>
**Prioridade:** M☒ S☐ C☐<br>
**Plano de Testes:**

- Verificar se é possível ver o nome do autor da issue;
- Verificar se aquele autor corresponde verdadeiramente àquela issue.
  <br>

---

#### **[REQ-55] Issue: Estado da issue**

---

**Objetivo:** Deverá ser possível ver o estado da issue: aberto ou fechado<br>
**User Story:** O utilizador quer que seja possível ver o estado da issue porque lhe permite saber <br>
**Design:** O estado da issue deve de aparecer antes do nome do autor sob a forma de texto<br>
**Prioridade:** M☐ S☐ C☒<br>
**Plano de Testes:**

- Verificar se é possível ver o estado da issue;
- Verificar se o estado corresponde verdadeiramente àquela issue.
  <br>

## 3.3 Mapa de Navegação
![](./imagens/Mapa_de_navegacao.png)

## 3.4 Mockups

<details>
<summary>Landing Page</summary>

![image](Mockups/Landing%20Page.png)

</details>

<details>
<summary>Vista do Home</summary>

![image](Mockups/Home-1.png)

</details>

<details>
<summary>Vista do Home - Todas as Categorias</summary>

![image](Mockups/Home%20Todas%20as%20Categorias-1.png)

</details>

<details>
<summary>Vista do Home - Todos os Autores</summary>

![image](Mockups/Home%20Todos%20os%20Autores-1.png)

</details>

<details>
<summary>Vista do Home - 1 Categoria</summary>

![image](Mockups/Home%201%20Categoria-1.png)

</details>

<details>
<summary>Vista do Home - 1 Autor</summary>

![image](Mockups/Home%201%20Autor-1.png)

</details>

<details>
<summary>Vista do Home - Churn</summary>

![image](Mockups/Home%20Churn-1.png)

</details>

<details>
<summary>Vista do Departamento</summary>

![image](Mockups/Departamento-1.png)

</details>

<details>
<summary>Vista do Departamento - Privado</summary>

![image](Mockups/Departamento%20Privado-1.png)

</details>

<details>
<summary>Vista de Commits</summary>

![image](Mockups/Commits-1.png)

</details>

<details>
<summary>Vista de Commits - Todos os Autores</summary>

![image](Mockups/Commits%20Todos%20os%20Autores-1.png)

</details>

<details>
<summary>Vista de Commits - 1 Autor</summary>

![image](Mockups/Commits%201%20Autor-1.png)

</details>

<details>
<summary>Vista de Commits - Todas as Categorias</summary>

![image](Mockups/Commits%20Todas%20as%20Categorias-1.png)

</details>

<details>
<summary>Vista de Commits - 1 Categoria</summary>

![image](Mockups/Commits%201%20Categoria-1.png)

</details>

<details>
<summary>Vista de 1 Ficheiro</summary>

![image](Mockups/Vista%20de%201%20Ficheiro-1.png)

</details>

<details>
<summary>Vista do Histórico de 1 Ficheiro</summary>

![image](Mockups/Histórico%20de%201%20Ficheiro-1.png)

</details>

<details>
<summary>Vista do Histórico de Alterações 1 Ficheiro</summary>

![image](Mockups/Histórico%20de%20Alterações%201%20Ficheiro-1.png)

</details>

<details>
<summary>Vista do Login</summary>

![image](Mockups/Login-1.png)

</details>

<details>
<summary>Vista dos Issues</summary>

![image](Mockups/Issues-1.png)

</details>

<details>
<summary>Vista de 1 Issue</summary>

![image](Mockups/1%20Issue-1.png)

</details>

## 3.5 Requisitos lógicos da base de dados

## **Tipo(s) de informação usada pelas várias funções**

As funções usam o _models.**str**()_ para devolver uma _string/charfield_ com as variadas informações necessárias. Por exemplo, no caso do repositório, esta função devolverá o _id_ e o nome do mesmo para.
No caso dos utilizadores, para além do nome e do _id_, ainda devolve o _username_ e um _charfield_ com o _link_ da imagem do utilizador.

## **Entidades e relações**

- Entidades:<br>
  -> Repo &emsp; &emsp; - possui os seguintes parâmetros:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _id_ id do repositório;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp;&emsp; &emsp; _name_ nome do repositório;<br>
  -> User &emsp; &emsp; - possui os seguintes parâmetros:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _id_ id do user;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _name_ nome do user;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _username_ nome de perfil;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _image_ url com a imagem de perfil;<br>
  -> UserRepo &emsp; - possui as seguintes foreign keys:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _user_id_ fk que aponta para o id do user;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _repo_id_ fk que aponta para o id do repositório;<br>
  -> Branch &emsp; &emsp; - possui os seguintes parâmetros:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _id_ id da branch;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _name_ nome da branch;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _alive_ valor que determina o estado da branch, eliminada ou não;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; - possui as seguintes foreign keys:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _user_id_ fk que aponta para o id do user;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _repo_id_ fk que aponta para o id do repositório;<br>
  -> Commit &emsp; - possui os seguintes parâmetros:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _id_ id do commit;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _name_ nome do commit;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _time_creation_ tempo de criação do commit;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _commit_gitlab_id_ id do commit dado pelo gitlab;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; - possui as seguintes foreign keys:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _user_id_ que aponta para o id do user;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _repo_id_ que aponta para o id do repositório;<br>
  -> BranchCommit &emsp; - possui as seguintes foreign keys:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _branch_id_ fk que aponta para o id da branch;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _commit_id_ fk que aponta para o id do commit;<br>
  -> Folder &emsp; &emsp; - possui os seguintes parâmetros:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _id_ id da pasta;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _time_creation_ tempo do commit no qual foi criada a pasta;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _dir_ diretoria da pasta; - possui as seguintes foreign keys:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _commit_id_ fk que aponta para o id do commit;<br>
  -> File &emsp; &emsp; - possui os seguintes parâmetros:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _id_ id do ficheiro;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _name_ nome do ficheiro;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _content_ conteúdo do ficheiro;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _time_creation_ tempo do commit no qual foi criado ou alterado o ficheiro;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _dir_ diretoria do ficheiro;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; - possui as seguintes foreign keys:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp;&emsp; &emsp; _commit_id_ fk que aponta para o id do commit;<br>
  -> Issue &emsp; &emsp; - possui os seguintes parâmetros:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _id_ id do problema;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _title_ titulo do problema;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _description_ descrição do problema;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _requirement_ tipo de problema;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _time_creation_ tempo de criação do problema; <br>
  &emsp; &emsp; &emsp; &emsp; &emsp; - possui as seguintes foreign keys:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _user_id_create_ fk que aponta para o id do user que criou o problema;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _repo_id_ fk que aponta para o id do repositório;<br>
  -> UserIssue &emsp; - possui as seguintes foreign keys:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _user_id_ fk que aponta para o id do user;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _issue_id_ fk que aponta para o id do problema;<br>
  -> Label &emsp; &emsp; - possui os seguintes parâmetros:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp;&emsp; &emsp; _id_ id da legenda;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _name_ nome da legenda;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _description_ descrição da legenda; <br>
  &emsp; &emsp; &emsp; &emsp; &emsp; - possui as seguintes foreign keys:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _repo_id_ fk que aponta para o id do repositório;<br>
  -> IssueLabel &emsp; - possui as seguintes foreign keys:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _issue_id_ fk que aponta para o id do problema;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _label_id_ fk que aponta para o id da legenda;<br>
  -> MergeRequest &emsp; - possui os seguintes parâmetros:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _id_ id do merge request;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _name_ nome do merge request;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _target_branch_id_ id da merge request de destino;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _description_ descrição do merge request;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _time_creation_ tempo de criação do merge request;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _time_merged_ tempo de quando ocorreu o merge;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _open_merge_request_ parametro que determina o estado do merge request, aberto, fechado, merged; <br>
  &emsp; &emsp; &emsp; &emsp; &emsp; - possui as seguintes foreign keys:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _source_branch_id_ fk que aponta para o id da branch de origem;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _user_id_ fk que aponta para o id do user;<br>
  -> MergeRequestLabel &emsp; - possui as seguintes foreign keys:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _merge_request_id_ fk que aponta para o id do merge request;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _label_id_ fk que aponta para o id da legenda;<br>
  -> IssueMergeRequest &emsp; - possui as seguintes foreign keys:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _issue_id_ fk que aponta para o id do problema;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _merge_request_id_ fk que aponta para o id do merge request;<br>
  -> MergeRequestCommit &emsp; - possui as seguintes foreign keys:<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _merge_request_id_ fk que aponta para o id do merge request;<br>
  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; _commit_id_ fk que aponta para o id do commit;<br>

Imagens referentes aos diagramas físico e relacional da base de dados, respetivamente:
![Modelo Físico](imagens/model_PH_ES_final_4.png)
![Modelo Relacional](imagens/model_ER_ES_final_4.png)

# 3.6 Atributos do sistema de software - requisitos não funcionais

Os Requisitos Não Funcionais (NFRs) definem os atributos do sistema, como segurança, confiabilidade, desempenho, capacidade de manutenção, escalabilidade e usabilidade.

Eles servem como restrições ou restrições ao design do sistema entre as diferentes pendências. Garantem a usabilidade e eficácia de todo o sistema e o não cumprimento de qualquer um deles pode resultar em sistemas que não atendem às necessidades internas do negócio, do utilizador ou do mercado.

Como todos os outros requisitos, os NFRs devem ser simples e claros para garantir que os desejos das partes interessadas sejam claramente compreendidos por todos.

Os seguintes critérios ajudam a definir os NFRs:<br>

**Limitados** - quando não têm contexto limitado, os NFRs podem ser irrelevantes e levar a um trabalho adicional significativo.<br>
**Independentes** - os NFRs devem ser independentes uns dos outros para que possam ser avaliados e testados sem consideração ou impacto de outras qualidades do sistema.<br>
**Negociáveis** - compreender os motivadores de negócios NFR e o contexto limitado exige a negociabilidade.<br>
**Testáveis** - NFRs devem ser afirmados com critérios objetivos, mensuráveis e verificáveis.<br>

Os Requisitos Não Funcionais do nosso projeto são:<br>

**[REQ-24] Tecnologia: Python** - A tecnologia a adoptar deverá ser baseada em Python.<br>
**[REQ-25] Acesso à informação interna do projeto** - Apenas utilizadores autenticados poderão aceder a informação interna do projecto. Só é necessária autenticação quando os projetos são privados.<br>
**[REQ-37] Boa usabilidade do sistema** - O sistema deverá ter uma boa usabilidade. Boa usabilidade é caracterizada pela escala SUS para avaliar a usabilidade da dashboard.<br>
**[REQ-38] Solução: Web-based** - A solução deve ser web-based.<br>
**[REQ-41] Multi-idioma da dashboard** - A dashboard deverá suportar multi-idioma.<br>

# 4. Mudanças no processo de gestão

Sempre que há um pedido de mudança de um requisito por parte do cliente quer seja a nível de funcionalide, quer seja a nível de prioridade, é reportada a situação à equipa através do canal de comunicação - Discord - e é alterada a descrição do requisito no GitLab.

Posteriormente, se a equipa de desenvolvimento - DEV - entender que o requisito não é possível de concretizar na integra como pedido então é comunicada a situação à equipa de requisitos - REQ. De seguida, a mesma fala com o cliente a explicar a situação, propõe uma nova solução e tenta chegar a um concenso.

# 5. Aprovação do documento

Nesta secção, são identificados todos _reviewers_ do documento:

| Nome        | Data       |
| ----------- | ---------- |
| Sofia Neves | 04.12.2021 |
| Sofia Neves | 18.12.2021 |
