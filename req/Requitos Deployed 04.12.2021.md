## **COMO CORRER**
Para correr é necessário seguir o seguinte conjunto de passos:
1. Conectar-se à VPN do DEI - [link de configuração](https://helpdesk.dei.uc.pt/configuration-instructions/vpn-access/) **OU** conectar-se à rede do DEI 
2. Aceder ao [link](http://dash-it.dei.uc.pt:8080/) e testar
<br>

## **REQUISITOS DEPLOYED**

---

#### **[REQ-15] Lista de membros ativos do repositório**

---

**Objetivo:** Deverá existir uma página com a lista de membros ativos do repositório. Um membro ativo é aquele que fez pelo menos um commit.<br>
**User Story:** O utilizador quer poder visualizar os membros ativos do repositório para poder saber quem é que faz parte do repositório. O utilizador quer não ter "poluição" na dashboard com membros inativos porque tem maior legibilidade<br>
**Prioridade:** M☒ S☐ C☐<br><br>

**Casos de teste**
- Deverá existir uma página com os dados relativos à equipa;<br>
- Será possivel ver listado por ordem alfabética todos os membros ativos da equipa;<br>
- No gráfico deverão estar todos os departamentos participantes no projeto;<br>
- Os membros ativos deverão ter pelo menos 1 commit realizado;<br>
- Os membros deverão estar listados por ordem alfabética;<br><br>

---

#### **[REQ-26] Issues: abertos, ativos e total**

---

**Objetivo:** Deverá ser possível visualizar as últimas 50 issues ao longo do tempo: abertos, fechados e total<br>
**User Story:** O utilizador quer visualizar as issues porque assim pode saber em que estado cada uma delas se encontra.<br>
**Prioridade:** M☒ S☐ C☐<br><br>

**Casos de teste**
- Verificar se as issues apresentadas são as últimas 50
- Verificar se se encontram na aba correta (aberto e fechado)
- Confirmar o número do id associado à issue



---

#### **[REQ-29] Por commit ver ficheiros alterados e autor**

---

**Objetivo:** Deverá ser possível ver, por cada commit, quais os ficheiros que foram modificados nesse commit, bem como o respectivo autor<br>
**User Story:** O utilizador quer visualizar o conteúdo de um commit porque são dados importantes para análise.<br>
**Prioridade:** M☒ S☐ C☐<br><br>

**Casos de teste**
- Confirmar se o commit tem aquele autor associado 
- Confirmar se o commit tem aquele ficheiro associado




---

#### **[REQ-30] Visualizar lista de commits**

---

**Objetivo:** Deverá ser possível visualizar uma lista de commits<br>
**User Story:** O utilizador quer visualizar uma lista de commits porque são dados relevantes para análise.<br>
**Prioridade:** M☒ S☐ C☐<br><br>

**Casos de teste**
    Existem diferentes casos de teste para este requisito uma vez que o mesmo se encontra em várias subvistas

**Vista de commits**

- Na barra lateral, deve ser possível carregar na vista de commits;<br>
- Deverá aparecer a lista de commits de todos os ficheiros;<br>
- Os ficheiros deverão aparecer oganizados por data e hora;<br>
- Verificar se os ficheiros estão ordenados corretamente;<br><br>

**Vista de commits de todos os autores**

- A lista deverá estar ordenada por ordem alfabética dos autores/membros;
- Verificar que a cada autor/membro corresponde os respetivos ficheiros;
- Garantir que é possível ver todos os ficheiros do respetivo autor/membro;
- Verificar que se encontra por ordem alfabética dos autores/membros;
- Confirmar que entre cada autor/membro, os ficheiros se encontram por ordenadas por data e hora;
- Deverá existir um dropdwn bar que permita escolher uma autor/membro;

**Vista de um autor**

- Deverá aparecer a lista de todos os commits do respetivo membro/autor;
- Verificar se todos os commits pertencem ao respetivo autor/membro;
- Garantir que é possível ver todos os ficheiros do respetivo autor/membro;
- Verificar que os commits se encontram ordenados por data e hora.

**Vista de todas as categorias**

- O agrupamento de commits deve aparecer por ordem alfabética;
- Garantir que é possível ver todos os ficheiros de cada categoria;
- Deverá existir um dropdwn bar que permita escolher uma categoria;
- Com o novo dropdown bar deverá ser possível visualizar os commits da categoria escolhida;

**Vista de uma categoria**

- Deverá aparecer a lista de todos os commits da respetiva categoria;
- Verificar que é possível ver todos os ficheiros da categoria escolhida;
- Confirmar se todos os commits pertencem à respetiva categoria;
- Verificar que os commits se encontram ordenados por data e hora.<br><br>

---

#### **[REQ-31] Lista de commits de cada membro**

---

**Objetivo:** Deverá aparecer a lista de todos os commits agrupados por autor(ordem alfabética)<br>
**User Story:** O utilizador quer poder observar a lista de commits de cada membro porque, assim, pode monitorar a atividade dos membros<br>
**Prioridade:** M☒ S☐ C☐<br><br>
**Casos de teste**
- Deverá existir uma dropdown bar para filtrar;<br>
- Por commit ver ficheiros alterados e autor.<br><br>

---

#### **[REQ-4] Recolha de dados automática**

---

**Objetivo:** Cada novo acesso à _dashboard_ deve envolver a recolha dos dados de atividade mais recente no repositório.<br>
**User Story:** O utilizador quer que a cada novo acesso à dashboard exista recolha dos dados da atividade mais recente no repositório porque quer visualizar sempre a informação mais atualizada do repositório<br>
**Prioridade:** M☒ S☐ C☐<br><br>
**Casos de teste**
- Sempre que se acede à dashboard, verificar se os dados estão atualizados.
- Verificar se há atualização dos dados quando se carrega no botão de refresh<br><br>

---

#### **[REQ-10] Escolha do repositório a analisar**

---

**Objetivo:** Escolher o repositório GitLab a analisar<br>
**User Story:** O utilizador quer poder escolher o repositório GitLab a analisar para efetuar a avaliação dos seus membros e examinar todos os progressos<br>
**Prioridade:** M☒ S☐ C☐<br><br>
**Casos de teste**
- Escolher um repositório privado e não deverá ser possível analisá-lo
- Escolher um repositório público de elevada dimensão e deverá ser possível analisá-lo
- Escolher um repositório que não existe e não deverá encontrar
- Escolher um repositório vazio

---

#### **[REQ-9] Criação de lista de ficheiros do repositório**

---

**Objetivo:** Deverá ser apresentada a lista de ficheiros do repositório na vista por árvore<br>
**User Story:** O utilizador quer visualizar a lista de ficheiros do repositório porque lhe permite saber quais os ficheiros existentes no repositório<br>
**Prioridade:** M☒ S☐ C☐<br><br>
**Casos de teste**
- Verificar se todos os ficheiros se encontram listados


#### **[REQ-12] Visualização do tempo desde a última atualização**

---

**Objetivo:** O momento da última atualização dos dados deverá estar sempre presente no ecrã<br>
**User Story:** O utilizador quer visualizar o tempo desde a última atualização dos dados porque lhe permite saber o quão atualizada está a informação.<br>
**Prioridade:** M☒ S☐ C☐<br><br>
**Casos de teste**
- Verificar a credibilidade do tempo apresentado<br><br>

---

#### **[REQ-20] Modo de visualização dos ficheiros: Nome e extensão**

---

**Objetivo:** Sempre que houver um ficheiro, o mesmo deve conter o nome e a extensão do próprio.<br>
**User Story:** O utilizador quer poder ver o nome e extensão dos ficheiros porque permite perceber a finalidade dos mesmos, o seu nome e o tipo de ficheiro que é<br>
**Prioridade:** M☒ S☐ C☐<br><br>
**Casos de teste**
- Em todas as vistas que possa conter um ficheiro verificar se os ficheiros estão representados com os 3 parâmetros
- Verificar se a categoria do ficheiro está correta em relação à pasta onde ele se encontra<br><br>

---

#### **[REQ-14] Árvore ficheiros: Visualizar conteúdo do ficheiro**

---

**Objetivo:** Deverá ser possível visualizar o conteúdo de um ficheiro num commit<br>
**User Story:** O utilizador quer ao clicar num ficheiro visualizar o conteúdo naquele commit para poder verificar o seu contéudo<br>
**Prioridade:** M☒ S☐ C☐<br><br>
**Casos de teste**
- Confirmar se a versão do ficheiro representada é aquela que corresponde ao último commit
- Verificar se o nome apresentado corresponde à pessoa que realizou o último commit<br><br>

---

#### **[REQ-1] Informação estática ficheiro: Tipo**

---

**Objetivo:** Adicionar informação estática sobre o ficheiro: Tipo<br>
**User Story:** O utilizador quer poder visualizar a informação estática referida acima porque são dados relevantes para análise<br>
**Prioridade:** M☐ S☒ C☐<br><br>
**Casos de teste**
- Quando um ficheiro é aberto deverá ser vísivel a extensão do mesmo

---

#### **[REQ-16/3] Visualização de ficheiros por categoria**

---

**Objetivo:** Deverá aparecer a lista de todos os ficheiros da categoria selecionada<br>
**User Story:** O utilizador quer visualizar a lista de todos os ficheiros por categoria porque, assim, a procura de ficheiros fica mais fácil<br>
**Prioridade:** M☐ S☒ C☐<br><br>

**Casos de teste**
Existem diferentes casos de teste para este requisito uma vez que o mesmo se encontra em várias subvistas

**Homepage - Todas as categorias**

- Verificar se aparece a lista de todos os ficheiros agrupados por categoria
- Testar se as categorias estão ordenadas por ordem alfabética
- Testar o que acontece aos ficheiros sem uma categoria associada
- Dropdown bar para filtrar por categoria

**Subvista da homepage uma categoria**

- Testar se aparecem apenas ficheiros da categoria selecionada<br><br>

---

#### **[REQ-6] Lista ficheiros: Agrupar ficheiros por autor**

---

**Objetivo:** Deverá aparecer a lista de todos os ficheiros agrupados por autor<br>
**User Story:** O utilizador quer ver todos os ficheiros do repositório agrupados por autor porque permite uma análise desses dados mais fácil<br>
**Prioridade:** M☐ S☒ C☐ <br>
**Casos de teste**
- Verificar se aparece a lista de todos os ficheiros agrupados por autor
- Testar se os ficheiros estão ordenados por ordem alfabética
- Dropdown bar para filtrar por membro ativo
- Testar o que acontece a um ficheiro com mais que um autor - aparece o nome de todos os autores à frente?
<br><br>

