<details>
<summary>Landing Page</summary>

![image](Mockups/Landing%20Page.png)
</details>

<details>
<summary>Vista do Home</summary>

![image](Mockups/Home-1.png)
</details>

<details>
<summary>Vista do Home - Todas as Categorias</summary>

![image](Mockups/Home%20Todas%20as%20Categorias-1.png)
</details>

<details>
<summary>Vista do Home - Todos os Autores</summary>

![image](Mockups/Home%20Todos%20os%20Autores-1.png)
</details>

<details>
<summary>Vista do Home - 1 Categoria</summary>

![image](Mockups/Home%201%20Categoria-1.png)
</details>

<details>
<summary>Vista do Home - 1 Autor</summary>

![image](Mockups/Home%201%20Autor-1.png)
</details>

<details>
<summary>Vista do Home - Churn</summary>

![image](Mockups/Home%20Churn-1.png)
</details>

<details>
<summary>Vista do Departamento</summary>

![image](Mockups/Departamento-1.png)
</details>

<details>
<summary>Vista do Departamento - Privado</summary>

![image](Mockups/Departamento%20Privado-1.png)
</details>

<details>
<summary>Vista de Commits</summary>

![image](Mockups/Commits-1.png)
</details>

<details>
<summary>Vista de Commits - Todos os Autores</summary>

![image](Mockups/Commits%20Todos%20os%20Autores-1.png)
</details>

<details>
<summary>Vista de Commits - 1 Autor</summary>

![image](Mockups/Commits%201%20Autor-1.png)
</details>

<details>
<summary>Vista de Commits - Todas as Categorias</summary>

![image](Mockups/Commits%20Todas%20as%20Categorias-1.png)
</details>

<details>
<summary>Vista de Commits - 1 Categoria</summary>

![image](Mockups/Commits%201%20Categoria-1.png)
</details>

<details>
<summary>Vista de 1 Ficheiro</summary>

![image](Mockups/Vista%20de%201%20Ficheiro-1.png)
</details>

<details>
<summary>Vista do Histórico de 1 Ficheiro</summary>

![image](Mockups/Histórico%20de%201%20Ficheiro-1.png)
</details>

<details>
<summary>Vista do Histórico de Alterações 1 Ficheiro</summary>

![image](Mockups/Histórico%20de%20Alterações%201%20Ficheiro-1.png)
</details>

<details>
<summary>Vista do Login</summary>

![image](Mockups/Login-1.png)
</details>

<details>
<summary>Vista dos Issues</summary>

![image](Mockups/Issues-1.png)
</details>

<details>
<summary>Vista de 1 Issue</summary>

![image](Mockups/1%20Issue-1.png)
</details>